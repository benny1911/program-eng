﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_Backend
{
    public class Doctor
    {
        // attributes :


        public bool needsToBeNotified;

        private int _iID;
        public int iID
        {
            get { return _iID; }
            set
            {
                // checks if the Id is valid
                if ((value >= 100000000 && value <= 999999999))
                {
                    _iID = value;
                }
                else
                // throws an exception...
                {
                    throw new Exception(" ID is not valid !! ");
                }
            }
        }
        private string _sFirst_name;
        public string sFirst_name
        {
            get { return _sFirst_name; }
            set
            {
                // go over the characters in the string , 
                // checks if the firstName is valid.
                    // if the name is made only of charcters

                    //  change it to the negative option like the last name!! 

                    if (isNameValid(value))
                    {
                        _sFirst_name = value;
                    }
                    else
                    // throws an exception .. 
                    {
                        throw new Exception(" First name is not valid !! ");
                    }
                
            }
        }
        private string _sLast_Name;
        public string sLast_Name
        {
            get { return _sLast_Name; }
            set
            {
                    if (isNameValid(value))
                    {
                        _sLast_Name = value; 
                    }
                    else
                    {
                        // throws an exception .. 
                        throw new Exception(" First name is not valid !! ");
                    }
                
            }

        }
        private double _dSalary;
        public double dSalary
        {
            get
            {
                return _dSalary;
            }
            set
            {
                //checks if the salary is legal >0
                if (value < 0)
                    throw new Exception("illegal salary value");

                else _dSalary = value;
            }

        }
        private char _cGender;
        public char cGender
        {
            get { return _cGender; }
            set
            {
                if (value.Equals('M') || value.Equals('F') || value.Equals('z'))
                    _cGender = value;
                else if (value.Equals('f'))
                    _cGender = 'F';
                else if (value.Equals('m'))
                    _cGender = 'M';
                else
                    throw new Exception(" Gender is not valid, must be 'M' or 'F' ");
            }
        }
        private List<int> lstpatients;
        private int? nullable;
        private string p1;
        private string p2;
        private double p3;
        private char p4;
        public List<int> lpatients
        {
            get { return lstpatients; }
            set { lstpatients = value; }
        }

        // methods and functions

        #region Constructors

        //constructor
        public Doctor(int iNewID, string sFirstName, string sLastName, double dNewSalary, char cNewGender, bool needsToBeNotified)
        {
            this.needsToBeNotified = needsToBeNotified;
            iID = iNewID;
            sFirst_name = sFirstName;
            sLast_Name = sLastName;
            dSalary = dNewSalary;
            cGender = cNewGender;
            this.lstpatients = new List<int>();
        }
        //constructor
        public Doctor(Doctor dMainDoctor)
        {
            this.needsToBeNotified = dMainDoctor.needsToBeNotified;
            iID = dMainDoctor.iID;
            sFirst_name = dMainDoctor.sFirst_name;
            sLast_Name = dMainDoctor.sLast_Name;
            lstpatients = dMainDoctor.getPatients();
            dSalary = dMainDoctor.dSalary;
            cGender = dMainDoctor.cGender;
        }
        // empty constructor
        public Doctor()
        {
            needsToBeNotified = false;
            iID = 100000001;
            sFirst_name = "nullified";
            sLast_Name = "nullified";
            dSalary = 1;
            cGender ='z';  // not "m" or "f" .. 
            lstpatients = new List<int>();
        }
      
        #endregion

        public bool isNameValid(string nameToCheck)
        {
            foreach (char c in nameToCheck)
            {
                if (!((c == ' ') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ||( c == '.')))
                    return false;
            }
            return true;
        }

        //creates a shallow copy of the doctors patients
        public List<int> getPatients()
        {
            List<int> pat = lstpatients;
            return pat;
        }

        /*
         * checks if a patient is a patient of this doctor
         * if he is , removes from the list and nullify the patients main doctor
         */
        public void removePatient(Patient patientToRemove)
        {
            foreach (int iExists in lstpatients)
            {
                if (iExists == patientToRemove.iID)
                {
                    lstpatients.Remove(iExists);
                    patientToRemove.MainDoctor = 100000000;
                    break;
                }
            }
        }
         public void addPatient(Patient patient)
        {
            
            //adds and assinges a new patient and a main doctor
            if (!(lstpatients.Contains(patient.iID)))
            {
                lstpatients.Add(patient.iID);
                //patient.MainDoctor = iID; how come?
            }
        }
        /// <summary>
        /// to string method for doctor
        /// </summary>
        /// <param name="doctor"></param>
        /// <returns></returns>
        public string ToString(Doctor doctor)
        {
            string data = "\n Doctor ID: " + doctor.iID + "\n First name " + doctor.sFirst_name + "\n Last name " + doctor.sLast_Name +
                "\n Salary: " + doctor.dSalary + "\n Gender " + doctor.cGender + " \n Patients: ";
            foreach (int patientToCheck in doctor.lstpatients)
            {
                data += patientToCheck.ToString();
            }

            return data;
        }
        
        //compares Doctors by id
        public bool equals(Doctor d2)
        {
            return (_iID == d2.iID);

        }
    }
}
