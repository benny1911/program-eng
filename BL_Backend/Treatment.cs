﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class Treatment
    {
        //attributes
        //a global static integer that goes up by 1 every time a new treatment is logged.

        private int _iVisitID;
        public int iVisitID
        {
            get { return _iVisitID; }
            set
            {
                if (value > 0) _iVisitID = value;
                else throw new Exception("That is not a valid ID");
            }
        }

        
        private static int _iGlobalTreatmentId = new Random().Next(0,10000);
        public static int iGlobalTreatmentId
        {
            get { return _iGlobalTreatmentId; }
            set { _iGlobalTreatmentId = value; }
        }
        //export use only
        public int iGlobalId
        {
            get { return iGlobalTreatmentId; }
            set { iGlobalTreatmentId = value + 1; }
        }


        private string _sTreatmentName;
        public string sTreatmentName
        {
            get { return _sTreatmentName; }
            set { _sTreatmentName = value; }
        }

        private int _iTreatmentID;
        public int iTreatmentID
        {
            get { return _iTreatmentID; }
            set
            {
                if (value > 0) _iTreatmentID = value;
                else throw new Exception("That is not a valid ID");
            }
        }

        private DateTime _DateOfStart;
        public DateTime DateOfStart
        {
            get { return _DateOfStart; }
            set { _DateOfStart = value; }
        }

        private DateTime _DateOfFinish;
        public DateTime DateOfFinish
        {
            get { return _DateOfFinish; }
            set { if (value >= DateOfStart) _DateOfFinish = value; }
        }

        private int _createdByDoctor;
        public int createdByDoctor
        {
            get { return _createdByDoctor; }
            set { _createdByDoctor = value; }
        }


        private string _sPrognosis;
        public string sPrognosis
        {
            get { return _sPrognosis; }
            set { _sPrognosis = value; }
        }

        private List<String> _sPrescriptions;
        public List<String> sPrescriptions
        {
            get { return _sPrescriptions; }
            set { _sPrescriptions = value; }
        }

        private int _iPatientID;
        public int iPatientID
        {
            get { return _iPatientID; }
            set {_iPatientID=value;}
        }



        public Treatment(int treatmentId, DateTime _DateOfStart, DateTime _DateOfFinish, Doctor createdByDoctor, string sPrognosis, List<string> prescriptions, string treatmentName, int patientid, int visitid)
        {
            this.iVisitID = visitid;
            this.iTreatmentID = treatmentId;
            this.DateOfStart = _DateOfStart;
            this.DateOfFinish = _DateOfFinish;
            this.createdByDoctor = createdByDoctor.iID;
            this.sPrognosis = sPrognosis;
            if(prescriptions==null) _sPrescriptions=new List<String>();
            else
                this._sPrescriptions = prescriptions;
            this.sTreatmentName = treatmentName;
            this.iPatientID = patientid;
        }

        public Treatment()
        {
            this.iVisitID = 22;
            this.iTreatmentID = 1;
            this.DateOfStart = DateTime.Now;
            this.DateOfFinish = DateTime.Now;
            this.createdByDoctor = 1000000000;
            this.sPrognosis = "";
            this._sPrescriptions = new List<string>();
            this.sTreatmentName = "";
            this._iPatientID = 111111112;
        
        }
        //Functions
        public void editTreatmentName(string newName)
        {
            sTreatmentName = newName;
        }

        public void editDateOfStart(DateTime _date)
        {
            this.DateOfStart = _date;
        }
        /*this function enables to change the start date to a new date.input is checked by the DateTime class.*/


        public void editDateOfEnd(DateTime _dateEnd)
        {
            this.DateOfFinish = _dateEnd;
        }
        /*/*this function enables to change the end date to a new date.input is checked by the DateTime class.*/


        public int getOriginalDoctor()
        {
            return this.createdByDoctor;
        }
        /*this function returns the original doctor who gave the treatment.*/

        public List<string> getPrescriptions()
        {
            return this.sPrescriptions;
        }
        /*this function returns the prescripstions which the doctor gave for the treatment*/


        public void AddPrognosis(string prognosis)
        {
            sPrognosis += ","+prognosis;
        }
        /*this function adds new prognosis to the existing one*/

        public void RemovePrognosis()
        {
            sPrognosis = "";
        }
        /*this function enables to remove the prognosis from the treatment*/

        public void addPrescriptions(string prescription)
        {
            sPrescriptions.Add(prescription);
        }
        /*this functions enables to add prescription to the prescriptions which were given*/

        public void removePrescriptions(string sPrescription)
        {
            foreach (string sNewPrescription in sPrescriptions)
            {
                if (sPrescription.Equals(sNewPrescription))
                {
                    sPrescriptions.Remove(sNewPrescription);
                    break;
                }
            }
        }
        //*this functions enables to remove a given prescription from the list of the prescriptions which were given for the treatment./

        public string ToString(Treatment treatment)
        {
            string data = "\n Treatment ID: " + treatment.iTreatmentID + "\n Date of start: " + treatment.DateOfStart.ToString() + "\n Date of finish: " + treatment.DateOfFinish.ToString() +
                "\n created by Doctor: " + treatment.createdByDoctor +
                "\n Prognosis " + treatment.sPrognosis + " \n Prescriptions: ";
            foreach (string s in treatment.sPrescriptions)
            {
                data += s.ToString();
            }

            return data;
        }

        //compares treatments by id
        public bool equals(Treatment T1)
        {
            return (T1.iTreatmentID == _iTreatmentID);

        }

    }
}