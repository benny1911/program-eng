﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class LabTest
    {
        //attributes:

        //a global static integer that goes up by 1 every time a new LabTest is logged.
        private static int _iGlobalLabTestId = new Random().Next(0, 10000);
        public static int iGlobalLabTestId
        {
            get { return _iGlobalLabTestId; }
            set { _iGlobalLabTestId = value; }
        }
        //export use only
        public int iGlobalId
        {
            get { return iGlobalLabTestId; }
            set { iGlobalLabTestId = value + 1; }
        }

        private int _iLabTestID;
        public int iLabTestID
        {
            get { return _iLabTestID; }
            set
            {
                _iLabTestID = value;
            }
        }

        private int _iPatientId;
        public int iPatientId
        {
            get { return _iPatientId; }
            set
            {
                _iPatientId = value;
            }
        }

        private int _iLabTechId;
        public int iLabTechId
        {
            get { return _iLabTechId; }
            set
            {
                _iLabTechId = value;
            }
        }

   

        private DateTime _dateOfLabTest;
        public DateTime dateOfLabTest
        {
            get { return _dateOfLabTest; }
            set { _dateOfLabTest = value; }
        }

        private int _AssignedDoctor;
        public int AssignedDoctor
        {
            get { return _AssignedDoctor; }
            set { _AssignedDoctor = value; }
        }


        private string _sResults;
        public string sResults
        {
            get { return _sResults; }
            set { if (value is string) _sResults = value; }
        }


        private string _sDoctorNotes;
        public string sDoctorNotes
        {
            get { return _sDoctorNotes; }
            set { if (value is string) _sDoctorNotes = value; }
        }
        
        //constructor
        public LabTest(int LabTestid, DateTime _dateLabTest, Doctor assignedDoctor, int patientID, string doctorNotes)
        {
            this.iLabTestID = LabTestid;
            this.dateOfLabTest = _dateLabTest;
            this.AssignedDoctor = assignedDoctor.iID;
            this._iPatientId = patientID;
            this.sDoctorNotes = doctorNotes;
            this._sResults = "";
            //this.iLabTestID = 0;
        }
        //empty constructor
        public LabTest()
        {
            this.iLabTestID = 1;
            this.dateOfLabTest = DateTime.Now;
            this.AssignedDoctor = 100000000;
            this._iPatientId = 111111112;
            this.sDoctorNotes = "";
            this._sResults = "";

        }


        //functions 


        public void editDateOfLabTest(DateTime newDate)
        {
            if ((newDate.Hour <= 20) && (newDate.Hour >= 8))
                this.dateOfLabTest = newDate;
            else
                throw new Exception("the clynic is closed in these hours");
        }
        //*this function enables the user to edit the date of the LabTest and checks the input.
        //*if the input is incorrect, it throws an exception.*//


        public void changeDoctor(Doctor newMainDoctor)
        {
            if (!(newMainDoctor is Doctor))
                throw new Exception("incorrect input for this field");
            else
            {
                this.AssignedDoctor = newMainDoctor.iID;
            }
        }
        //*this function enables to change the doctor which the patient LabTested, and allso checks the input.*//


    
        //this function removes a treatment from the LabTest's treatments.*//
        public void RemoveDoctorNotes()
        {
            this.sDoctorNotes = "";
        }
        //this function enables ro remove the doctor notes from the LabTest*//

        public void AddDoctorNotes(string NewNotes)
        {
            this.sDoctorNotes += "," + NewNotes;
        }
        //*this function enables to add doctor notes to the excisting ones.*// 

        //overrides object
        public string ToString(LabTest LabTest)
        {
            string data = "\n LabTest id: " + LabTest.iLabTestID + "\n Date: " + LabTest.dateOfLabTest.ToString() + "\n Assigned Doctor: " + LabTest.AssignedDoctor +
                "\n Patient ID: " + LabTest._iPatientId + " \n Doctor's notes: " + LabTest.sDoctorNotes + "\n Results: "+LabTest.sResults;
           


            return data;
        }

        //2 LabTests are equal if they have the same ID
        public bool equals(LabTest v1, LabTest v2)
        {
            return (v1.iLabTestID == _iLabTestID);

        }

    }
}

