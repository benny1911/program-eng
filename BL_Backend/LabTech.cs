﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class LabTech
    {
        // attributes :

        private int _iID;
        public int iID
        {
            get { return _iID; }
            set
            {
                // checks if the Id is valid
                if ((value >= 100000000 && value <= 999999999))
                {
                    _iID = value;
                }
                else
                // throws an exception...
                {
                    throw new Exception(" ID is not valid !! ");
                }
            }
        }
        private string _sFirst_name;
        public string sFirst_name
        {
            get { return _sFirst_name; }
            set
            {
                // go over the characters in the string , 
                // checks if the firstName is valid.
                    // if the name is made only of charcters

                    //  change it to the negative option like the last name!! 

                    if (isNameValid(value))
                    {
                        _sFirst_name = value;
                    }
                    else
                    // throws an exception .. 
                    {
                        throw new Exception(" First name is not valid !! ");
                    }
                
            }
        }
        private string _sLast_Name;
        public string sLast_Name
        {
            get { return _sLast_Name; }
            set
            {
                    if (isNameValid(value))
                    {
                        _sLast_Name = value; 
                    }
                    else
                    {
                        // throws an exception .. 
                        throw new Exception(" First name is not valid !! ");
                    }
                
            }

        }
        private double _dSalary;
        public double dSalary
        {
            get
            {
                return _dSalary;
            }
            set
            {
                //checks if the salary is legal >0
                if (value < 0)
                    throw new Exception("illegal salary value");

                else _dSalary = value;
            }

        }

        private char _cGender;
        public char cGender
        {
            get { return _cGender; }
            set
            {
                if (value.Equals('M') || value.Equals('F') || value.Equals('z'))
                    _cGender = value;
                else if (value.Equals('f'))
                    _cGender = 'F';
                else if (value.Equals('m'))
                    _cGender = 'M';
                else
                    throw new Exception(" Gender is not valid, must be 'M' or 'F' ");
            }
        }
        // methods and functions

        #region Constructors

        //constructor
        public LabTech(int iNewID, string sFirstName, string sLastName, double dNewSalary, char cNewGender)
        {
            iID = iNewID;
            sFirst_name = sFirstName;
            sLast_Name = sLastName;
            dSalary = dNewSalary;
            cGender = cNewGender;
        }
        //constructor
        public LabTech(LabTech labTechnician)
        {
            iID = labTechnician.iID;
            sFirst_name = labTechnician.sFirst_name;
            sLast_Name = labTechnician.sLast_Name;
            dSalary = labTechnician.dSalary;
            cGender = labTechnician.cGender;
        }
        // empty constructor
        public LabTech()
        {
            iID = 100000001;
            sFirst_name = "nullified";
            sLast_Name = "nullified";
            dSalary = 1;
            cGender ='z';  // not "m" or "f" .. 
        }

      
        #endregion

        public bool isNameValid(string nameToCheck)
        {
            foreach (char c in nameToCheck)
            {
                if (!((c == ' ') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ||( c == '.')))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// to string method for LabTech
        /// </summary>
        /// <param name="tech"></param>
        /// <returns></returns>
        public string ToString(LabTech tech)
        {
            string data = "\n LabTech ID: " + tech.iID + "\n First name " + tech.sFirst_name + "\n Last name " + tech.sLast_Name +
                "\n Salary: " + tech.dSalary + "\n Gender " + tech.cGender;
            return data;
        }
        
        //compares Doctors by id
        public bool equals(LabTech d2)
        {
            return (_iID == d2.iID);

        }
    }
}
