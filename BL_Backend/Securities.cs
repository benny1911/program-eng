﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    // the class supports all the securities data and details:
    public class Securities
    {
        private int iID;
        public int _id
        {
            get { return iID; }
            set { iID = value; }
        }
        private int iPassword;
        public int _ipass
        {
            get { return iPassword; }
            set { iPassword = value;}
        }
        private string sAutorization;
        public string _sauto
        {
            get { return sAutorization; }
            set { sAutorization = value;}
        }
        private static int adminUserId = 100100100;

        private static int adminPass = 100100100;

        public Securities()
        {
            _id = 0;
            _ipass= 0;
            _sauto = "";
        }
        public Securities(int iNewId,int iNewPass,string sNewAutorization)
        {
            iID = iNewId;
            iPassword = iNewPass;
            if (sNewAutorization.Equals("Doctor") || sNewAutorization.Equals("Patient") 
                || sNewAutorization.Equals("Admin") || sNewAutorization.Equals("Tech"))
                sAutorization = sNewAutorization;
            else sAutorization = "";
        }
        //getters:
        public static int GetAdminID()
        {
            return adminUserId;
        }
        public static int GetAdminPass()
        {
            return adminPass;
        }
        public int getID()
        {
            return iID;
        }
        public int getPass()
        {
            return iPassword;
        }
        public string getAutorization()
        {
            return sAutorization;
        }
        //setters
        public void setPass(int pass)
        {
            iPassword = pass;
        }
        public void setAutorization(string sNewAutorization)
        {
            if (sNewAutorization.Equals("Doctor") || sNewAutorization.Equals("Patient") 
                || sNewAutorization.Equals("Admin") || sNewAutorization.Equals("Tech"))
                sAutorization = sNewAutorization;
        }
    }

}