﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class Visit
    {
        //attributes:

        //a global static integer that goes up by 1 every time a new visit is logged.
        private static int _iGlobalVisitId = new Random().Next(0, 10000);
        public static int iGlobalVisitId
        {
            get { return _iGlobalVisitId; }
            set { _iGlobalVisitId = value; }
        }
        //export use only
        public int iGlobalId
        {
            get { return iGlobalVisitId; }
            set { iGlobalVisitId = value+1; }
        }

        private int _iVisitID;
        public int iVisitID
        {
            get { return _iVisitID; }
            set
            {
                _iVisitID = value;
            }
        }

        private int _iPatientId;
        public int iPatientId
        {
            get { return _iPatientId; }
            set
            {
                _iPatientId = value;
            }
        }

        private DateTime _dateOfVisit;
        public DateTime dateOfVisit
        {
            get { return _dateOfVisit; }
            set { _dateOfVisit = value; }
        }

        private int _AssignedDoctor;
        public int AssignedDoctor
        {
            get { return _AssignedDoctor; }
            set { _AssignedDoctor = value; }
        }


        private string _sDoctorNotes;
        public string sDoctorNotes
        {
            get { return _sDoctorNotes; }
            set { if (value is string) _sDoctorNotes = value; }
        }

        private List<int> _Treatments;
        public List<int> Treaments
        {
            get { return _Treatments; }
            set { _Treatments = value; }
        }

        //constructor
        public Visit(int visitid, DateTime _dateVisit, Doctor assignedDoctor, int patientID, string doctorNotes, List<int> trnNew)
        {
            this.iVisitID = visitid;
            this.dateOfVisit = _dateVisit;
            this.AssignedDoctor = assignedDoctor.iID;
            this._iPatientId = patientID;
            this.sDoctorNotes = doctorNotes;
            this._Treatments = trnNew;
         }
        //empty constructor
        public Visit()
        {
            this.iVisitID = 1;
            this.dateOfVisit = DateTime.Now;
            this.AssignedDoctor = 100000000;
            this._iPatientId = 111111112;
            this.sDoctorNotes = "";
            this._Treatments = new List<int>();
        }


        //functions 


        public void editDateOfVisit(DateTime newDate)
        {
            if ((newDate.Hour <= 20) && (newDate.Hour >= 8))
                this.dateOfVisit = newDate;
            else
                throw new Exception("the clynic is closed in these hours");
        }
        //*this function enables the user to edit the date of the visit and checks the input.
        //*if the input is incorrect, it throws an exception.*//


        public void changeDoctor(Doctor newMainDoctor)
        {
            if (!(newMainDoctor is Doctor))
                throw new Exception("incorrect input for this field");
            else
            {
                this.AssignedDoctor = newMainDoctor.iID;
            }
        }
        //*this function enables to change the doctor which the patient visited, and allso checks the input.*//


        public void AddTreatment(Treatment _treatment)
        {
            if(!(_Treatments.Contains(_treatment.iTreatmentID))) _Treatments.Add(_treatment.iTreatmentID);
        }
        //*this function enables to add new treatment to the treatments of the visit.*//

        public void RemoveTreatment(Treatment treatment)
        {
            foreach (int newTreatment in _Treatments)
            {
                if (treatment.iTreatmentID == newTreatment)
                {
                    _Treatments.Remove(treatment.iTreatmentID);
                    break;
                }
            }
        }
        //this function removes a treatment from the visit's treatments.*//
        public void RemoveDoctorNotes()
        {
            this.sDoctorNotes = "";
        }
        //this function enables ro remove the doctor notes from the visit*//

        public void AddDoctorNotes(string NewNotes)
        {
            this.sDoctorNotes += ","+NewNotes;
        }
        //*this function enables to add doctor notes to the excisting ones.*// 

        //overrides object
        public string ToString(Visit visit)
        {
            string data = "\n Visit id: " + visit.iVisitID + "\n Date: " + visit.dateOfVisit.ToString() + "\n Assigned Doctor: " + visit.AssignedDoctor +
                "\n Patient ID: " + visit._iPatientId + " \n Doctor's notes: " + visit.sDoctorNotes + "\n Treatments: ";
            foreach (int t in visit.Treaments)
            {
                data+="\n        "+t.ToString(); 
            }
            
                
            return data;
        }

        //2 visits are equal if they have the same ID
        public bool equals(Visit v1,Visit v2)
        {
            return (v1.iVisitID == _iVisitID);
            
        }

        public List<int> getTreatments()
        {
            return this._Treatments;
        }
        //firt test for gal
    }
}

