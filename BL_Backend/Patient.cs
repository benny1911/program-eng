﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BL_Backend
{
    public class Patient
    {
        // attributes :

        private int _iID;
        public int iID
        {
            get { return _iID; }
            set
            {
                // checks if the Id is valid
                if (value >= 100000000 && value <= 999999999)
                {
                    _iID = value;
                }
                else
                // throws an exception...
                {
                    throw new Exception(" ID is not valid !! ");
                }
            }
        }
        private string _sFirst_name;
        public string sFirst_name
        {
            get { return _sFirst_name; }
            set
            {
                  if (isNameValid(value)) { _sFirst_name=value;  }
                    else
                        // throws an exception .. 
                    {
                        throw new Exception(" First name is not valid !! ");
                    }
               
            }
        }
        private string _sLast_Name;
        public string sLast_Name
        {
            get { return _sLast_Name; }
            set
            {
                if (isNameValid(value)) { _sLast_Name = value; }
                else
                // throws an exception .. 
                {
                    throw new Exception(" last name is not valid !! ");
                }
            }

        }
        private char _cGender;
        public char cGender
        {
            get { return _cGender; }
            set
            {
                if (value.Equals('M') || value.Equals('F') || value.Equals('z'))
                    _cGender = value;
                else if (value.Equals('f'))
                    _cGender = 'F';
                else if (value.Equals('m'))
                    _cGender = 'M';
                else // the input not valid - not m or f
                {
                    // throws an exception .. 
                    throw new Exception("Gender is not valid, must be 'M' or 'F' ");
                }
            }
        }
        private double _iAge;
        public double iAge
        {
            get { return _iAge; }
            set
            {
                //Verifies valid age
                if ((value > 0) && (value <= 120))
                {
                    _iAge = value;
                }
            }
        }
        //creates a shalow copy of the patients Main Doctor
        //note:the patients List is a shallow copy
        private int _MainDoctor;
        public int MainDoctor
        {
            get { return _MainDoctor; }
            set {_MainDoctor = value; }
        }
        private List<int> lVisits;
        public List<int> lstVisits
        {
            get { return lVisits; }
            set { lVisits=value; }
        }
        private List<int> lTreatments;
        public List<int> lstTreatments
        {
            get { return lTreatments; }
            set { lTreatments=value; }
        }
        

        
        // methods and functions


        //constructor
        public Patient(int iNewID, string sFirstName, string sLastName, double iNewAge, char cNewGender, int mainDoctor)
        {
            iID = iNewID;
            sFirst_name = sFirstName;
            sLast_Name = sLastName;
            iAge = iNewAge;
            cGender = cNewGender;
            lVisits=new List<int>();
            lTreatments=new List<int>();
            MainDoctor = mainDoctor;
        }
        //empty constructor
        public Patient()
        {
            iID = 111111112;
            sFirst_name = "nullified";
            sLast_Name = "nullified";
            iAge = 1;
            cGender = 'z' ;
            MainDoctor = 100000000;
            lVisits = new List<int>();
            lTreatments = new List<int>();
        }


        public bool isNameValid(string nameToCheck)
        {
            foreach (char c in nameToCheck)
            {
                if (!((c == ' ') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '.'))
                    return false;
            }
            return true;
        }

        public void AddVisit(Visit vNewVisit)
        {  
            if (!lVisits.Contains(vNewVisit.iVisitID)) 
            {
                lVisits.Add(vNewVisit.iVisitID);
                vNewVisit.iPatientId = iID;
            }
        }

        //checks if the visit exist, if not does nothing
        public void RemoveVisit(Visit vVisitToRemove)
        {
            foreach(int vCheck in lVisits)
            {
                if (vCheck == vVisitToRemove.iVisitID)
                {
                    lVisits.Remove(vCheck);
                    break;
                }
            }
        }

        //returns all the visits made by the patient
        public List<int> getVisits()
        {
            return this.lVisits;
        }

        //returns akk the treatments made by the patient
        public List<int> getTreatments()
        {
            return this.lTreatments;
        }
        public void AddTreatment(Treatment tToAdd)
        {
            //only checks shallow copies
            if (!lTreatments.Contains(tToAdd.iTreatmentID)) { lTreatments.Add(tToAdd.iTreatmentID); }
        }
        //checks if the treatment exists, if not does nothing
        public void RemoveTreatment(Treatment treatmentToRemove)
        {
            foreach (int tCheck in lTreatments)
            {
                if (tCheck == treatmentToRemove.iTreatmentID)
                {
                    lTreatments.Remove(tCheck);
                    break;
                }
            }
        }

        public string ToString(Patient patient)
        {
            string data = "\n Patient ID: " + patient.iID + "\n First name " + patient.sFirst_name + "\n Last name " + patient.sLast_Name +
                "\n Age: " + patient.iAge + "\n Gender " + patient.cGender + "\n Main Doctor " + patient.MainDoctor+ "\n Visits: ";
            foreach (int v in patient.lVisits)
            {
                data += v.ToString();
            }
            data += "\n Treatments: ";
                foreach (int t in patient.lTreatments)
            {
                data += t.ToString();
            }

            return data;
        }

        //compares Patients by id
        public bool equals(Patient p1)
        {
            return (p1.iID == _iID);

        }

    }
}
