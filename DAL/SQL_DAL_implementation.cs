﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SqlClient;
using BL_Backend;
using System.Data;
namespace DAL
{

    public class SQL_DAL_implementation :IDAL
    {
        //public void exportXMLTechs(List<LabTech> Test, String fileName) { }
        //public void exportXMLTests(List<LabTest> Test, String fileName) { }
        //public void importXMLTechs(String fileName) { }
        //public void importXMLTests(String fileName) { }
        //fields:
        public team19DataContext db;
        public SqlConnection myConnection { get; set; }

        //constructor:
        public SQL_DAL_implementation()
        {
            this.db = new team19DataContext();
            SQLConnect();
        }


        public void SQLConnect()
        {
            myConnection = new SqlConnection("user id=team19;" +

                                                         "password=123456;" +

                                                         "server=ise-sql12.campus.ad.bgu.ac.il;" +

                                                         "Trusted_Connection=no;" +

                                                         "database=team19; " +

                                                         "connection timeout=30");


            try
            {
                myConnection.Open();
            }

            catch (Exception e1)
            {
                throw e1;
            }
        }

        public void SQLWrite(String commandText)
        {
                SqlCommand command = new SqlCommand(commandText, myConnection);
                command.ExecuteNonQuery();//throws exceptions
        }



        #region Add
        public void addDoctor(Doctor doctorToAdd)
        {
            int bitNeedsToBeNotified=0;
            if (doctorToAdd.needsToBeNotified)
                bitNeedsToBeNotified = 1;
            string commandText = "INSERT INTO doctors " +

             "Values (" + Convert.ToInt32(doctorToAdd.cGender) +", " + doctorToAdd.iID + ", '" + doctorToAdd.sFirst_name + "', '" + doctorToAdd.sLast_Name + 
             "', " + doctorToAdd.dSalary  + ", " + bitNeedsToBeNotified +")";

            SQLWrite(commandText);

        }

        public void addPatient(Patient patientToAdd)
        {
        
           string commandText = "INSERT INTO patients " +

             "Values (" +Convert.ToInt32(patientToAdd.cGender) +", " + patientToAdd.iID + ", '" + patientToAdd.sFirst_name + "', '" + patientToAdd.sLast_Name +
             "', " + patientToAdd.iAge + ", " + patientToAdd.MainDoctor + ")";

           SQLWrite(commandText);
       
        }


        public void addTech(LabTech techToAdd)
        {
           string commandText = "INSERT INTO labTech " +

             "Values (" + Convert.ToInt32(techToAdd.cGender) + ", " + techToAdd.iID + ", '" + techToAdd.sFirst_name + "', '" + techToAdd.sLast_Name +
             "', " + techToAdd.dSalary +  ")";

           SQLWrite(commandText);

        }



        public void addVisit(Visit visitToAdd)
        {
            string date = "'" + visitToAdd.dateOfVisit.Year + "-" + visitToAdd.dateOfVisit.Month + "-" + visitToAdd.dateOfVisit.Day + "'";
            string commandText = "INSERT INTO visits " +

             "Values (" + visitToAdd.AssignedDoctor + ", " + visitToAdd.iVisitID + ", " + visitToAdd.iPatientId + ", " 
             + date + ", '" + visitToAdd.sDoctorNotes + "')";

            SQLWrite(commandText);

        }


        public void addTreatment(Treatment treatmentToAdd)
        {
            string dateOfStart = "'" + treatmentToAdd.DateOfStart.Year + "-" + treatmentToAdd.DateOfStart.Month + "-" + treatmentToAdd.DateOfStart.Day + "'";
            string dateOfFinish = "'" + treatmentToAdd.DateOfFinish.Year + "-" + treatmentToAdd.DateOfFinish.Month + "-" + treatmentToAdd.DateOfFinish.Day + "'";

         string commandText = "INSERT INTO treatments " +

             "Values (" + treatmentToAdd.createdByDoctor + ", " + treatmentToAdd.iTreatmentID + ", " + treatmentToAdd.iPatientID + ", "
             + dateOfStart  +", " + dateOfFinish + ", '" + treatmentToAdd.sTreatmentName + "', '" +
             treatmentToAdd.sPrognosis + "', '" +treatmentToAdd.sPrescriptions.ElementAt(0) +  "', " + treatmentToAdd.iVisitID +")";


         SQLWrite(commandText);

        }



        public void addLabTest(LabTest labTestToAdd)
        {
            string date = "'" + labTestToAdd.dateOfLabTest.Year + "-" + labTestToAdd.dateOfLabTest.Month + "-" + labTestToAdd.dateOfLabTest.Day + "'";
           string commandText = "INSERT INTO labTest " +

             "Values (" + labTestToAdd.AssignedDoctor + ", " + labTestToAdd.iLabTestID + ", " + labTestToAdd.iPatientId + ", "
             + date + ", " + labTestToAdd.iLabTechId + ", '" + labTestToAdd.sDoctorNotes + "', '"
             + labTestToAdd.sResults + "')";


           SQLWrite(commandText);

        }

        public void addSecurity(Securities securityToAdd)
        {
            string commandText = "INSERT INTO securities " +

              "Values (" + securityToAdd._id + ", " + securityToAdd._ipass + ", '" + securityToAdd._sauto + "')";


            SQLWrite(commandText);

        }


        #endregion

        #region Update

        #region doctor
        public void updateDoctor(Doctor d)
        {
            foreach (doctor curr in db.doctors)
            {
                if (curr.iID == d.iID)
                {
                    curr.sFirst_name = d.sFirst_name;
                    curr.sLast_name = d.sLast_Name;
                    curr.dSalary = d.dSalary;
                    curr.cGender = d.cGender;
                    curr.needsToBeNotified = d.needsToBeNotified;
                }

            }
            db.SubmitChanges();
        }
        public void updateDoctorsGender(Doctor d,char newGender)
        {
            foreach (doctor curr in db.doctors)
            {
                if (curr.iID == d.iID)
                {
                    curr.cGender = newGender;
                    break;
                }
            }
            db.SubmitChanges();
        }

        public void updateDoctorsFirstName(Doctor d, string newName)
        {
            foreach (doctor curr in db.doctors)
            {
                if (curr.iID == d.iID)
                {
                    curr.sFirst_name = newName;
                    break;
                }
            }
            db.SubmitChanges();
        }

        public void updateDoctorsLastName(Doctor d, string newName)
        {
            foreach (doctor curr in db.doctors)
            {
                if (curr.iID == d.iID)
                {
                    curr.sLast_name = newName;
                    break;
                }

            }
            db.SubmitChanges();
        }

        public void updateDoctorsSalary(Doctor d, double newSalary)
        {
            foreach(doctor curr in db.doctors)
            {
                if (curr.iID == d.iID)
                {
                    curr.dSalary = newSalary;
                    break;
                }

            }
            db.SubmitChanges();
        }

        public void updateDoctorsNeedsToBeNotified(Doctor d, bool notify)
        {
            foreach (doctor curr in db.doctors)
            {
                if (curr.iID == d.iID)
                {
                    curr.needsToBeNotified = notify;
                    break;
                }
                    
            }
            db.SubmitChanges();
        }


        #endregion

        public void updatePatient(Patient p)
        {
            foreach (patient curr in db.patients)
            {
                if (curr.iID == p.iID)
                {
                    curr.cGender = p.cGender;
                    curr.sFirst_name = p.sFirst_name;
                    curr.sLast_name = p.sLast_Name;
                    //dafuq casting
                    curr.age = (int?)p.iAge;
                    curr.mainDoctor = p.MainDoctor;

                    break;
                }

            }
            db.SubmitChanges();
        }

        public void updateVisit(Visit v)
        {
            foreach (visit curr in db.visits)
            {
                if (curr.iVisitID == v.iVisitID)
                {
                    curr.assignedDoctor = v.AssignedDoctor;
                    curr.iPatientID = v.iPatientId;
                    curr.dateOfStart = v.dateOfVisit;
                    curr.sDoctor_notes = v.sDoctorNotes;
                    break;
                }

            }
            db.SubmitChanges();
        }

        public void updateTreatment(Treatment t)
        {
            foreach (treatment curr in db.treatments)
            {
                if (curr.itreatmentID == t.iTreatmentID)
                {
                    curr.assignedDoctor = t.createdByDoctor;
                    curr.iPatientID = t.iPatientID;
                    curr.dateOfStart = t.DateOfStart;
                    curr.dateOfFinish = t.DateOfFinish;
                    curr.sTreatments_name = t.sTreatmentName;
                    curr.sPrignosis = t.sPrognosis;
                    String s = "";
                    foreach(string tmp in t.sPrescriptions)
                    {
                        s += ", " + tmp;
                    }
                    curr.sPrescription = s;
                    break;
                }

            }
            db.SubmitChanges();
        }

        public void updateSecurity(Securities s)
        {
            foreach (security curr in db.securities)
            {
                if (curr.id == s._id)
                {
                    curr.pass = s._ipass;
                    curr.sauto = s._sauto;
                    break;
                }

            }
            db.SubmitChanges();
        }

        public void updateSecurity(int id,string newAuto)
        {
            foreach (security curr in db.securities)
            {
                if (curr.id == id)
                {
                    curr.sauto = newAuto;
                    break;
                }

            }
            db.SubmitChanges();
        }

        public void updateLabTech(LabTech t)
        {
            foreach (labTech curr in db.labTeches)
            {
                if (curr.iID == t.iID)
                {
                    curr.sFirst_name = t.sFirst_name;
                    curr.sLast_name = t.sLast_Name;
                    curr.dSalary = t.dSalary;
                    curr.cGender = t.cGender;
                    break;
                }

            }
            db.SubmitChanges();
        }

        public void updateLabTest(LabTest t)
        {
            foreach (labTest curr in db.labTests)
            {
                if (curr.iLabTestID == t.iLabTestID)
                {
                    curr.assignedDoctor = t.AssignedDoctor;
                    curr.iPatientID = t.iPatientId;
                    curr.dateOfLabTest = t.dateOfLabTest;
                    curr.iLabTechId = t.iLabTechId;
                    curr.sDoctorNotes = t.sDoctorNotes;
                    curr.sResults = t.sResults;
                    break;
                }

            }
            db.SubmitChanges();
        }


        #endregion

        #region remove

        public void removeDoctor(int idToRemove)
        {
            foreach(doctor curr in db.doctors)
            {
                if (curr.iID == idToRemove)
                {
                    db.doctors.DeleteOnSubmit(curr);
                    break;
                }
            }
            //removes assinged doctors from the patients
            foreach(patient curr in db.patients)
            {
                if(curr.mainDoctor==idToRemove)
                    curr.mainDoctor = 1000000001;
            }
            //remove assinged doctor from the visits
            foreach(visit curr in db.visits)
            {
                if (curr.assignedDoctor == idToRemove)
                    curr.assignedDoctor = 1000000001;
            }
            //remove assinged doctor from treatment
            foreach(treatment curr in db.treatments)
            {
                if (curr.assignedDoctor == idToRemove)
                    curr.assignedDoctor = 1000000001;
            }
            //remove assinged doctor from lab tests
            foreach(labTest curr in db.labTests)
            {
                if (curr.assignedDoctor == idToRemove)
                    curr.assignedDoctor = 1000000001;
            }
            //removes doctors autorization
            foreach (security curr in db.securities)
            {
                if (curr.id == idToRemove)
                {
                    db.securities.DeleteOnSubmit(curr);
                    break;
                }
            }

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void removePatient(int idToRemove)
        {
            //removes the patient
            foreach (patient curr in db.patients)
            {
                if (curr.iID == idToRemove)
                {
                    db.patients.DeleteOnSubmit(curr);
                    break;
                }
            }
            //removes his visits
            foreach(visit curr in db.visits)
            {
                if (curr.iPatientID == idToRemove)
                    db.visits.DeleteOnSubmit(curr);
            }
            //removes the treatments
            foreach(treatment curr in db.treatments)
            {
                if (curr.iPatientID == idToRemove)
                    db.treatments.DeleteOnSubmit(curr);
            }
            //removes the tests
            foreach(labTest curr in db.labTests)
            {
                if (curr.iPatientID == idToRemove)
                    db.labTests.DeleteOnSubmit(curr);
            }
            //removes patients autorization
            foreach (security curr in db.securities)
            {
                if (curr.id == idToRemove)
                {
                    db.securities.DeleteOnSubmit(curr);
                    break;
                }
            }
            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void removeVisit(int idToRemove)
        {
            foreach(visit curr in db.visits)
            {
                if(curr.iVisitID==idToRemove)
                {
                    db.visits.DeleteOnSubmit(curr);
                    break;
                }
            }

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void removeTreatment(int idToRemove)
        {
            foreach(treatment curr in db.treatments)
            {
                if (curr.itreatmentID == idToRemove)
                {
                    db.treatments.DeleteOnSubmit(curr);
                    break;
                }
            }

            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public void removeLabTech(int idToRemove)
        {
            //removes the tech
            foreach (labTech curr in db.labTeches)
            {
                if (curr.iID == idToRemove)
                {
                    db.labTeches.DeleteOnSubmit(curr);
                    break;
                }
            }
            //removes his autorizations
            foreach (security curr in db.securities)
            {
                if (curr.id == idToRemove)
                {
                    db.securities.DeleteOnSubmit(curr);
                    break;
                }
            }
            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void removeLabTest(int idToRemove)
        {
            foreach (labTest curr in db.labTests)
            {
                if (curr.iLabTestID == idToRemove)
                {
                    db.labTests.DeleteOnSubmit(curr);
                    break;
                }
            }
            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void removeSecurity(int idToRemove)
        {
            foreach (security curr in db.securities)
            {
                if (curr.id == idToRemove)
                {
                    db.securities.DeleteOnSubmit(curr);
                    break;
                }
            }
            try
            {
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region Queries
        //Queries:

        // all patients:
        public IQueryable<patient> getAllPatients()
        {
            IQueryable<patient> patientQuery = from pat in db.patients select pat;

            return patientQuery;
        }


        // all doctors:
        public IQueryable<doctor> getAllDoctors()
        {
            IQueryable<doctor> doctorQuery = from doc in db.doctors select doc;
            return doctorQuery;
        }

        // all visits:
        public IQueryable<visit> getAllVisits()
        {
            IQueryable<visit> visitQuery = from vis in db.visits select vis;
            return visitQuery;
        }

        // all treatments:
        public IQueryable<treatment> getAlltreatments()
        {
            IQueryable<treatment> treatmentQuery = from t in db.treatments select t;
            return treatmentQuery;
        }

        // all LabTechs:
        public IQueryable<labTech> getAllLabTechs()
        {
            IQueryable<labTech> LabTechQuery = from t in db.labTeches select t;
            return LabTechQuery;
        }

        // all labTest:
        public IQueryable<labTest> getAllLabTests()
        {
            IQueryable<labTest> LabTestQuery = from t in db.labTests select t;
            return LabTestQuery;
        }

        public IQueryable<security> getAllSecurities()
        {
            IQueryable<security> SecurityQuery = from sec in db.securities select sec;
            return SecurityQuery;
        }

     

        public Doctor getDoctorById(int id)
        {
           IQueryable<doctor> doctors= from doc in db.doctors where Convert.ToInt32(doc.iID)==id select doc;
           if (doctors.ToArray().Length == 0)
               return null;
           else if (doctors.ToArray().Length > 1)
               throw new Exception("more than one doctor found with that id - error");
           else
           {
               List<Doctor> realDoctors = toDoctors(doctors);
               return realDoctors.ElementAt(0);
           }
        }

        public Patient getPatientById(int id)
        {
            IQueryable<patient> patients = from pat in db.patients where Convert.ToInt32(pat.iID) == id select pat;
            if (patients.ToArray().Length == 0)
                return null;
            else if (patients.ToArray().Length > 1)
                throw new Exception("more than one patient found with that id - error");
            else
            {
                List<Patient> realPatients = toPatients(patients);
                return realPatients.ElementAt(0);
            }
        }


        public Visit getVisitById(int id)
        {
            IQueryable<visit> visits = from vis in db.visits where Convert.ToInt32(vis.iVisitID) == id select vis;
            if (visits.ToArray().Length == 0)
                return null;
            else if (visits.ToArray().Length > 1)
                throw new Exception("more than one visit found with that id - error");
            else
            {
                List<Visit> realVisits = toVisits(visits);
                return realVisits.ElementAt(0);
            }
        }




        public Treatment getTreatmentById(int id)
        {
            IQueryable<treatment> treatments = from t in db.treatments where Convert.ToInt32(t.itreatmentID) == id select t;
            if (treatments.ToArray().Length == 0)
                return null;
            else if (treatments.ToArray().Length > 1)
                throw new Exception("more than one treatment found with that id - error");
            else
            {
                List<Treatment> realTreatments = toTreatments(treatments);
                return realTreatments.ElementAt(0);
            }
        }




        public LabTech getLabTechById(int id)
        {
            IQueryable<labTech> techs = from tech in db.labTeches where Convert.ToInt32(tech.iID) == id select tech;
            if (techs.ToArray().Length == 0)
                return null;
            else if (techs.ToArray().Length > 1)
                throw new Exception("more than one technician found with that id - error");
            else
            {
                List<LabTech> realTechs = toLabTechs(techs);
                return realTechs.ElementAt(0);
            }
        }


        public LabTest getLabTestById(int id)
        {
            IQueryable<labTest> tests = from test in db.labTests where Convert.ToInt32(test.iLabTestID) == id select test;
            if (tests.ToArray().Length == 0)
                return null;
            else if (tests.ToArray().Length > 1)
                throw new Exception("more than one test found with that id - error");
            else
            {
                List<LabTest> realTests = toLabTests(tests);
                return realTests.ElementAt(0);
            }
        }

        
        // Queries from quries class : 



        //info about doctors:

         // the function returns a list of the male/ female doctors , as reqested
        public IQueryable<doctor> SortByGenderDoc(List<Doctor> curr, char gender)
        // assumption : in the database 70 is male and  109 or 102 is female .. 
        {
            IQueryable<doctor> current = convertDoctors(curr);

            IQueryable<doctor> doctorQuery = from doc in current
                                             where doc.cGender == Convert.ToInt32(gender)
                                           select doc;
            return doctorQuery;
        }

        // returns all the doctors with the requested range of salary
        public IQueryable<doctor> SortBySalary(List<Doctor> curr, int lowRange, int HighRange)
        {
            IQueryable<doctor> current = convertDoctors(curr);
            IQueryable<doctor> doctorQuery = from doc in current
                                             where doc.dSalary >= lowRange && doc.dSalary <= HighRange 
                                           select doc;
            return doctorQuery;
        }

        // returns all the doctors with the same requested first name
        public IQueryable<doctor> sortByFirstNameDoc(List<Doctor> curr, string FirstName)
        {
            IQueryable<doctor> current = convertDoctors(curr);
            IQueryable<doctor> doctorQuery = from doc in current
                                             where doc.sFirst_name == FirstName
                                           select doc;
            return doctorQuery;
        }

        // returns all the doctors with the same requested last name
        public IQueryable<doctor> sortByLastNameDoc(List<Doctor> curr, string LastName)
        {
            IQueryable<doctor> current = convertDoctors(curr);
            IQueryable<doctor> doctorQuery = from doc in current
                                             where doc.sLast_name == LastName
                                           select doc;
            return doctorQuery;
        }

        // returns all the doctors with the same requested id's
        public IQueryable<doctor> sortByIDDoc(List<Doctor> curr, int ID)
        {
            IQueryable<doctor> current = convertDoctors(curr);
            IQueryable<doctor> doctorQuery = from doc in current
                                             where doc.iID == ID
                                           select doc;
            return doctorQuery;
        }

        //return list of patients by doctor id
        public IQueryable<patient> getListOfPatientbyDocId( int ID)
        {
            IQueryable<patient> doctorQuery = from pat in db.patients
                                             where pat.mainDoctor == ID
                                             select pat;
            return doctorQuery;
        }

        //return list of visits the doctor is assigned to
        public IQueryable<visit> getListOfVisitsbyDocId(int ID)
        {
            IQueryable<visit> doctorQuery = from vis in db.visits
                                            where vis.assignedDoctor == ID
                                              select vis;
            return doctorQuery;
        }

        //return list of lab tests the doctor is assigned to
        public IQueryable<labTest> getListOflabTestsbyDocId(int ID)
        { 
            IQueryable<labTest> doctorQuery = from lab in db.labTests
                                            where lab.assignedDoctor == ID
                                            select lab;
            return doctorQuery;
        }


        // info about patients:

        // returns all the patients with the same requested id's
        public  IQueryable<patient> sortByIDP( List<Patient> curr, int ID)
        {
            IQueryable<patient> current = convertPatients(curr);
            IQueryable<patient> patientQuery = from pat in current
                                               where pat.iID == ID
                                                select pat;
            return patientQuery;
        }

        // returns all the patients with the same requested first name
        public IQueryable<patient> sortByFirstNameP(List<Patient> curr, string FirstName)
        {
            IQueryable<patient> current = convertPatients(curr);
            IQueryable<patient> patientQuery = from pat in current
                                               where pat.sFirst_name == FirstName
                                               select pat;
            return patientQuery;
        }

        // returns all the patients with the same requested last name
        public IQueryable<patient> sortByLastNameP(List<Patient> curr, string LastName)
        {
            IQueryable<patient> current = convertPatients(curr);
            IQueryable<patient> patientQuery = from pat in current
                                               where pat.sLast_name == LastName
                                               select pat;
            return patientQuery;
        }

        // the function returns a list of the male/ female patients , as reqested
        public IQueryable<patient> SortByGenderP(List<Patient> curr, char gender)
        {
            IQueryable<patient> current = convertPatients(curr);
            IQueryable<patient> patientQuery = from pat in current
                                               where pat.cGender == Convert.ToInt32(gender)
                                               select pat;
            return patientQuery;
        }

        //return all lab tests the patient has
        public IQueryable<labTest> getListOflabTestsbyPatId(int id)
        {
            IQueryable<labTest> patientQuery = from lab in db.labTests
                                               where lab.iPatientID == id
                                               select lab;
            return patientQuery;
        }

        // get all patients in the age in the range
        public IQueryable<patient> SortByGenderAge( int age)
        {
            IQueryable<patient> patientQuery = from pat in db.patients
                                               where age <= pat.age && pat.age <= age
                                               select pat;
            return patientQuery;
        }

        //get main doctor
        public IQueryable<int> getMainDocByPatID(int ID)
        {
            IQueryable<int> patientQuery = from pat in db.patients
                                               where pat.iID==ID
                                           select Convert.ToInt32(pat.mainDoctor);
            return patientQuery;
        }


        // info about visits: 

        // a function to get all the visits that their id is bigger then the given one 
        public IQueryable<visit> SortVisitById(List<Visit> curr, int StartID)
        {
            IQueryable<visit> currentvisitQuery = convertVisits(curr);
            IQueryable<visit> visitQuery = from vis in currentvisitQuery where vis.iVisitID >= StartID select vis;
            return visitQuery;
        }

        // return all the visits that started after the given date
        public IQueryable<visit> SortVisitByDate(List<Visit> curr, DateTime date)
        {
            IQueryable<visit> currentvisitQuery = convertVisits(curr);
            IQueryable<visit> visitQuery = from vis in currentvisitQuery where vis.dateOfStart >= date select vis;
            return visitQuery;
        }

        //gets a patient's id and returns the list of the visits he made (not sorted)
        public IQueryable<visit> VisitsMadeByPatient(List<Visit> curr, int id)
        {
            IQueryable<visit> current = convertVisits(curr);
            IQueryable<visit> visitQuery = from vis in current where vis.iPatientID == id select vis;
            return visitQuery;
        }

        /* the function gets a list of all the visits made , visitId smaller and higher and 
        returns a list of the visits in the given range  */
        public IQueryable<visit> SortByVisitsId(List<Visit> curr, int startId, int endId)
        {
            IQueryable<visit> current = convertVisits(curr);
            IQueryable<visit> visitQuery = from vis in current
                                           where vis.iVisitID <= startId && vis.iVisitID >= endId
                                           select vis;
            return visitQuery;
        }

        public IQueryable<visit> SortByVisitsDay(List<Visit> curr, DateTime date)
        /* the function gets a list of all the visits made and a date,
            returns a list of the visits happened at the exact quested date  */
        {
            IQueryable<visit> current = convertVisits(curr);
            IQueryable<visit> visitQuery = from vis in current where vis.dateOfStart == date select vis;
            return visitQuery;
        }



        // info about treatments

        //gets a patient's id and returns the list of the treatments made  (not sorted):
        public IQueryable<treatment> TreatmentsMadeByPatient(List<Treatment> curr, int id)
        {
            IQueryable<treatment> current = convertTreatments(curr);
            IQueryable<treatment> treatmentQuery = from t in current where t.iPatientID == id select t;
            return treatmentQuery;
        }

        // return all the treatments that started after the given date
        public IQueryable<treatment> SortTreatmentsByDate(List<Treatment> curr, DateTime date)
        {
            IQueryable<treatment> current = convertTreatments(curr);
            IQueryable<treatment> treatmentQuery = from t in current where t.dateOfStart >= date select t;
            return treatmentQuery;
        }

        // return all the treatments that ended before the given date
        public IQueryable<treatment> SortTreatmentsByDateEnd(List<Treatment> curr, DateTime date)
        {
            IQueryable<treatment> current = convertTreatments(curr);
            IQueryable<treatment> treatmentQuery = from t in current where t.dateOfStart <= date select t;
            return treatmentQuery;
        }

        // return all the treatments with the same doctor in charge
        public IQueryable<treatment> SortTreatmentsByDoctors(List<Treatment> curr,int doctorinchargeId)
        {
            IQueryable<treatment> current = convertTreatments(curr);
            IQueryable<treatment> treatmentQuery = from t in current where t.assignedDoctor == doctorinchargeId select t;
            return treatmentQuery;
        }

        // return all the treatments  that their id is bigger then the given one 
        public IQueryable<treatment> SortTreatmentsById(List<Treatment> curr , int startid)
        {
            IQueryable<treatment> current = convertTreatments(curr);
            IQueryable<treatment> treatmentQuery = from t in current where t.itreatmentID >= startid select t;
            return treatmentQuery;
        }

        //private List<Doctor> asListDoctors(IQueryable<doctor> doctors)
        //{
        //    List<Doctor> l = new List<Doctor>();
        //    foreach (doctor d in doctors)
        //    {
        //        Doctor toAdd = new Doctor(((int)d.iID), d.sFirst_name, d.sLast_name, ((double)d.dSalary), ((char)d.cGender));
        //    }
        //    return null;
        //}

        // info about test lab

        //return all lab tests after the date dateOfStart
        public IQueryable<labTest> SortLabTestsByDate(List<LabTest> curr, DateTime dateOfStart)
        {
            IQueryable<labTest> currentLabTestList = convertLabTests(curr);
            IQueryable<labTest> SortTestsByDatel = from t in currentLabTestList where dateOfStart <= t.dateOfLabTest select t;
            return SortTestsByDatel;
        }

        //return all lab test with bigger ID then Id
        public IQueryable<labTest> SortLabTestById(List<LabTest> curr, int Id)
        {
            IQueryable<labTest> currentLabTestList = convertLabTests(curr);
            IQueryable<labTest> SortTestsByID = from t in currentLabTestList where Id <= t.iLabTestID select t;
            return SortTestsByID;
        }
        public IQueryable<labTest> SortLabTestByPatId(List<LabTest> curr, int Id)
        {
            IQueryable<labTest> currentLabTestList = convertLabTests(curr);
            IQueryable<labTest> SortTestsByID = from t in currentLabTestList where Id == t.iPatientID select t;
            return SortTestsByID;
        }
        
     

        // info about tech lab

        //return all lab tech with the first name FirstName
        public IQueryable<labTech> sortByFirstNameTech(List<LabTech> curr, string FirstName)
        {
            IQueryable<labTech> currentLabTechList = convertLabTechs(curr);
            IQueryable<labTech> SortedByFirstNamel = from t in currentLabTechList where FirstName == t.sFirst_name select t;
            return SortedByFirstNamel;
        }

        //return all lab tech with the last name FLastName
        public IQueryable<labTech> sortByLastNameTech(List<LabTech> curr, string FLastName)
        {
            IQueryable<labTech> currentLabTechList = convertLabTechs(curr);
            IQueryable<labTech> SortedByLastNamel = from t in currentLabTechList where FLastName == t.sLast_name select t;
            return SortedByLastNamel;
        }

        //return all lab tech with the id bigger then Id
        public IQueryable<labTech> sortByIDTech(List<LabTech> curr, int Id)
        {
            IQueryable<labTech> currentLabTechList = convertLabTechs(curr);
            IQueryable<labTech> sortByIDl = from t in currentLabTechList where Id <= t.iID select t;
            return sortByIDl;
        }

        //return all lab tech with salary in the given range 
        public IQueryable<labTech> SortBySalaryTech(List<LabTech> curr, int lowRange, int HighRange)
        {
            IQueryable<labTech> currentLabTechList = convertLabTechs(curr);
            IQueryable<labTech> SortedBySalary = from t in currentLabTechList where lowRange <= t.dSalary && t.dSalary <= HighRange select t;
            return SortedBySalary;
        }

        //return all lab tech with the same gender given
        public IQueryable<labTech> sortByGenderTech(List<LabTech> curr, char gender)
        {
            IQueryable<labTech> currentLabTechList = convertLabTechs(curr);
            IQueryable<labTech> sortedByGenderl = from t in currentLabTechList where gender == t.cGender select t; 
            return sortedByGenderl;
        }

        //return all test lab the tech lab is assigned to:
        public IQueryable<labTest> getlistOfTestLabByTechId( int Id)
        {
            IQueryable<labTest> SortTestsByID = from t in db.labTests where Id == t. iLabTechId select t;
            return SortTestsByID;
        }


        #endregion Queries

        #region Doctor

        public List<Doctor> allDoctors()
        {
            IQueryable<doctor> tableDocs = getAllDoctors();
            return toDoctors(tableDocs);
        }

        public List<Doctor> toDoctors(IQueryable<doctor> tableDocs)
        {
            List<Doctor> realDocs = new List<Doctor>();
            foreach (doctor tableDoc in tableDocs)
            {
                Doctor newDoc = new Doctor(Convert.ToInt32(tableDoc.iID), tableDoc.sFirst_name, tableDoc.sLast_name, Convert.ToDouble(tableDoc.dSalary), Convert.ToChar(tableDoc.cGender), Convert.ToBoolean(tableDoc.needsToBeNotified));
                realDocs.Add(newDoc);
            }
            return realDocs;
        }

        public Boolean hasPatient(int doctorId,int patientId)
        {
            foreach(patient curr in db.patients)
            {
                if(curr.iID==patientId)
                {
                    if (curr.mainDoctor == doctorId) return true;
                    return false;
                }
            }
            return false;
        }
        public void addPatientToDoctor(int doctorsID, int patientsID)
        {
            foreach (patient curr in db.patients)
            {
                if (curr.iID == patientsID)
                {
                    curr.mainDoctor = doctorsID;
                    break;
                }
            }
        }
        public void removePatientFromDoctor(int doctorsID, int patientsID)
        {
            foreach (patient curr in db.patients)
            {
                if (curr.iID == patientsID)
                {
                    curr.mainDoctor = 100000001;
                    break;
                }
            }
        }

        public IQueryable<treatment> getListOfTreatmentsByDocId(int p)
        {
            IQueryable<treatment> Query = from pat in db.treatments
                                          where pat.assignedDoctor == p
                                          select pat;
            return Query;
        }
   
        #endregion Doctor 

        #region Patient
        public List<Patient> allPatients()
        {
            IQueryable<patient> tablePats = getAllPatients();
            return toPatients(tablePats);
        }

        public List<Patient> toPatients(IQueryable<patient> tablePats)
        {
            List<Patient> realPats = new List<Patient>();
            foreach (patient tablePat in tablePats)
            {
                Patient newPat = new Patient(Convert.ToInt32(tablePat.iID), tablePat.sFirst_name, tablePat.sLast_name, Convert.ToInt32(tablePat.age), Convert.ToChar(tablePat.cGender), Convert.ToInt32(tablePat.mainDoctor));
                realPats.Add(newPat);
            }
            return realPats;
        }

        public void addVisitToPatient(int patientID, int visitID)
        {
            foreach (visit curr in db.visits)
            {
                if(visitID==curr.iVisitID)
                {
                    curr.iPatientID = patientID;
                }
            }
        }

        public bool patientHasVisit(int patientID, int visitID)
        {
            throw new NotImplementedException();
        }

        public void removeVisitFromPatient(int patientID, int visitID)
        {
            foreach(visit curr in db.visits)
            {
                if (visitID == curr.iVisitID) curr.iPatientID = 100000001;
            }
        }

        public IQueryable<treatment> getListOfTreatmentsbyPatientId(int p)
        {
            IQueryable<treatment> Query = from pat in db.treatments
                                          where pat.iPatientID == p
                                          select pat;
            return Query;
        }

        public IQueryable<visit> getListOfVisitssbyPatientId(int p)
        {
            IQueryable<visit> Query = from pat in db.visits
                                      where pat.iPatientID == p
                                      select pat;
            return Query;
        }

        #endregion Patient

        #region LabTech
        public List<LabTech> allLabTechs()
        {
            IQueryable<labTech> tableTechs = getAllLabTechs();
            return toLabTechs(tableTechs);
        }

        public List<LabTech> toLabTechs(IQueryable<labTech> tableTechs)
        {
            List<LabTech> realTechs = new List<LabTech>(); ;
            foreach (labTech tableTech in tableTechs)
            {
                LabTech newTech = new LabTech(Convert.ToInt32(tableTech.iID), tableTech.sFirst_name, tableTech.sLast_name, Convert.ToDouble(tableTech.dSalary), Convert.ToChar(tableTech.cGender));
                realTechs.Add(newTech);
            }
            return realTechs;
        }
        #endregion LabTech

        #region Visit

        public List<Visit> allVisits()
        {
            IQueryable<visit> tableVisits = getAllVisits();
            return toVisits(tableVisits);
        }

        public List<Visit> toVisits(IQueryable<visit> tableVisits)
        {
            List<Visit> realVisits = new List<Visit>(); ;
            foreach (visit tableVisit in tableVisits)
            {
                Doctor assignedDoctor=getDoctorById(Convert.ToInt32(tableVisit.assignedDoctor));
                Visit newVisit = new Visit(Convert.ToInt32(tableVisit.iVisitID), Convert.ToDateTime(tableVisit.dateOfStart), assignedDoctor, Convert.ToInt32(tableVisit.iPatientID), tableVisit.sDoctor_notes,null);
                realVisits.Add(newVisit);
            }
            return realVisits;
        }

        public bool visitHasTreatment(int visitID, int treatmentID)
        {
            foreach(treatment curr in db.treatments)
            {
                if (curr.itreatmentID == treatmentID)
                    if (curr.iVisitId == visitID) return true;
                    else return false;
            }
            return false;
        }
        public void addTreatmentToVisit(int visitID, int treatmentID)
        {
            foreach (treatment curr in db.treatments)
            {
                if (curr.itreatmentID == treatmentID)
                    curr.iVisitId = visitID;
                    
            }
        }

        public void removeTreatmentFromVisit(int visitID, int treatmentID)
        {
            foreach (treatment curr in db.treatments)
            {
                if (curr.itreatmentID == treatmentID)
                    curr.iVisitId = 0;

            }
        }

        public IQueryable<treatment> getTreatmentByVisit(int p)
        {
            IQueryable<treatment> Query = from pat in db.treatments
                                      where pat.assignedDoctor == p
                                      select pat;
            return Query;
        }

        #endregion Visit 

        #region Treatment

        public List<Treatment> allTreatments()
        {
            IQueryable<treatment> tableTreatments = getAlltreatments();
            return toTreatments(tableTreatments);
        }

        public List<Treatment> toTreatments(IQueryable<treatment> tableTreatments)
        {

            List<Treatment> realTreatments = new List<Treatment>();
            foreach (treatment tableTreatment in tableTreatments)
            {
                Doctor assignedDoctor = getDoctorById(Convert.ToInt32(tableTreatment.assignedDoctor));
                List<string> prescriptions = new List<string>();
                prescriptions.Add(tableTreatment.sPrescription);
                Treatment newTreatment = new Treatment(Convert.ToInt32(tableTreatment.itreatmentID), Convert.ToDateTime(tableTreatment.dateOfStart),
                    Convert.ToDateTime(tableTreatment.dateOfFinish), assignedDoctor, tableTreatment.sPrignosis, prescriptions, tableTreatment.sTreatments_name,
                    Convert.ToInt32(tableTreatment.iPatientID), Convert.ToInt32(tableTreatment.iVisitId));
                realTreatments.Add(newTreatment);
            }
            return realTreatments;
        }

        #endregion Treatment 
        
        #region LabTest

        public List<LabTest> allLabTests()
        {
            IQueryable<labTest> tableLabTests = getAllLabTests();
            return toLabTests(tableLabTests);
        }

        public List<LabTest> toLabTests(IQueryable<labTest> tableLabTests)
        {
            List<LabTest> realLabTests = new List<LabTest>();
            foreach (labTest tableLabTest in tableLabTests)
            {
                Doctor assignedDoctor = getDoctorById(Convert.ToInt32(tableLabTest.assignedDoctor));
                LabTest newLabTest = new LabTest(Convert.ToInt32(tableLabTest.iLabTestID), Convert.ToDateTime(tableLabTest.dateOfLabTest),
                     assignedDoctor,Convert.ToInt32( tableLabTest.iPatientID), tableLabTest.sDoctorNotes);
                newLabTest.sResults = tableLabTest.sResults;
                realLabTests.Add(newLabTest);
            }
            return realLabTests;
        }

        #endregion LabTest 

        #region Security

        public List<Securities> allSecurities()
        {
            IQueryable<security> tableSecurities = getAllSecurities();
            return toSecurities(tableSecurities);
        }

        public List<Securities> toSecurities(IQueryable<security> tableSecurities)
        {
            List<Securities> realSecurities = new List<Securities>() ;
            foreach (security security in tableSecurities)
            {
                Securities newSecurity = new Securities(Convert.ToInt32(security.id), Convert.ToInt32(security.pass), security.sauto);
                realSecurities.Add(newSecurity);
            }
            return realSecurities;
        }

        #endregion Security 

        #region convert to ints
        public List<int> toInt(IQueryable<patient> queryable)
        {
            List<int> ans = new List<int>();
            foreach (patient pat in queryable)
            {
                ans.Add((int)pat.iID);
            }
            return ans;
        }

        public List<int> toInt(IQueryable<treatment> queryable)
        {
            List<int> ans = new List<int>();
            foreach (treatment pat in queryable)
            {
                ans.Add((int)pat.itreatmentID);
            }
            return ans;
        }

        public List<int> toInt(IQueryable<visit> queryable)
        {
            List<int> ans = new List<int>();
            foreach (visit pat in queryable)
            {
                ans.Add((int)pat.iVisitID);
            }
            return ans;
        }
        public List<int> toInt(IQueryable<labTest> queryable)
        {
            List<int> ans = new List<int>();
            foreach (labTest pat in queryable)
            {
                ans.Add((int)pat.iLabTestID);
            }
            return ans;
        }

        #endregion

        #region XML
        //exports
        public void exportXMLDoctors(List<Doctor> Doc, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.doctorsListToXml(Doc, fileName);
        }
        public void exportXMLTechs(List<LabTech> tec, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.techsListToXml(tec, fileName);
        }
        public void exportXMLTests(List<LabTest> test, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.testsListToXml(test, fileName);
        }
        public void exportXMLPatients(List<Patient> Pat, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.patientListToXml(Pat, fileName);
        }
        public void exportXMLVisits(List<Visit> Vis, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.visitListToXml(Vis, fileName);
        }
        public void exportXMLTreatments(List<Treatment> Trm, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.treatmentListToXml(Trm, fileName);
        }
        public void exportXMLSecurity(List<Securities> security, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.SecurityToXml(security, fileName);
        }
       
        #endregion

        #region convert to table
        private IQueryable<doctor> convertDoctors(List<Doctor> curr)
        {
            IQueryable<doctor> t=null;
            List<doctor> ans = new List<doctor>();
            foreach(Doctor d in curr)
            {
                t = from k in db.doctors where d.iID == k.iID select k;
                foreach(doctor tmp in t)
                {
                    ans.Add(tmp);
                }
            }
                return ans.AsQueryable<doctor>();
        }

        private IQueryable<patient> convertPatients(List<Patient> curr)
        {
            IQueryable<patient> t = null;
            List<patient> ans = new List<patient>();
            foreach (Patient d in curr)
            {
                t = from k in db.patients where d.iID == k.iID select k;
                foreach (patient tmp in t)
                {
                    ans.Add(tmp);
                }
            }
            return ans.AsQueryable<patient>();
        }

        private IQueryable<visit> convertVisits(List<Visit> curr)
        {
            IQueryable<visit> t = null;
            List<visit> ans = new List<visit>();
            foreach (Visit d in curr)
            {
                t = from k in db.visits where d.iVisitID == k.iVisitID select k;
                foreach (visit tmp in t)
                {
                    ans.Add(tmp);
                }
            }
            return ans.AsQueryable<visit>();
        }

        private IQueryable<treatment> convertTreatments(List<Treatment> curr)
        {
            IQueryable<treatment> t = null;
            List<treatment> ans = new List<treatment>();
            foreach (Treatment d in curr)
            {
                t = from k in db.treatments where d.iTreatmentID == k.itreatmentID select k;
                foreach (treatment tmp in t)
                {
                    ans.Add(tmp);
                }
            }
            return ans.AsQueryable<treatment>();
        }

        private IQueryable<labTech> convertLabTechs(List<LabTech> curr)
        {
            IQueryable<labTech> t = null;
            List<labTech> ans = new List<labTech>();
            foreach (LabTech d in curr)
            {
                t = from k in db.labTeches where d.iID == k.iID select k;
                foreach (labTech tmp in t)
                {
                    ans.Add(tmp);
                }
            }
            return ans.AsQueryable<labTech>();
        }

        private IQueryable<labTest> convertLabTests(List<LabTest> curr)
        {
            IQueryable<labTest> t = null;
            List<labTest> ans = new List<labTest>();
            foreach (LabTest d in curr)
            {
                t = from k in db.labTests where d.iLabTestID == k.iLabTestID select k;
                foreach (labTest tmp in t)
                {
                    ans.Add(tmp);
                }
            }
            return ans.AsQueryable<labTest>();
        }
        #endregion

        #region Excel
        public void ExporttoExcel_Doctors(List<Doctor> lst)
        {
            ExcelExports ex = new ExcelExports();
            ex.ExporttoExcel_Doctors(lst);
        }

        public void ExporttoExcel_Patients(List<Patient> lst)
        {
            ExcelExports ex = new ExcelExports();
            ex.ExporttoExcel_Patients(lst);
        }

        public void ExporttoExcel_Visits(List<Visit> lst)
        {
            ExcelExports ex = new ExcelExports();
            ex.ExporttoExcel_Visits(lst);
        }

        public void ExporttoExcel_Treatments(List<Treatment> lst)
        {
            ExcelExports ex = new ExcelExports();
            ex.ExporttoExcel_Treatments(lst);
        }

        public void ExporttoExcel_LabTech(List<LabTech> lst)
        {
            ExcelExports ex = new ExcelExports();
            ex.ExporttoExcel_LabTech(lst);
        }
        public void ExporttoExcel_LabTest(List<LabTest> lst)
        {
            ExcelExports ex = new ExcelExports();
            ex.ExporttoExcel_LabTest(lst);
        }
        #endregion

        #region Pdf



        #endregion
    }
}
