﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using System.Web;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.IO;

namespace DAL
{
    public class ExcelExports
    {
        //fields:
        SQL_DAL_implementation onnectToDB;
        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet;
        object misValue = System.Reflection.Missing.Value;
        private static String path = Directory.GetCurrentDirectory().ToString() + "\\Excel\\";
        //  fileName = path+fileName+".XML";

        //constructor:
        public ExcelExports()
        {
            this.onnectToDB = new SQL_DAL_implementation();
            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.StandardWidth = 12;
        }


        //methods:

        //create excel for doctors:
        public void ExporttoExcel_Doctors(List<Doctor> curr)
        {

            //create columns :
            xlWorkSheet.Cells[1, 2] = "Gender";
            xlWorkSheet.Cells[1, 3] = "ID";
            xlWorkSheet.Cells[1, 4] = "First_name";
            xlWorkSheet.Cells[1, 5] = "Last_name";
            xlWorkSheet.Cells[1, 6] = "Salary";

            //cGender	iID	sFirst_name	sLast_name	dSalary

            //adding data:
            int row = 1;
            foreach (Doctor doctor in curr)
            {
                xlWorkSheet.Cells[row + 1, 1] = row.ToString();
                xlWorkSheet.Cells[row + 1, 2] = doctor.cGender.ToString();
                xlWorkSheet.Cells[row + 1, 3] = doctor.iID.ToString();
                xlWorkSheet.Cells[row + 1, 4] = doctor.sFirst_name.ToString();
                xlWorkSheet.Cells[row + 1, 5] = doctor.sLast_Name.ToString();
                xlWorkSheet.Cells[row + 1, 6] = doctor.dSalary.ToString();
                row++;
            }


            string Filename = "Doctors";
            exportToFile(Filename);

        }

        //create excel for patients:
        public void ExporttoExcel_Patients(List<Patient> curr)
        {
            //create columns :
            xlWorkSheet.Cells[1, 2] = "Gender";
            xlWorkSheet.Cells[1, 3] = "ID";
            xlWorkSheet.Cells[1, 4] = "First_name";
            xlWorkSheet.Cells[1, 5] = "Last_name";
            xlWorkSheet.Cells[1, 6] = "Age";
            xlWorkSheet.Cells[1, 7] = "main Doctor";

            //cGender	iID	sFirst_name	sLast_name	iAge	mainDoctor


            //adding data:
            int row = 1;
            foreach (Patient patient in curr)
            {
                xlWorkSheet.Cells[row + 1, 1] = row.ToString();
                xlWorkSheet.Cells[row + 1, 2] = patient.cGender.ToString();
                xlWorkSheet.Cells[row + 1, 3] = patient.iID.ToString();
                xlWorkSheet.Cells[row + 1, 4] = patient.sFirst_name.ToString();
                xlWorkSheet.Cells[row + 1, 5] = patient.sLast_Name.ToString();
                xlWorkSheet.Cells[row + 1, 6] = patient.iAge.ToString();
                xlWorkSheet.Cells[row + 1, 7] = patient.MainDoctor.ToString();

                row++;
            }

            string Filename = "Patients";
            exportToFile(Filename);

            // MessageBox.Show("File created !");
        }

        //create excel for visits:
        public void ExporttoExcel_Visits(List<Visit> curr)
        {
            //create columns :
            xlWorkSheet.Cells[1, 2] = "assigned doctor";
            xlWorkSheet.Cells[1, 3] = "VisitID";
            xlWorkSheet.Cells[1, 4] = "PatientID";
            xlWorkSheet.Cells[1, 5] = "date of start";
            xlWorkSheet.Cells[1, 6] = "sDoctor_notes";


            //assigned doctor	iVisitID	iPatientID	date of start	sDoctor_notes

            //adding data:
            int row = 1;
            foreach (Visit visit in curr)
            {
                xlWorkSheet.Cells[row + 1, 1] = row.ToString();
                xlWorkSheet.Cells[row + 1, 2] = visit.AssignedDoctor.ToString();
                xlWorkSheet.Cells[row + 1, 3] = visit.iVisitID.ToString();
                xlWorkSheet.Cells[row + 1, 4] = visit.iPatientId.ToString();
                xlWorkSheet.Cells[row + 1, 5] = visit.dateOfVisit.ToString();
                xlWorkSheet.Cells[row + 1, 6] = visit.sDoctorNotes.ToString();
                row++;
            }

            string Filename = "Visits";
            exportToFile(Filename);

            // MessageBox.Show("File created !");
        }

        //create excel for treatments:
        public void ExporttoExcel_Treatments(List<Treatment> curr)
        {
            //create columns :
            xlWorkSheet.Cells[1, 2] = "assigned doctor";
            xlWorkSheet.Cells[1, 3] = "treatmentID";
            xlWorkSheet.Cells[1, 4] = "PatientID";
            xlWorkSheet.Cells[1, 5] = "date of start";
            xlWorkSheet.Cells[1, 6] = "date of finish";
            xlWorkSheet.Cells[1, 7] = "Treatments name";
            xlWorkSheet.Cells[1, 8] = "Prignosis";
            xlWorkSheet.Cells[1, 9] = "Prescription";


            //assigned doctor	itreatmentID	iPatientID	date of start	date of finish	sTreatments_name	sPrignosis	sPrescription


            //adding data:
            int row = 1;
            foreach (Treatment treatment in curr)
            {
                xlWorkSheet.Cells[row + 1, 1] = row.ToString();
                xlWorkSheet.Cells[row + 1, 2] = treatment.createdByDoctor.ToString();
                xlWorkSheet.Cells[row + 1, 3] = treatment.iTreatmentID.ToString();
                xlWorkSheet.Cells[row + 1, 4] = treatment.iPatientID.ToString();
                xlWorkSheet.Cells[row + 1, 5] = treatment.DateOfStart.ToString();
                xlWorkSheet.Cells[row + 1, 6] = treatment.DateOfFinish.ToString();
                xlWorkSheet.Cells[row + 1, 7] = treatment.sTreatmentName.ToString();
                xlWorkSheet.Cells[row + 1, 8] = treatment.sPrognosis.ToString();
                xlWorkSheet.Cells[row + 1, 9] = treatment.sPrescriptions.ToString();
                row++;
            }

            string Filename = "Treatments";
            exportToFile(Filename);

            // MessageBox.Show("File created !");
        }

        //create excel for Labtech:
        public void ExporttoExcel_LabTech(List<LabTech> curr)
        {
            //create columns :
            xlWorkSheet.Cells[1, 2] = "Gender";
            xlWorkSheet.Cells[1, 3] = "ID";
            xlWorkSheet.Cells[1, 4] = "First_name";
            xlWorkSheet.Cells[1, 5] = "Last_name";
            xlWorkSheet.Cells[1, 6] = "Salary";

            //cGender	iID	sFirst_name	sLast_name	dSalary

            //adding data:
            int row = 1;
            foreach (LabTech labtech in curr)
            {
                xlWorkSheet.Cells[row + 1, 1] = row.ToString();
                xlWorkSheet.Cells[row + 1, 2] = labtech.cGender.ToString();
                xlWorkSheet.Cells[row + 1, 3] = labtech.iID.ToString();
                xlWorkSheet.Cells[row + 1, 4] = labtech.sFirst_name.ToString();
                xlWorkSheet.Cells[row + 1, 5] = labtech.sLast_Name.ToString();
                xlWorkSheet.Cells[row + 1, 6] = labtech.dSalary.ToString();
                row++;
            }


            string Filename = "LabTechs";
            exportToFile(Filename);

            // MessageBox.Show("File created !");
        }

        //create excel for Labtest:
        public void ExporttoExcel_LabTest(List<LabTest> curr)
        {
            //create columns :
            xlWorkSheet.Cells[1, 2] = "assigned doctor";
            xlWorkSheet.Cells[1, 3] = "LabTestID";
            xlWorkSheet.Cells[1, 4] = "PatientID";
            xlWorkSheet.Cells[1, 5] = "date Of Lab Test";
            xlWorkSheet.Cells[1, 6] = "Lab Tech Id";
            xlWorkSheet.Cells[1, 7] = "Doctor Notes";
            xlWorkSheet.Cells[1, 8] = "Results";

            //assigned doctor	iLabTestID	iPatientID	dateOfLabTest	iLabTechId	sDoctorNotes	sResults


            //adding data:
            int row = 1;
            foreach (LabTest labtest in curr)
            {
                xlWorkSheet.Cells[row + 1, 1] = row.ToString();
                xlWorkSheet.Cells[row + 1, 2] = labtest.AssignedDoctor.ToString();
                xlWorkSheet.Cells[row + 1, 3] = labtest.iLabTestID.ToString();
                xlWorkSheet.Cells[row + 1, 4] = labtest.iPatientId.ToString();
                xlWorkSheet.Cells[row + 1, 5] = labtest.dateOfLabTest.ToString();
                xlWorkSheet.Cells[row + 1, 6] = labtest.iLabTechId.ToString();
                xlWorkSheet.Cells[row + 1, 7] = labtest.sDoctorNotes.ToString();
                xlWorkSheet.Cells[row + 1, 8] = labtest.sResults.ToString();
                row++;
            }


            string Filename = "LabTests";
            exportToFile(Filename);

            // MessageBox.Show("File created !");
        }

        //create excel for Securities:
        public void ExporttoExcel_Securities(List<Securities> curr)
        {
            //create columns :
            xlWorkSheet.Cells[1, 2] = "id ";
            xlWorkSheet.Cells[1, 3] = "pass";
            xlWorkSheet.Cells[1, 4] = "authority";

            //id	pass	sauto



            //adding data:
            int row = 1;
            foreach (Securities securities in curr)
            {
                xlWorkSheet.Cells[row + 1, 1] = row.ToString();
                xlWorkSheet.Cells[row + 1, 2] = securities._id.ToString();
                xlWorkSheet.Cells[row + 1, 3] = securities._ipass.ToString();
                xlWorkSheet.Cells[row + 1, 4] = securities._sauto.ToString();
                row++;
            }


            string Filename = "Securities";
            exportToFile(Filename);

            // MessageBox.Show("File created !");
        }


        private void exportToFile(string fileName)
        {
            xlWorkBook.SaveAs(path + fileName + ".xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            releaseObject(xlApp);
            releaseObject(xlWorkBook);
            releaseObject(xlWorkSheet);
            // MessageBox.Show("File created !");
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }



    }
}
