﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using BL_Backend;
using System.Threading.Tasks;

namespace DAL
{ 
    class XML_DAL
    {
        //path to the directory to save
        private static String path = Directory.GetCurrentDirectory().ToString() + "\\XML\\";
        private String password = "1234abcd";
        public XML_DAL()
        {
            return;
        }

        #region XML Export
        public void doctorsListToXml(List<Doctor> doctor, String fileName)
        {
            String encryption = fileName + ".XML";
            fileName = path+fileName+".XML";
            XmlDocument xmlDoc = new XmlDocument();
            if (File.Exists(@fileName))
            {
                File.Delete(@fileName);
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Doctor>));
            using (StreamWriter writer = File.AppendText(@fileName))
            {
                serializer.Serialize(writer, doctor);
            }
            //Encryptes only the dataBase, all the other outputs it does not encrypts
            if(encryption.Equals("Doctor.XML"))
            {
                EncryptDycript.EncryptFile(path + "Doctor.XML", path + "EncryptionDoctor.XML", password);
            }
        }

        public void techsListToXml(List<LabTech> tech, String fileName)
        {
            String encryption = fileName + ".XML";
            fileName = path + fileName + ".XML";
            XmlDocument xmlDoc = new XmlDocument();
            if (File.Exists(@fileName))
            {
                File.Delete(@fileName);
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<LabTech>));
            using (StreamWriter writer = File.AppendText(@fileName))
            {
                serializer.Serialize(writer, tech);
            }
            //Encryptes only the dataBase, all the other outputs it does not encrypts
            if (encryption.Equals("Techs.XML"))
            {
                EncryptDycript.EncryptFile(path + "Techs.XML", path + "EncryptionTechs.XML", password);
            }
        }
        public void testsListToXml(List<LabTest> Test, String fileName)
        {
            String encryption = fileName + ".XML";
            fileName = path + fileName + ".XML";
            XmlDocument xmlDoc = new XmlDocument();
            if (File.Exists(@fileName))
            {
                File.Delete(@fileName);
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<LabTest>));
            using (StreamWriter writer = File.AppendText(@fileName))
            {
                serializer.Serialize(writer, Test);
            }
            //Encryptes only the dataBase, all the other outputs it does not encrypts
            if (encryption.Equals("Tests.XML"))
            {
                EncryptDycript.EncryptFile(path + "Tests.XML", path + "EncryptionTests.XML", password);
            }
        }
        public void patientListToXml(List<Patient> patient, String fileName)
        {
            String encryption = fileName + ".XML";
            fileName = @path + fileName+".XML";
            XmlDocument xmlDoc = new XmlDocument();
            if (File.Exists(@fileName))
            {
                File.Delete(@fileName);
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Patient>));
            using (StreamWriter writer = File.AppendText(@fileName))
            {
                serializer.Serialize(writer, patient);
            }
            //Encryptes only the dataBase, all the other outputs it does not encrypts
            if (encryption.Equals("Patient.XML"))
            {
                EncryptDycript.EncryptFile(path + "Patient.XML", path + "EncryptionPatient.XML", password);
            }
        }

        public void visitListToXml(List<Visit> visit, String fileName)
        {
            String encryption = fileName + ".XML";
            fileName = @path + fileName+".XML";
            XmlDocument xmlDoc = new XmlDocument();
            if (File.Exists(@fileName))
            {
                File.Delete(@fileName);
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Visit>));
            using (StreamWriter writer = File.AppendText(@fileName))
            {
                serializer.Serialize(writer, visit);
            }
            //Encryptes only the dataBase, all the other outputs it does not encrypts
            if (encryption.Equals("Visit.XML"))
            {
                EncryptDycript.EncryptFile(path + "Visit.XML", path + "EncryptionVisit.XML", password);
            }
        }

        public void treatmentListToXml(List<Treatment> treatment, String fileName)
        {
            String encryption = fileName + ".XML";
            fileName = @path + fileName+".XML";
            XmlDocument xmlDoc = new XmlDocument();
            if (File.Exists(@fileName))
            {
                File.Delete(@fileName);
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Treatment>));
            using (StreamWriter writer = File.AppendText(@fileName))
            {
                serializer.Serialize(writer, treatment);
            }
            //Encryptes only the dataBase, all the other outputs it does not encrypts
            if (encryption.Equals("Treatment.XML"))
            {
                EncryptDycript.EncryptFile(path + "Treatment.XML", path + "EncryptionTreatment.XML", password);
            }
        }
        public void SecurityToXml(List<Securities> security, String fileName)
        {
            String encryption = fileName + ".XML";
            fileName = @path + fileName + ".XML";
            XmlDocument xmlDoc = new XmlDocument();
            if (File.Exists(@fileName))
            {
                File.Delete(@fileName);
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Securities>));
            using (StreamWriter writer = File.AppendText(@fileName))
            {
                serializer.Serialize(writer, security);
            }
            //Encryptes only the dataBase, all the other outputs it does not encrypts
            if (encryption.Equals("Security.XML"))
            {
                EncryptDycript.EncryptFile(path + "Security.XML", path + "EncryptionSecurity.XML", password);
            }
        }
        #endregion
        #region XML Import
        public List<Doctor> doctorsFromXML(string fileName)
        {
            string toDec = path+"Encryption" + fileName+".XML";
            fileName = path+ fileName + ".XML";
            try
            {
                EncryptDycript.DecryptFile(toDec, fileName, password);

            }
            catch (Exception e) { }
            if (!(File.Exists(fileName)))
            {
                return new List<Doctor>();
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Doctor>));
            using (StreamReader reader = new StreamReader(fileName))
            {
                try
                {
                    return (List<Doctor>)serializer.Deserialize(reader);
                }
                catch
                {
                    Console.WriteLine("Could not import data");
                    return new List<Doctor>();
                }
            }
        }
        public List<LabTech> techsFromXML(string fileName)
        {
            string toDec = path + "Encryption" + fileName + ".XML";
            fileName = path + fileName + ".XML";
            try
            {
                EncryptDycript.DecryptFile(toDec, fileName, password);

            }
            catch (Exception e) { }
            if (!(File.Exists(fileName)))
            {
                return new List<LabTech>();
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<LabTech>));
            using (StreamReader reader = new StreamReader(fileName))
            {
                try
                {
                    return (List<LabTech>)serializer.Deserialize(reader);
                }
                catch
                {
                    Console.WriteLine("Could not import data");
                    return new List<LabTech>();
                }
            }
        }
        public List<LabTest> testsFromXML(string fileName)
        {
            string toDec = path + "Encryption" + fileName + ".XML";
            fileName = path + fileName + ".XML";
            try
            {
                EncryptDycript.DecryptFile(toDec, fileName, password);

            }
            catch (Exception e) { }
            if (!(File.Exists(fileName)))
            {
                return new List<LabTest>();
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<LabTest>));
            using (StreamReader reader = new StreamReader(fileName))
            {
                try
                {
                    return (List<LabTest>)serializer.Deserialize(reader);
                }
                catch
                {
                    Console.WriteLine("Could not import data");
                    return new List<LabTest>();
                }
            }
        }

        public List<Patient> patientsFromXML(string fileName)
        {
            string toDec = path + "Encryption" + fileName + ".XML";
            fileName = path + fileName + ".XML";
            try
            {
                EncryptDycript.DecryptFile(toDec, fileName, password);

            }
            catch (Exception e) { }
            if (!(File.Exists(fileName)))
            {
                return new List<Patient>();
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Patient>));
            using (StreamReader reader = new StreamReader(fileName))
            {
                try
                {
                    return (List<Patient>)serializer.Deserialize(reader);
                }
                catch
                {
                    Console.WriteLine("Could not import data");
                    return new List<Patient>();
                }
            }
        }

        public List<Visit> visitsFromXML(string fileName)
        {
            string toDec = path + "Encryption" + fileName + ".XML";
            fileName = path + fileName + ".XML";
            try
            {
                EncryptDycript.DecryptFile(toDec, fileName, password);

            }
            catch (Exception e) { }
            if (!(File.Exists(fileName)))
            {
                return new List<Visit>();
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Visit>));
            using (StreamReader reader = new StreamReader(fileName))
            {
                try
                {
                    return (List<Visit>)serializer.Deserialize(reader);
                }
                catch
                {
                    Console.WriteLine("Could not import data");
                    return new List<Visit>();
                }
            }
        }

        public List<Treatment> treatmentsFromXML(string fileName)
        {
            string toDec = path + "Encryption" + fileName + ".XML";
            fileName = path + fileName + ".XML";
            try
            {
                EncryptDycript.DecryptFile(toDec, fileName, password);

            }
            catch (Exception e) { }
            if (!(File.Exists(fileName)))
            {
                return new List<Treatment>();
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Treatment>));
            using (StreamReader reader = new StreamReader(fileName))
            {
                try
                {
                    return (List<Treatment>)serializer.Deserialize(reader);
                }
                catch
                {
                    Console.WriteLine("Could not import data");
                    return new List<Treatment>();
                }
            }
        }
        public List<Securities> SecurityFromXML(string fileName)
        {
            string toDec = path + "Encryption" + fileName + ".XML";
            fileName = path + fileName + ".XML";
            try
            {
                EncryptDycript.DecryptFile(toDec, fileName, password);

            }
            catch (Exception e) { }
            if (!(File.Exists(fileName)))
            {
                return new List<Securities>();
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<Securities>));
            using (StreamReader reader = new StreamReader(fileName))
            {
                try
                {
                    return (List<Securities>)serializer.Deserialize(reader);
                }
                catch
                {
                    Console.WriteLine("Could not import data");
                    return new List<Securities>();
                }
            }
        }
        #endregion
    }
}
