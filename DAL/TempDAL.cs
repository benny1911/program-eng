﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using System.Xml;
using System.IO;
using System.Xml.Linq;
//using 

namespace DAL
{
    public class TempDAL
    {

        public List<Doctor> Doctors { get; private set; }
        public List<Patient> Patients { get; private set; }
        public List<Visit> Visits { get; private set; }
        public List<Treatment> Treatments { get; private set; }
        public List<Securities> Security { get; private set; }
        public List<LabTech> Labtechs { get; private set; }
        public List<LabTest> Labtests { get; private set; }

        
        #region functions
        public TempDAL()
        {
            Doctors=new List<Doctor>();
            Patients=new List<Patient>();
            Visits=new List<Visit>();
            Treatments=new List<Treatment>();
            Security = new List<Securities>();
            Labtechs = new List<LabTech>();
            Labtests = new List<LabTest>();
            importXMLDoctors("Doctor");
            importXMLPatients("Patient");
            importXMLVisits("Visit");
            importXMLTreatments("Treatment");
            importXMLSecurity("Security");
            importXMLTechs("Techs");
            importXMLTests("Tests");
        }

        //adds a new doctor to the database.
        public void addDoctor(Doctor doctor)
        {
            Doctors.Add(doctor);
        }

        public void addLabTech(LabTech tech)
        {
            Labtechs.Add(tech);
        }

        public void addLabTest(LabTest test)
        {
            Labtests.Add(test);
        }

        //adds a new patient to the database.
        public void addPatient(Patient patient)
        {
            Patients.Add(patient);

            foreach (Doctor dctCurrent in  Doctors)
            {
                if (dctCurrent.iID == patient.MainDoctor)
                {
                    dctCurrent.addPatient(patient);
                }
            }
        }

        //adds a new visit to the database.
        public void addVisit(Visit visit)
        {
            Visits.Add(visit);

            foreach(Patient pntCurrent in Patients)
            {
                if(pntCurrent.iID == visit.iPatientId)
                {
                    pntCurrent.AddVisit(visit);
                }
            }
        }

        //adds a new treatment to the database.
        public void addTreatment(Treatment treatment)
        {
            Treatments.Add(treatment);

            foreach (Patient pntCurrent in Patients)
            {
                if (pntCurrent.iID == treatment.iPatientID)
                {
                    pntCurrent.AddTreatment(treatment);
                }
            }
        }
        #endregion
       
        #region XML
        //exports
        public void exportXMLDoctors(List<Doctor> Doc, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
                xmlVar.doctorsListToXml(Doc, fileName);
        }
        public void exportXMLTechs(List<LabTech> tec, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.techsListToXml(tec, fileName);
        }
        public void exportXMLTests(List<LabTest> test, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.testsListToXml(test, fileName);
        }
        public void exportXMLPatients(List<Patient> Pat, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.patientListToXml(Pat, fileName);
        }
        public void exportXMLVisits(List<Visit> Vis, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.visitListToXml(Vis, fileName);
        }
        public void exportXMLTreatments(List<Treatment> Trm, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.treatmentListToXml(Trm, fileName);
        }
        public void exportXMLSecurity(List<Securities> security, String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            xmlVar.SecurityToXml(security, fileName);
        }
        //Imports
        public void importXMLDoctors(String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            Doctors=xmlVar.doctorsFromXML(fileName);
        }
        public void importXMLPatients(String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            Patients=xmlVar.patientsFromXML(fileName);
        }
        public void importXMLVisits(String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            Visits=xmlVar.visitsFromXML(fileName);
        }
        public void importXMLTreatments(String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            Treatments=xmlVar.treatmentsFromXML(fileName);
            
        }
        public void importXMLSecurity(String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            Security = xmlVar.SecurityFromXML(fileName);
        }
        public void importXMLTechs(String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            Labtechs = xmlVar.techsFromXML(fileName);
        }
        public void importXMLTests(String fileName)
        {
            XML_DAL xmlVar = new XML_DAL();
            Labtests = xmlVar.testsFromXML(fileName);
        }
        #endregion

    }
}

