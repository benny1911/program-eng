﻿using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System;
using System.Configuration.Assemblies;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.Mvc;
using System.Net;
using System.ComponentModel;
using System.Collections.Generic;
using BL_Backend;

namespace DAL

{
    public class ExportToPdf
    {

        public static void ExportToPDFDoctors(List<Doctor> doc)
        {
            var doc1 = new Document();
            string path = Directory.GetCurrentDirectory().ToString() + "\\PDF\\";
            PdfWriter.GetInstance(doc1, new FileStream(path + "Doctors.pdf", FileMode.Create));
            doc1.Open();
            doc1.Add(new Paragraph("Doctors:                                                                    Time:" + DateTime.Now));
            doc1.Add(new Paragraph("\n"));
            PdfPTable table = new PdfPTable(6);
            PdfPCell cell = new PdfPCell(new Phrase("Doctors"));
            cell.Colspan = 6;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font times = new Font(bfTimes, 10, Font.ITALIC);
            table.AddCell(new PdfPCell(new Phrase("No.", times)));
            table.AddCell(new PdfPCell(new Phrase("ID", times)));
            table.AddCell(new PdfPCell(new Phrase("First name", times)));
            table.AddCell(new PdfPCell(new Phrase("Last name", times)));
            table.AddCell(new PdfPCell(new Phrase("Salary", times)));
            table.AddCell(new PdfPCell(new Phrase("Gender", times)));

            int c = 1;
            foreach (Doctor d in doc)
            {
                table.AddCell(new PdfPCell(new Phrase("" + c, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sFirst_name, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sLast_Name, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.dSalary, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.cGender, times)));
                c++;
            }
            doc1.Add(table);
            doc1.Close();
        }


        public static void ExportToPDFVisits(List<Visit> currentVisitsList)
        {
           var doc1 = new Document();
            string path = Directory.GetCurrentDirectory().ToString() + "\\PDF\\";
            PdfWriter.GetInstance(doc1, new FileStream(path + "Visits.pdf", FileMode.Create));
            doc1.Open();
            doc1.Add(new Paragraph("Visits:                                                                    Time:" + DateTime.Now));
            doc1.Add(new Paragraph("\n"));
            PdfPTable table = new PdfPTable(6);
            PdfPCell cell = new PdfPCell(new Phrase("Visits"));
            cell.Colspan = 6;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font times = new Font(bfTimes, 10, Font.ITALIC);
            table.AddCell(new PdfPCell(new Phrase("No.", times)));
            table.AddCell(new PdfPCell(new Phrase("ID", times)));
            table.AddCell(new PdfPCell(new Phrase("Patient ID", times)));
            table.AddCell(new PdfPCell(new Phrase("Date of visit", times)));
            table.AddCell(new PdfPCell(new Phrase("Assinged doctor", times)));
            table.AddCell(new PdfPCell(new Phrase("Doctors Notes", times)));
            int c = 1;
            foreach (Visit d in currentVisitsList)
            {
                table.AddCell(new PdfPCell(new Phrase("" + c, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iVisitID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iPatientId, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.dateOfVisit, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.AssignedDoctor, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sDoctorNotes, times)));
                c++;
            }
            doc1.Add(table);
            doc1.Close();
        }

        public static void ExportToPDFTreatments(List<Treatment> currentTreatmentsList)
        {
            var doc1 = new Document();
            string path = Directory.GetCurrentDirectory().ToString() + "\\PDF\\";
            PdfWriter.GetInstance(doc1, new FileStream(path + "Treatments.pdf", FileMode.Create));
            doc1.Open();
            doc1.Add(new Paragraph("Treatments:                                                                    Time:" + DateTime.Now));
            doc1.Add(new Paragraph("\n"));
            PdfPTable table = new PdfPTable(9);
            PdfPCell cell = new PdfPCell(new Phrase("Treatments"));
            cell.Colspan = 9;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);

            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font times = new Font(bfTimes, 10, Font.ITALIC);
            table.AddCell(new PdfPCell(new Phrase("No.", times)));
            table.AddCell(new PdfPCell(new Phrase("ID", times)));
            table.AddCell(new PdfPCell(new Phrase("Visit Id", times)));
            table.AddCell(new PdfPCell(new Phrase("Patients Id", times)));
            table.AddCell(new PdfPCell(new Phrase("Treatments name", times)));
            table.AddCell(new PdfPCell(new Phrase("Date of start", times)));
            table.AddCell(new PdfPCell(new Phrase("Date of finish", times)));
            table.AddCell(new PdfPCell(new Phrase("Created by doctor", times)));
            table.AddCell(new PdfPCell(new Phrase("Prognosis", times)));

            int c = 1;
            foreach (Treatment d in currentTreatmentsList)
            {
                table.AddCell(new PdfPCell(new Phrase("" + c, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iTreatmentID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iVisitID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iPatientID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sTreatmentName, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.DateOfStart, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.DateOfFinish, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.createdByDoctor, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sPrognosis, times)));
                c++;
            }
            doc1.Add(table);
            doc1.Close();
        }


        public static void ExportToPDFLabTech(List<LabTech> currentLabTechList)
        {
            var doc1 = new Document();
            string path = Directory.GetCurrentDirectory().ToString() + "\\PDF\\";
            PdfWriter.GetInstance(doc1, new FileStream(path + "LabTechs.pdf", FileMode.Create));
            doc1.Open();
            doc1.Add(new Paragraph("LabTechs:                                                                    Time:" + DateTime.Now));
            doc1.Add(new Paragraph("\n"));
            PdfPTable table = new PdfPTable(6);
            PdfPCell cell = new PdfPCell(new Phrase("LabTechs"));
            cell.Colspan = 6;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font times = new Font(bfTimes, 10, Font.ITALIC);
            table.AddCell(new PdfPCell(new Phrase("No.", times)));
            table.AddCell(new PdfPCell(new Phrase("ID", times)));
            table.AddCell(new PdfPCell(new Phrase("First name", times)));
            table.AddCell(new PdfPCell(new Phrase("Last name", times)));
            table.AddCell(new PdfPCell(new Phrase("Salary", times)));
            table.AddCell(new PdfPCell(new Phrase("Gender", times)));

            int c = 1;
            foreach (LabTech d in currentLabTechList)
            {
                table.AddCell(new PdfPCell(new Phrase("" + c, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sFirst_name, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sLast_Name, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.dSalary, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.cGender, times)));
                c++;
            }
            doc1.Add(table);
            doc1.Close();
        }

        public static void ExportToPDFLabTest(List<LabTest> currentLabTestList)
        {
            var doc1 = new Document();
            string path = Directory.GetCurrentDirectory().ToString() + "\\PDF\\";
            PdfWriter.GetInstance(doc1, new FileStream(path + "LabTests.pdf", FileMode.Create));
            doc1.Open();
            doc1.Add(new Paragraph("LabTests:                                                                    Time:" + DateTime.Now));
            doc1.Add(new Paragraph("\n"));
            PdfPTable table = new PdfPTable(8);
            PdfPCell cell = new PdfPCell(new Phrase("LabTests"));
            cell.Colspan = 8;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font times = new Font(bfTimes, 10, Font.ITALIC);
            table.AddCell(new PdfPCell(new Phrase("No.", times)));
            table.AddCell(new PdfPCell(new Phrase("ID", times)));
            table.AddCell( new PdfPCell(new Phrase("Patient ID", times)));
            table.AddCell(new PdfPCell(new Phrase("Date of visit", times)));
            table.AddCell(new PdfPCell(new Phrase("Assinged doctor", times)));
            table.AddCell(new PdfPCell(new Phrase("Doctors Notes", times)));
            table.AddCell(new PdfPCell(new Phrase("LabTech", times)));
            table.AddCell(new PdfPCell(new Phrase("Results", times)));

            int c = 1;
            foreach (LabTest d in currentLabTestList)
            {
                table.AddCell(new PdfPCell(new Phrase("" + c, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iLabTestID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iPatientId, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.dateOfLabTest, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.AssignedDoctor, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sDoctorNotes, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iLabTechId, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sResults, times)));
                c++;
            }
            doc1.Add(table);
            doc1.Close();
        }

        public static void ExportToPDFPatients(List<Patient> currentPatientsList)
        {
            var doc1 = new Document();
            string path = Directory.GetCurrentDirectory().ToString() + "\\PDF\\";
            PdfWriter.GetInstance(doc1, new FileStream(path + "Patients.pdf", FileMode.Create));
            doc1.Open();
            doc1.Add(new Paragraph("Patients:                                                                    Time:" + DateTime.Now));
            doc1.Add(new Paragraph("\n"));
            PdfPTable table = new PdfPTable(7);
            PdfPCell cell = new PdfPCell(new Phrase("Patients"));
            cell.Colspan = 7;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font times = new Font(bfTimes, 10, Font.ITALIC);
            table.AddCell(new PdfPCell(new Phrase("No.", times)));
            table.AddCell(new PdfPCell(new Phrase("ID", times)));
            table.AddCell(new PdfPCell(new Phrase("First name", times)));
            table.AddCell(new PdfPCell(new Phrase("Last name", times)));
            table.AddCell(new PdfPCell(new Phrase("Age", times)));
            table.AddCell(new PdfPCell(new Phrase("Gender", times)));
            table.AddCell(new PdfPCell(new Phrase("Main doctor", times)));


            int c = 1;
            foreach (Patient d in currentPatientsList)
            {
                table.AddCell(new PdfPCell(new Phrase("" + c, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sFirst_name, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sLast_Name, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iAge, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.cGender, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.MainDoctor, times)));
                c++;
            }
            doc1.Add(table);
            doc1.Close();
        }

        public static void ExportToPDFall(List<Patient> currentPatientsList, List<LabTest> currentLabTestList,List<Treatment> currentTreatmentsList,List<Visit> currentVisitsList)
        {
            var doc1 = new Document();
            string path = Directory.GetCurrentDirectory().ToString() + "\\PDF\\";
            PdfWriter.GetInstance(doc1, new FileStream(path + "Info.pdf", FileMode.Create));
            doc1.Open();
            doc1.Add(new Paragraph("Patients:                                                                    Time:" + DateTime.Now));
            doc1.Add(new Paragraph("\n"));
            PdfPTable table = new PdfPTable(7);
            PdfPCell cell = new PdfPCell(new Phrase("Patients"));
            cell.Colspan = 7;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font times = new Font(bfTimes, 10, Font.ITALIC);
            table.AddCell(new PdfPCell(new Phrase("No.", times)));
            table.AddCell(new PdfPCell(new Phrase("ID", times)));
            table.AddCell(new PdfPCell(new Phrase("First name", times)));
            table.AddCell(new PdfPCell(new Phrase("Last name", times)));
            table.AddCell(new PdfPCell(new Phrase("Age", times)));
            table.AddCell(new PdfPCell(new Phrase("Gender", times)));
            table.AddCell(new PdfPCell(new Phrase("Main doctor", times)));


            int c = 1;
            foreach (Patient d in currentPatientsList)
            {
                table.AddCell(new PdfPCell(new Phrase("" + c, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sFirst_name, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sLast_Name, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iAge, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.cGender, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.MainDoctor, times)));
                c++;
            }
            doc1.Add(table);
            doc1.Add(new Paragraph("\n"));
            //tests
            table = new PdfPTable(7);
            cell = new PdfPCell(new Phrase("LabTests"));
            cell.Colspan = 8;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);
            table.AddCell(new PdfPCell(new Phrase("No.", times)));
            table.AddCell(new PdfPCell(new Phrase("ID", times)));
            table.AddCell(new PdfPCell(new Phrase("Patient ID", times)));
            table.AddCell(new PdfPCell(new Phrase("Date of visit", times)));
            table.AddCell(new PdfPCell(new Phrase("Assinged doctor", times)));
            table.AddCell(new PdfPCell(new Phrase("Doctors Notes", times)));
            table.AddCell(new PdfPCell(new Phrase("LabTech", times)));
            table.AddCell(new PdfPCell(new Phrase("Results", times)));

            c = 1;
            foreach (LabTest d in currentLabTestList)
            {
                table.AddCell(new PdfPCell(new Phrase("" + c, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iLabTestID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iPatientId, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.dateOfLabTest, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.AssignedDoctor, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sDoctorNotes, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iLabTechId, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sResults, times)));
                c++;
            }
            doc1.Add(table);

            doc1.Add(new Paragraph("\n"));
            table = new PdfPTable(9);
            cell = new PdfPCell(new Phrase("Treatments"));
            cell.Colspan = 9;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);

            bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            times = new Font(bfTimes, 10, Font.ITALIC);
            table.AddCell(new PdfPCell(new Phrase("No.", times)));
            table.AddCell(new PdfPCell(new Phrase("ID", times)));
            table.AddCell(new PdfPCell(new Phrase("Visit Id", times)));
            table.AddCell(new PdfPCell(new Phrase("Patients Id", times)));
            table.AddCell(new PdfPCell(new Phrase("Treatments name", times)));
            table.AddCell(new PdfPCell(new Phrase("Date of start", times)));
            table.AddCell(new PdfPCell(new Phrase("Date of finish", times)));
            table.AddCell(new PdfPCell(new Phrase("Created by doctor", times)));
            table.AddCell(new PdfPCell(new Phrase("Prognosis", times)));

            c = 1;
            foreach (Treatment d in currentTreatmentsList)
            {
                table.AddCell(new PdfPCell(new Phrase("" + c, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iTreatmentID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iVisitID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iPatientID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sTreatmentName, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.DateOfStart, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.DateOfFinish, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.createdByDoctor, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sPrognosis, times)));
                c++;
            }
            doc1.Add(table);
            doc1.Add(new Paragraph("\n"));
            table = new PdfPTable(6);
            cell = new PdfPCell(new Phrase("Visits"));
            cell.Colspan = 6;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);
            bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            times = new Font(bfTimes, 10, Font.ITALIC);
            table.AddCell(new PdfPCell(new Phrase("No.", times)));
            table.AddCell(new PdfPCell(new Phrase("ID", times)));
            table.AddCell(new PdfPCell(new Phrase("Patient ID", times)));
            table.AddCell(new PdfPCell(new Phrase("Date of visit", times)));
            table.AddCell(new PdfPCell(new Phrase("Assinged doctor", times)));
            table.AddCell(new PdfPCell(new Phrase("Doctors Notes", times)));
            c = 1;
            foreach (Visit d in currentVisitsList)
            {
                table.AddCell(new PdfPCell(new Phrase("" + c, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iVisitID, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.iPatientId, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.dateOfVisit, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.AssignedDoctor, times)));
                table.AddCell(new PdfPCell(new Phrase("" + d.sDoctorNotes, times)));
                c++;
            }
            doc1.Add(table);
            doc1.Add(new Paragraph("\n"));
            doc1.Close();
        }
        }
    }

