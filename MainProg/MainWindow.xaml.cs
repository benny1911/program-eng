﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GUI_PL;
using BL;
using BL_Backend;
using DAL;

namespace MainProg
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Visibility = System.Windows.Visibility.Hidden;
            IDAL itsDAL = new SQL_DAL_implementation();
            Doctor_BL itsDoctor_BL = new Doctor_BL(itsDAL);
            LabTech_BL itsLabTech_BL = new LabTech_BL(itsDAL);
            LabTest_BL itsLabTest_BL = new LabTest_BL(itsDAL);
            Patient_BL itsPatient_BL = new Patient_BL(itsDAL);
            Visit_BL itsVisit_BL = new Visit_BL(itsDAL);
            Treatment_BL itsTreatment_BL = new Treatment_BL(itsDAL);
            Securities_BL itsSecurity = new Securities_BL(itsDAL);
            Queries itsQueries = new Queries(itsDoctor_BL, itsPatient_BL, itsVisit_BL, itsTreatment_BL, itsLabTest_BL, itsDAL);
            try
            {
                GUI_PL.MainWindow main = new GUI_PL.MainWindow(itsDAL, itsDoctor_BL, itsPatient_BL, itsVisit_BL, itsTreatment_BL, itsLabTech_BL, itsLabTest_BL, itsSecurity, itsQueries);
               main.ShowDialog();
            }
            catch { }
            
            this.Close();

        }
    }
}
