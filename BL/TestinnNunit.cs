﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;
using DAL;
using BL_Backend;
using NUnit.Framework;
namespace BL_Tests
{
   
    [TestFixture]
    class TestVisit
    {

        [Test]
        public void RemoveVisitFromData_Test()
        {
            IDAL itsDAL = new TempDAL();
            Patient_BL patient1 = new Patient_BL(itsDAL);
            Visit_BL visit1 = new Visit_BL(itsDAL);
            Visit_BL visit2=new Visit_BL(itsDAL);
            List<Visit_BL> expected = new List<Visit_BL>();
            expected.Add(visit1);
            expected.Add(visit2);
            expected.Remove(visit1);
            Assert.AreEqual(itsDAL.Visits,expected);
        }

        [Test]
        public void RemovePatientFromData_Test()
        {
            IDAL itsDAL = new TempDAL();
            Patient_BL patient1 = new Patient_BL(itsDAL);
            Patient_BL patient2 = new Patient_BL(itsDAL);
            List<Patient_BL> expected = new List<Patient_BL>();
            expected.Add(patient1);
            expected.Add(patient2);
            expected.Remove(patient2);
            Assert.AreEqual(itsDAL.Patients,expected);
     
        }
        [Test]
        public void RemoveTreatmentFromData_Test()
        {
            IDAL itsDAL = new TempDAL();
            Treatment_BL Treatment1 = new Treatment_BL(itsDAL);
            Treatment_BL Treatment2 = new Treatment_BL(itsDAL);
            List<Treatment_BL> expected = new List<Treatment_BL>();
            expected.Add(Treatment1);
            expected.Add(Treatment2);
            expected.Remove(Treatment1);
            Assert.AreEqual(itsDAL.Treatments,expected);
        }
        [Test]
        public void RemoveDoctorFromData_Test()
        {
            IDAL itsDAL = new TempDAL();
            Doctor_BL doctor1 = new Doctor_BL(itsDAL);
            Doctor_BL doctor2 = new Doctor_BL(itsDAL);
            List<Doctor_BL> expected = new List<Doctor_BL>();
            expected.Add(doctor1);
            expected.Add(doctor2);
            expected.Remove(doctor1);
            Assert.AreEqual(itsDAL.Doctors,expected);
        }
        [Test]
        public void DoesPatientExist_Test()
        {
            IDAL itsDAL = new TempDAL();
            Patient_BL patient1 = new Patient_BL(itsDAL);
            Assert.Contains(patient1,itsDAL.Patients);
        }
        [Test]
        public void ChangeDoctor_Test()
        {
            Doctor doctor = new Doctor(987654331, "ronit", "geva", 2000, 'F');
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', 987654321);
            patient1.MainDoctor=doctor.iID;
            List<Doctor> expected=new List<Doctor>();
            expected.Add(doctor);
            Assert.AreEqual(expected, patient1.MainDoctor);
        }
        [Test]
        public void RemoveVisit_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F');
            Visit visit = new Visit(12, new DateTime(2010, 12, 1), doctor1, 204369763, "sick", null);
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', 987654321);
            Visit visit2 = new Visit(13, new DateTime(2010, 12, 1), doctor1, 204369763, " very sick", null);
            patient1.AddVisit(visit);
            patient1.AddVisit(visit2);
            patient1.RemoveVisit(visit2);
            List<Visit> expected = new List<Visit>();
            expected.Add(visit);
            expected.Add(visit2);
            expected.Remove(visit2);
            Assert.AreEqual(patient1.lstVisits, expected);
        }
        [Test]
        public void removePrecription_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F');
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', 111111111);
            Visit visit = new Visit(12, new DateTime(2010, 12, 1), doctor1, 204369763, "sick", null);
            Treatment treatment1 = new Treatment(111, new DateTime(2010, 12, 1), new DateTime(2010, 12, 15), doctor1, "not looking too shabby", null, "Test", patient1.iID);
            treatment1.addPrescriptions("antibaotica");
            treatment1.addPrescriptions("norofen");
            treatment1.removePrescriptions("antibaotica");
            List<String> expected = new List<String>();
            expected.Add(treatment1.sPrescriptions[0]);
            expected.Add(treatment1.sPrescriptions[1]);
            expected.Remove(treatment1.sPrescriptions[1]);
            Assert.AreEqual(expected, patient1.lstTreatments);

        }

    }
}


