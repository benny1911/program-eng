﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public class Treatment_BL 
    {
        IDAL itsDAL;

        public Treatment_BL(IDAL itsDAL)
        {
            this.itsDAL = itsDAL;
        }

        //public List<Treatment> getItsDal()
        //{
        //    return itsDAL.Treatments;
        //}
   

        //returns the treatment that matches the id the method got. if no such treatment exists, returns null
        public Treatment getTreatmentByID(int iTreatmentID)
        {
            //foreach (Treatment treatmentToCheck in )
            //{
            //    if (iTreatmentID == treatmentToCheck.iTreatmentID) return treatmentToCheck;
            //}

            //throw new Exception("Treatment was not found in the Database.");//if the treatment doesn't exist in the DB
            return itsDAL.getTreatmentById(iTreatmentID);
        }

        //the function delete the treatment's occurrences in the Program
        public void RemovetreatmentFromData(Treatment t)
        {
            itsDAL.removeTreatment(t.iTreatmentID);
            ////checks if the treatment is on the treatment list:
            //if (getTreatmentByID(t.iTreatmentID) != null)
            //{
            //    itsDAL.removeTreatment(t.iTreatmentID);

            //}
            //else  
            //    throw new Exception("the treatments doesn't exist!");
        }

        //returns the treatment that matches the name the method got. if no such treatment exists, returns null
        public Treatment getTreatmentByName(string treatmentName)
        {
            List<Treatment> treatments = itsDAL.allTreatments();
            foreach (Treatment treatmentToCheck in treatments)
            {
                if (treatmentName.Equals(treatmentToCheck.sTreatmentName)) return treatmentToCheck;
            }
            return null;
        }

        //creates a treatment by asking the user for all the relevant data
        //throws excepetion for wrong IDs and date
        public Treatment createTreatment(Doctor_BL aDoctor_BL)
        {
            bool keepRunning=true;
            int treatmentId = Treatment.iGlobalTreatmentId;
            Treatment.iGlobalTreatmentId += 1;
            bool bIsFound = false;
            string nPatientID = "";

            // Asking for user to enter the correct Patient ID
            while (!bIsFound)
            {
                Console.WriteLine("Enter Patient ID: ");
                nPatientID = Console.ReadLine();

                // Checking if the patient exists
                foreach (Patient pntCurrent in itsDAL.allPatients())
                {
                    if (pntCurrent.iID == Convert.ToInt32(nPatientID))
                    {
                        bIsFound = true;
                    }
                }
            }

            Console.WriteLine("Enter Treatment name: ");
            string treatmentName=Console.ReadLine();
            Console.WriteLine(" Enter Date of Start (<month>/<day>/<year> <hour>:<minutes>");
            DateTime dateOfStart = DateTime.Parse(Console.ReadLine(),
                         System.Globalization.CultureInfo.InvariantCulture);
            Console.WriteLine("Enter Date of Finish (<month>/<day>/<year> <hour>:<minutes>");
            keepRunning = true;
            DateTime dateOfFinish = DateTime.Now;
            while (keepRunning)
            {
                dateOfFinish = DateTime.Parse(Console.ReadLine(),
                             System.Globalization.CultureInfo.InvariantCulture);
                if (dateOfFinish < dateOfStart) Console.WriteLine("Invalid Date");
                else keepRunning = false;
            }
            keepRunning=true;
            Doctor createdByDoctor = null;
            while (keepRunning)
            {
                Console.WriteLine("Enter the Doctor's ID: ");
                int doctorId = int.Parse(Console.ReadLine());
                createdByDoctor = aDoctor_BL.getDoctorByID(doctorId);
                if (createdByDoctor == null)
                    Console.WriteLine("Wrong Id. Please try again");
                else
                    keepRunning = false;
            }
            keepRunning = true;
            Console.WriteLine("Enter Prognosis: ");
            string prognosis = Console.ReadLine();
            int prescriptionCounter=0;
            string nextPrescription;
            List<string> prescriptions=null;
            while (keepRunning)
            {
                Console.WriteLine("Enter Prescription number " + prescriptionCounter + " or 0 to stop:");
                nextPrescription = Console.ReadLine();
                if (nextPrescription == "0")
                    keepRunning = false;
                else
                {
                    prescriptions.Add(nextPrescription);
                    prescriptionCounter++;
                }
            }
            Treatment newTreatment = new Treatment(treatmentId, dateOfStart, dateOfFinish, createdByDoctor, prognosis, prescriptions, treatmentName, Convert.ToInt32(nPatientID), 0);
            itsDAL.addTreatment(newTreatment);
            return newTreatment;
        }
        //exports list of treatments to xml
        //public void ExportXML(List<Treatment> trmToExport, string fileName)
        //{
        //    itsDAL.exportXMLTreatments(trmToExport, fileName);
        //}


        public void ExporttoExcel_Treatments(List<Treatment> currentTreatmentsList)
        {
            itsDAL.ExporttoExcel_Treatments(currentTreatmentsList);
        }
    }
}
