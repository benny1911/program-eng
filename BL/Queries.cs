﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;


namespace BL
{
    public class Queries
    {
        // the Queries worked on a 4 lists of types:
        public List<Doctor> Doctors { get; set; }
        public List<Patient> Patients { get; set; }
        public List<Visit> Visits { get; set; }
        public List<Treatment> Treatments { get; set; }
        public List<LabTest> labTests { get; set; }
        // the Queries need the functions in that classes: (need to "know" them )
        private Doctor_BL itsDoctor_BL;
        private Patient_BL itsPatient_BL;
        private Visit_BL itsVisit_BL;
        private Treatment_BL itsTreatment_BL;
        private LabTest_BL itsLabTest_BL;
        private IDAL itsDal;

        // empty constructor for the Quries menu in CLI_PL.CLI_PL
        public Queries(Doctor_BL aDoctor_BL, Patient_BL aPatient_BL, Visit_BL aVisit_BL, Treatment_BL aTreatment_BL, LabTest_BL aLabTest_BL, IDAL aDal)
        {
            itsDal = aDal;
            Doctors = itsDal.allDoctors();
            Patients = itsDal.allPatients();
            Visits = itsDal.allVisits();
            Treatments = itsDal.allTreatments();
            itsDoctor_BL = aDoctor_BL;
            itsPatient_BL = aPatient_BL;
            itsVisit_BL = aVisit_BL;
            itsTreatment_BL = aTreatment_BL;
            itsLabTest_BL = aLabTest_BL;

        }

        #region old ver
        //    //gets a patient's id and returns the list of the treatments made  (not sorted):
        //    public List<Treatment> TreatmentsMadeByPatient(int id)
        //    {
        //        //itsPatient_BL = new Patient_BL();
        //        List<Treatment> lstNew = new List<Treatment>();
        //        //toPatient is the patient with the given id
        //        Patient toPatient = itsPatient_BL.getPatientByID(id);
        //        if(toPatient!=null)
        //                return itsPatient_BL.getTreatmentsByList(Treatments, toPatient.getTreatments()); // could be null
        //        else // the patients isn't registered the medical center 
        //                Console.WriteLine("the patients isn't registered in the medical center, try again! ");

        //        // defult return statment 
        //        return lstNew;
        //    }



        //    public List<Visit> VisitsMadeByPatient(int id)
        //    {
        //        List<Visit> lstNew=new List<Visit>();
        //        /* gets a patient's id and returns the list of the visits he made (not sorted)
        //        check if the id is valid (made of chars only)
        //        and if the id belongs to a patient in the medical center */
        //        if (id >= 100000000 && id <= 999999999)
        //        {
        //         Patient toPatient = itsPatient_BL.getPatientByID(id);
        //         if (toPatient != null)
        //             return itsPatient_BL.getVisitssByList(Visits, toPatient.getVisits()); 
        //         else // the patients isn't registered the medical center
        //                // @TODO - is exception needed ? 
        //                Console.WriteLine("the patients isn't registered the medical center, try again! ");
        //        }
        //        else  // the id isn't valid
        //            Console.WriteLine("the Id isn't valid. try again! ");
        //        // defult return statment 
        //        return lstNew;
        //    }

        //    public List<Visit> SortByVisitsId(List<Visit> visits, int startId, int endId)
        //    /* the function gets a list of all the visits made , visitId smaller and higher and 
        //        returns a list of the visits in the given range  */
        //    {
        //        List<Visit> SortByvisitId = new List<Visit>();
        //        foreach (Visit visit in visits)
        //        {
        //            if ((visit.iVisitID >= startId) && (visit.iVisitID <= endId))
        //                SortByvisitId.Add(visit);
        //        }
        //        return SortByvisitId;
        //    }

        //    public List<Visit> SortByVisitsDay(List<Visit> visits, DateTime date)
        //    /* the function gets a list of all the visits made and a date,
        //        returns a list of the visits happened at the exact quested date  */
        //    {
        //        List<Visit> SortByvisitDay = new List<Visit>();
        //        foreach (Visit visit in visits)
        //        {
        //            if (visit.dateOfVisit >= date)
        //            {
        //                SortByvisitDay.Add(visit);
        //            }
        //        }
        //        return SortByvisitDay;
        //    }


        //    // the function returns a list of the male/ female doctors , as reqested
        //    public List<Doctor> SortByGenderDoc(List<Doctor> doctorlist, char gender)
        //    {
        //        // create new list to return
        //        List<Doctor> sortedByGenderl = new List<Doctor>();

        //        foreach (Doctor doctor in doctorlist)
        //        {
        //            //checks if the doctor is a male
        //            if ((gender == 'M' || gender == 'm')&&(doctor.cGender=='M' || doctor.cGender=='m'))
        //            {
        //                sortedByGenderl.Add(doctor);
        //            }
        //            //checks if the doctor is a female
        //            if ((gender == 'F' || gender == 'f')&&(doctor.cGender=='F' || doctor.cGender=='f'))
        //            {
        //                sortedByGenderl.Add(doctor);
        //            }
        //        }
        //        return sortedByGenderl;
        //    }

        //    // the function returns a list of the male/ female patients , as reqested
        //    public List<Patient> SortByGenderP(List<Patient> patientlist, char gender)
        //    {
        //        List<Patient> sortedByGenderl = new List<Patient>();

        //        foreach (Patient patient in patientlist)
        //        {
        //            if ((gender == 'M' || gender == 'm') && (patient.cGender == 'M' || patient.cGender == 'm'))
        //            {
        //                sortedByGenderl.Add(patient);
        //            }
        //            //checks if the doctor is a female
        //            if ((gender == 'F' || gender == 'f') && (patient.cGender == 'F' || patient.cGender == 'f'))
        //            {
        //                sortedByGenderl.Add(patient);
        //            }
        //        }
        //        return sortedByGenderl;
        //    }

        //    // returns all the doctors with the requested range of salary
        //    public List<Doctor> SortBySalary(List<Doctor> doctors, int lowRange, int HighRange)
        //    {
        //        List<Doctor> SortedBySalary = new List<Doctor>();
        //        foreach (Doctor doctor in doctors)
        //        {
        //            if ((doctor.dSalary >= lowRange) && (doctor.dSalary <= HighRange))
        //                SortedBySalary.Add(doctor);
        //        }
        //        return SortedBySalary;


        //    }

        //    // returns all the doctors with the same requested first name
        //    public List<Doctor> sortByFirstNameDoc(List<Doctor> doctorlist, string FirstName)
        //    {
        //        List<Doctor> SortedByFirstNamel = new List<Doctor>();

        //        foreach (Doctor doctor in doctorlist)
        //        {
        //            if (FirstName == doctor.sFirst_name)
        //                SortedByFirstNamel.Add(doctor);
        //        }
        //        return SortedByFirstNamel;
        //    }
        //    // returns all the patients with the same requested first name
        //    public List<Patient> sortByFirstNameP(List<Patient> patientlist, string FirstName)
        //    {
        //        List<Patient> SortedByFirstNamel = new List<Patient>();

        //        foreach (Patient patient in patientlist)
        //        {
        //            if (FirstName.Equals(patient.sFirst_name))
        //                SortedByFirstNamel.Add(patient);
        //        }
        //        return SortedByFirstNamel;
        //    }

        //    // returns all the doctors with the same requested last name
        //    public List<Doctor> sortByLastNameDoc(List<Doctor> doctorlist, string LastName)
        //    {
        //        List<Doctor> sortByLastNamel = new List<Doctor>();

        //        foreach (Doctor doctor in doctorlist)
        //        {
        //            if (LastName.Equals(doctor.sLast_Name))
        //                sortByLastNamel.Add(doctor);
        //        }
        //        return sortByLastNamel;
        //    }
        //    // returns all the patients with the same requested last name
        //    public List<Patient> sortByLastNameP(List<Patient> patientlist, string LastName)
        //    {
        //        List<Patient> sortByLastNamel = new List<Patient>();

        //        foreach (Patient patient in patientlist)
        //        {
        //            if (LastName.Equals(patient.sLast_Name))
        //                sortByLastNamel.Add(patient);
        //        }
        //        return sortByLastNamel;
        //    }

        //    // returns all the doctors with the same requested id's
        //    public List<Doctor> sortByIDDoc(List<Doctor> doctorlist, int ID)
        //    {
        //        List<Doctor> sortByIDl = new List<Doctor>();
        //        foreach (Doctor doctor in doctorlist) 
        //        {
        //            if (doctor.iID >= ID)
        //            {
        //                sortByIDl.Add(doctor);
        //            }
        //        }
        //        return sortByIDl;
        //    }

        //    // returns all the patients with the same requested id's
        //    public List<Patient> sortByIDP(List<Patient> patientlist, int ID)
        //    {
        //        List<Patient> sortByIDl = new List<Patient>();
        //        foreach (Patient patient in patientlist)
        //        {
        //            if (patient.iID >= ID)
        //                sortByIDl.Add(patient);
        //        }
        //        return sortByIDl;
        //    }

        //    // info about visits: 

        //    // a function to get all the visits that their id is bigger then the given one 
        //    public List<Visit> SortVisitById(List<Visit> visitlist, int StartID)
        //    {
        //        List<Visit> SortVisitById = new List<Visit>();
        //        foreach (Visit visit in visitlist)
        //        {
        //            if (StartID <= visit.iVisitID)
        //            {
        //                SortVisitById.Add(visit);
        //            }
        //        }
        //        return SortVisitById;
        //    }

        //    // return all the visits that started after the given date
        //    public List<Visit> SortVisitByDate(List<Visit> visitlist, DateTime date)
        //    {
        //        List<Visit> SortVisitByDate = new List<Visit>();
        //        foreach (Visit visit in visitlist)
        //        {
        //            if (date <= visit.dateOfVisit)
        //            // @TODO :is it legal to compare dates this way?? 
        //            {
        //                SortVisitByDate.Add(visit);
        //            }
        //        }
        //        return SortVisitByDate;
        //    }

        //    // info about treatments

        //    // return all the treatments that started after the given date
        //    public List<Treatment> SortTreatmentsByDate(List<Treatment> treatmentlist, DateTime date)
        //    {
        //        List<Treatment> SortTreatmentsByDatel = new List<Treatment>();
        //        foreach (Treatment treatment in treatmentlist)
        //        {
        //            if (date <= treatment.DateOfStart)
        //            // @TODO :is it legal to compare dates this way?? 
        //            {
        //                SortTreatmentsByDatel.Add(treatment);
        //            }
        //        }
        //        return SortTreatmentsByDatel;
        //    }
        //    // return all the treatments that ended before the given date
        //    public List<Treatment> SortTreatmentsByDateEnd(List<Treatment> treatmentlist, DateTime date)
        //    {
        //        List<Treatment> SortTreatmentsByDateEndl = new List<Treatment>();
        //        foreach (Treatment treatment in treatmentlist)
        //        {
        //            if (treatment.DateOfFinish <= date)
        //            // @TODO :is it legal to compare dates this way?? 
        //            {
        //                SortTreatmentsByDateEndl.Add(treatment);
        //            }
        //        }
        //        return SortTreatmentsByDateEndl;
        //    }

        //    // return all the treatments with the same doctor in charge
        //    public List<Treatment> SortTreatmentsByDoctors(List<Treatment> treatmentlist, int doctorinchargeId)
        //    {
        //        List<Treatment> SortTreatmentsByDoctorsl = new List<Treatment>();
        //        foreach (Treatment treatment in treatmentlist)
        //        {
        //            if (doctorinchargeId == treatment.createdByDoctor)
        //            {
        //                SortTreatmentsByDoctorsl.Add(treatment);
        //            }
        //        }
        //        return SortTreatmentsByDoctorsl;
        //    }

        //    // return all the treatments with the same doctor in charge
        //    public List<Treatment> SortTreatmentsById(List<Treatment> treatmentlist, int startid)
        //    {
        //        List<Treatment> SortTreatmentsByIdl = new List<Treatment>();
        //        foreach (Treatment treatment in treatmentlist)
        //        {
        //            if (treatment.iTreatmentID >= startid)
        //            {
        //                SortTreatmentsByIdl.Add(treatment);
        //            }
        //        }
        //        return SortTreatmentsByIdl;
        //    }



        //    public List<LabTest> SortLabTestsByDate(List<LabTest> currentLabTestList, DateTime dateOfStart)
        //    {
        //        List<LabTest> SortTestsByDatel = new List<LabTest>();
        //        foreach (LabTest test in currentLabTestList)
        //        {
        //            if (dateOfStart <= test.dateOfLabTest)
        //            // @TODO :is it legal to compare dates this way?? 
        //            {
        //                SortTestsByDatel.Add(test);
        //            }
        //        }
        //        return SortTestsByDatel;
        //    }

        //    public List<LabTest> SortLabTestById(List<LabTest> currentLabTestList, int Id)
        //    {
        //        List<LabTest> SortTestsByID = new List<LabTest>();
        //        foreach (LabTest test in currentLabTestList)
        //        {
        //            if (test.iLabTestID >= Id)
        //            {
        //                SortTestsByID.Add(test);
        //            }
        //        }
        //        return SortTestsByID;
        //    }

        //    public List<LabTech> sortByFirstNameTech(List<LabTech> currentLabTechList, string FirstName)
        //    {
        //        List<LabTech> SortedByFirstNamel = new List<LabTech>();

        //        foreach (LabTech tech in currentLabTechList)
        //        {
        //            if (FirstName == tech.sFirst_name)
        //                SortedByFirstNamel.Add(tech);
        //        }
        //        return SortedByFirstNamel;
        //    }

        //    public List<LabTech> sortByLastNameTech(List<LabTech> currentLabTechList, string FLastName)
        //    {
        //        List<LabTech> SortedByLastNamel = new List<LabTech>();

        //        foreach (LabTech tech in currentLabTechList)
        //        {
        //            if (FLastName == tech.sLast_Name)
        //                SortedByLastNamel.Add(tech);
        //        }
        //        return SortedByLastNamel;
        //    }

        //    public List<LabTech> sortByIDTech(List<LabTech> currentLabTechList, int Id)
        //    {
        //        List<LabTech> sortByIDl = new List<LabTech>();
        //        foreach (LabTech tech in currentLabTechList)
        //        {
        //            if (tech.iID >= Id)
        //            {
        //                sortByIDl.Add(tech);
        //            }
        //        }
        //        return sortByIDl;
        //    }

        //    public List<LabTech> SortBySalaryTech(List<LabTech> currentLabTechList, int lowRange, int HighRange)
        //    {
        //        List<LabTech> SortedBySalary = new List<LabTech>();
        //        foreach (LabTech tech in currentLabTechList)
        //        {
        //            if ((tech.dSalary >= lowRange) && (tech.dSalary <= HighRange))
        //                SortedBySalary.Add(tech);
        //        }
        //        return SortedBySalary;
        //    }

        //    public List<LabTech> sortByGenderTech(List<LabTech> currentLabTechList, char gender)
        //    {
        //        // create new list to return
        //        List<LabTech> sortedByGenderl = new List<LabTech>();

        //        foreach (LabTech labTech in currentLabTechList)
        //        {
        //            //checks if the doctor is a male
        //            if ((gender == 'M' || gender == 'm') && (labTech.cGender == 'M' || labTech.cGender == 'm'))
        //            {
        //                sortedByGenderl.Add(labTech);
        //            }
        //            //checks if the doctor is a female
        //            if ((gender == 'F' || gender == 'f') && (labTech.cGender == 'F' || labTech.cGender == 'f'))
        //            {
        //                sortedByGenderl.Add(labTech);
        //            }
        //        }
        //        return sortedByGenderl;
        //    }
        //}
        #endregion

        public List<Doctor> sortByFirstNameDoc(List<Doctor> currentDoctorsList, string p)
        {
            IQueryable<doctor> d=itsDal.sortByFirstNameDoc(currentDoctorsList, p);
            return itsDal.toDoctors(d);
        }
        public List<Doctor> sortByLastNameDoc(List<Doctor> currentDoctorsList, string p)
        {
            IQueryable<doctor> d = itsDal.sortByLastNameDoc(currentDoctorsList, p);
            return itsDal.toDoctors(d);
        }
        public List<Doctor> sortByIDDoc(List<Doctor> currentDoctorsList, int Id)
        {
            IQueryable<doctor> d = itsDal.sortByIDDoc(currentDoctorsList, Id);
            return itsDal.toDoctors(d);
        }
        public List<Doctor> SortByGenderDoc(List<Doctor> currentDoctorsList, char StringConvertedToChar)
        {
            IQueryable<doctor> d = itsDal.SortByGenderDoc(currentDoctorsList, StringConvertedToChar);
            return itsDal.toDoctors(d);
        }
        public List<Doctor> SortBySalary(List<Doctor> currentDoctorsList, int p1, int p2)
        {
            IQueryable<doctor> d = itsDal.SortBySalary(currentDoctorsList,p1, p2);
            return itsDal.toDoctors(d);
        }
        public List<Patient> sortByFirstNameP(List<Patient> currentPatientsList, string p)
        {
            IQueryable<patient> pat = itsDal.sortByFirstNameP(currentPatientsList, p);
            return itsDal.toPatients(pat);
        }
        public List<Patient> sortByLastNameP(List<Patient> currentPatientsList, string p)
        {
            IQueryable<patient> pat = itsDal.sortByLastNameP(currentPatientsList, p);
            return itsDal.toPatients(pat);
        }
        public List<Patient> sortByIDP(List<Patient> currentPatientsList, int Id)
        {
            IQueryable<patient> pat = itsDal.sortByIDP(currentPatientsList, Id);
            return itsDal.toPatients(pat);
        }
        public List<Patient> SortByGenderP(List<Patient> currentPatientsList, char StringConvertedToChar)
        {
            IQueryable<patient> pat = itsDal.SortByGenderP(currentPatientsList, StringConvertedToChar);
            return itsDal.toPatients(pat);
        }
        public List<Visit> SortVisitByDate(List<Visit> currentVisitsList, DateTime dateToCheck)
        {
            IQueryable<visit> vis = itsDal.SortVisitByDate(currentVisitsList, dateToCheck);
            return itsDal.toVisits(vis);
        }
        public List<Visit> SortVisitById(List<Visit> currentVisitsList, int Id)
        {
            IQueryable<visit> vis = itsDal.SortVisitById(currentVisitsList, Id);
            return itsDal.toVisits(vis);
        }
        public List<Treatment> SortTreatmentsByDate(List<Treatment> currentTreatmentsList, DateTime dateOfStart)
        {
            IQueryable<treatment> trm = itsDal.SortTreatmentsByDate(currentTreatmentsList, dateOfStart);
            return itsDal.toTreatments(trm);
        }
        public List<Treatment> SortTreatmentsById(List<Treatment> currentTreatmentsList, int Id)
        {
            IQueryable<treatment> trm = itsDal.SortTreatmentsById(currentTreatmentsList, Id);
            return itsDal.toTreatments(trm);
        }
        public List<LabTest> SortLabTestsByDate(List<LabTest> currentLabTestList, DateTime dateOfStart)
        {
            IQueryable<labTest> trm = itsDal.SortLabTestsByDate(currentLabTestList, dateOfStart);
            return itsDal.toLabTests(trm);
        }
        public List<LabTest> SortLabTestById(List<LabTest> currentLabTestList, int Id)
        {
            IQueryable<labTest> trm = itsDal.SortLabTestById(currentLabTestList, Id);
            return itsDal.toLabTests(trm);
        }
        public List<LabTech> sortByFirstNameTech(List<LabTech> currentLabTechList, string p)
        {
            IQueryable<labTech> trm = itsDal.sortByFirstNameTech(currentLabTechList, p);
            return itsDal.toLabTechs(trm);
        }
        public List<LabTech> sortByLastNameTech(List<LabTech> currentLabTechList, string p)
        {
            IQueryable<labTech> trm = itsDal.sortByLastNameTech(currentLabTechList, p);
            return itsDal.toLabTechs(trm);
        }
        public List<LabTech> sortByIDTech(List<LabTech> currentLabTechList, int Id)
        {
            IQueryable<labTech> trm = itsDal.sortByIDTech(currentLabTechList, Id);
            return itsDal.toLabTechs(trm);
        }
        public List<LabTech> sortByGenderTech(List<LabTech> currentLabTechList, char StringConvertedToChar)
        {
            IQueryable<labTech> trm = itsDal.sortByGenderTech(currentLabTechList, StringConvertedToChar);
            return itsDal.toLabTechs(trm);
        }
        public List<LabTech> SortBySalaryTech(List<LabTech> currentLabTechList, int p1, int p2)
        {
            IQueryable<labTech> trm = itsDal.SortBySalaryTech(currentLabTechList, p1,p2);
            return itsDal.toLabTechs(trm);
        }


    }
}
