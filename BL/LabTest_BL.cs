﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public class LabTest_BL
    {
        IDAL itsDAL;

        public LabTest_BL(IDAL itsDAL)
        {
            this.itsDAL = itsDAL;
        }

        //public List<LabTest> getItsDal()
        //{
        //    return itsDAL.Labtests;
        //}

        //the function delete the LabTest's occurrences in the Program
        public void RemoveLabTestFromData(LabTest d)
        {
            itsDAL.removeLabTest(d.iLabTestID);

        }

        //returns the doctor that matches the id the method got. if no such doctor exists, returns null
        public LabTest getLabTestByID(int iLabTestID)
        {

            return itsDAL.getLabTestById(iLabTestID);
        }

        public List<LabTest> getLabTestByPatID(List<LabTest> l,int ipatID)
        {

            return itsDAL.toLabTests(itsDAL.SortLabTestByPatId(l,ipatID));
            
        }

        public void ExporttoExcel_LabTest(List<LabTest> currentLabTestList)
        {
            itsDAL.ExporttoExcel_LabTest(currentLabTestList);
        }
    }
}
