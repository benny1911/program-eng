﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public class Patient_BL
    {
        IDAL itsDAL;

        public Patient_BL(IDAL itsDAL)
        {
            this.itsDAL = itsDAL;
        }
        //public List<Patient> getItsDal()
        //{
        //    return itsDAL.Patients;
        //}

        //returns the patient that matches the id the method got. if no such patient exists, returns null
        public Patient getPatientByID(int iPatientID)
        {

            return itsDAL.getPatientById(iPatientID);
        }
        //delete the patient from the db
        public void RemovePatientFromData(Patient p, Visit_BL itsVisit_BL)
        {
            itsDAL.removePatient(p.iID);
        }
        //checks if a patient with the given if exists in the database.
        public bool doesPatientExist(int ID)
        {
            return itsDAL.getPatientById(ID) != null;
        }
        //creates a treatment by asking the user for all the relevant data
        //throws exceptions for wrong ID, salary, or gender.
        public Patient createPatient(Doctor_BL aDoctor_BL)
        {
            bool keepRunning = true;
            Console.WriteLine("Enter ID: ");
            int ID = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter first name: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter last name: ");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter age: ");
            int age=int.Parse(Console.ReadLine());
            Console.WriteLine("Enter gender: ");
            char gender = char.Parse(Console.ReadLine());
            Doctor mainDoctor = null;
            while (keepRunning)
            {
                Console.WriteLine("Enter the main Doctor's ID: ");
                int doctorId = int.Parse(Console.ReadLine());
                mainDoctor = aDoctor_BL.getDoctorByID(doctorId);
                if (mainDoctor == null)
                    Console.WriteLine("Wrong Id. Please try again");
                else
                    keepRunning = false;
            }
            Patient newPatient = new Patient(ID, firstName, lastName, age, gender,mainDoctor.iID);
            itsDAL.addPatient(newPatient);
            return newPatient;

        }

        /// <summary>
        /// creates a treatments list out of their ID
        /// </summary>
        /// <param name="trnCur"></param>
        /// <param name="trnID"></param>
        /// <returns></returns>
        public List<Treatment> getTreatmentsByList(List<Treatment> trnCur,List<int> trnID)
        {
            List<Treatment> trnNew = new List<Treatment>();
            foreach(Treatment t in trnCur)
            {
                if (trnID.Contains(t.iTreatmentID)) trnNew.Add(t);
            }
            return trnNew;
        }

        /// <summary>
        /// creates a visits list out of their ID
        /// </summary>
        /// <param name="vstCur"></param>
        /// <param name="vstID"></param>
        /// <returns></returns>
        public List<Visit> getVisitssByList(List<Visit> vstCur, List<int> vstID)
        {
            List<Visit> vstNew = new List<Visit>();
            foreach (Visit v in vstCur)
            {
                if (vstID.Contains(v.iVisitID)) vstNew.Add(v);
            }
            return vstNew;
        }
        ////exports list of patients to xml
        //public void ExportXML(List<Patient> ptnToExport, string fileName)
        //{
        //    itsDAL.exportXMLPatients(ptnToExport, fileName);
        //}

        public IEnumerable<int> getTreatments(int p)
        {
            return itsDAL.toInt(itsDAL.getListOfTreatmentsbyPatientId(p));
        }

        public IEnumerable<int> getVisits(int p)
        {
            return itsDAL.toInt(itsDAL.getListOfVisitssbyPatientId(p));
        }

        public IEnumerable<LabTest> getLabTest(int p)
        {
            return itsDAL.toLabTests(itsDAL.getListOflabTestsbyPatId(p));
        }

        public void ExporttoExcel_Patients(List<Patient> currentPatientsList)
        {
            itsDAL.ExporttoExcel_Patients(currentPatientsList);
        }
    }
}
