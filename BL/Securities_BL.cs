﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public class Securities_BL
    {
        IDAL itsDal;
        public Securities_BL(IDAL newDAL)
        {
            itsDal = newDAL;
        }
        // the function checks if the username and password are correct.
        public bool ValidateDetails(string userID, int Password)
        {
            //all the program is using id as string, i dont want to change it in many placed so i check like this
            try
            {
                //checks if it's an admin
                if (Convert.ToInt32(userID) == Securities.GetAdminID() && Password == Securities.GetAdminPass()) return true;
                //checks if it's doctor or patient
                List<Securities> securities = itsDal.allSecurities();
                foreach (Securities s in securities)
                {
                    if (Convert.ToInt32(userID) == s.getID() && Password == s.getPass()) return true;
                }
            }
            catch
            {
                return false;
            }
            //defualt
            return false;
        }

        public bool changePassword(string userID, int oldPassword, int newPassword)
        //the function change a user's password return "true" if the process succeed and false otherwise
        //asumption - administrator cannot change his own password
        {
            //if the user exists
            if (ValidateDetails(userID, oldPassword))
            {
                //foreach user in the securities
                List<Securities> securities = itsDal.allSecurities();
                foreach (Securities s in securities)
                {
                    //if its the user
                    if (Convert.ToInt32(userID) == s.getID())
                    {
                        //changes the password and returns true, any other case returns false
                        s.setPass(newPassword);
                        itsDal.updateSecurity(s);
                        return true;
                    }

                }
            }
            return false;
        }
        public string theUserType(string userID, int Password)
        {
            if (userID.Equals(Securities.GetAdminID().ToString()) && Password == Securities.GetAdminPass()) return "Admin";
            //if the user exists
            if (ValidateDetails(userID, Password))
            {
                //finds the user
                List<Securities> securities = itsDal.allSecurities();
                foreach (Securities s in securities)
                {
                    if (Convert.ToInt32(userID) == s.getID()) return s.getAutorization();
                }
            }
            return "wrong input";
        }

        public void addUser(int ID, string type)
        {
            //checks if the user already exists
            List<Securities> securities = itsDal.allSecurities();
            foreach (Securities s in securities)
            {
                if (ID == s.getID()) return;
            }
            //adds the user
            Securities sec = new Securities(ID, ID, type);
            itsDal.addSecurity(sec);
        }

        public void removeUser(int ID, string type)
        {
            itsDal.removeSecurity(ID);
        }

        public string userExists(int iID)
        {
            //checks if the user exists and removed it
            List<Securities> securities = itsDal.allSecurities();
            foreach (Securities s in securities)
            {
                if (iID == s.getID()) return s.getAutorization();
            }
            return "wrong input";
        }

        //inv: the user exists

        public void changeAutorization(int iID, string newAuto)
        {
            itsDal.updateSecurity(iID, newAuto);
        }




    }
}