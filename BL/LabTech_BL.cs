﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public class LabTech_BL
    {
        IDAL itsDAL;

        public LabTech_BL(IDAL itsDAL)
        {
            this.itsDAL = itsDAL;
        }

        //public List<LabTech> getItsDal()
        //{
        //    return itsDAL.Labtechs;
        //}
   
        //the function delete the LabTech's occurrences in the Program
        public void RemoveLabTechFromData(LabTech d)
        {
            itsDAL.removeLabTech(d.iID);

        }

        //returns the doctor that matches the id the method got. if no such doctor exists, returns null
        public LabTech getLabTechByID(int iLabTechID)
        {
            return itsDAL.getLabTechById(iLabTechID);
        }



        public void ExporttoExcel_LabTech(List<LabTech> currentLabTechList)
        {
            itsDAL.ExporttoExcel_LabTech(currentLabTechList);
        }
    }
}
