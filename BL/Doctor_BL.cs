﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public class Doctor_BL
    {
        IDAL itsDAL;

        public Doctor_BL(IDAL itsDAL)
        {
            this.itsDAL = itsDAL;
        }

        //public List<Doctor> getItsDal()
        //{
        //    return itsDAL.Doctors;
        //}

        public Doctor toDoctor(doctor tableDoc)
        {
            Doctor newDoc = new Doctor(Convert.ToInt32(tableDoc.iID), tableDoc.sFirst_name, tableDoc.sLast_name,Convert.ToDouble(tableDoc.dSalary), Convert.ToChar(tableDoc.cGender), Convert.ToBoolean(tableDoc.needsToBeNotified));
            return newDoc;
        }

        

        
      
   
        //the function delete the treatment's occurrences in the Program
        public void RemoveDoctorFromData(Doctor d, Visit_BL itsVisitBl)
        {
            itsDAL.removeDoctor(d.iID);
            ////checks if the doctor is on the doctor list:
            //if (getDoctorByID(d.iID)!=null)
            //{  
            //    //delete the doctor who treat the patient
            //    List<Patient> patients= itsDAL.allPatients();
            //    foreach (Patient patient in patients)
            //    {
            //        if (patient.MainDoctor == d.iID)
            //        {
            //            patient.MainDoctor = 100000000;
            //            itsDAL.updatePatient(patient);
            //            break;
            //        }
            //    }
            //    //delete the visits the doctor
            //    List<Visit> visits = itsDAL.allVisits();
            //    foreach (Visit visit in visits)
            //    {
            //        if (visit.AssignedDoctor == d.iID) itsVisitBl.RemoveVisitFromData(visit, new Patient_BL(itsDAL));
            //        break;
            //    }
            //    //delete the treatments the doctor has
            //    List<Treatment> treatments = itsDAL.allTreatments();
            //    foreach (Treatment treatment in treatments)
            //    {
            //        if (treatment.createdByDoctor == d.iID)
            //        {
            //            treatment.createdByDoctor = 100000000;
            //            itsDAL.updateTreatment(treatment);
            //        }
            //        break;
            //    }
            //    ////change the doctor who assign to a visit to null
            //    //foreach (Visit visit in visits)
            //    //{
            //    //    if (visit.AssignedDoctor == d.iID) visit.AssignedDoctor = 100000000;
            //    //    break;
            //    //}
            //    //delete the doctor from the database
            //    List<Doctor> doctors= itsDAL.allDoctors();
            //    foreach (Doctor doctor in doctors)
            //    {
            //        if (doctor == d) itsDAL.removeDoctor(doctor.iID);
            //        break;
            //    }
            //}
            //else throw new Exception("the doctor doesn't exist!");

        }

        //returns the doctor that matches the id the method got. if no such doctor exists, returns null
        public Doctor getDoctorByID(int iDoctorID)
        {

            return itsDAL.getDoctorById(iDoctorID);
        }

        public void updateDoctor(Doctor d)
        {
            itsDAL.updateDoctor(d);
        }



        //creates a doctor by asking the user for all the relevant data
        //throws exceptions for wrong ID, salary, or gender.
        public Doctor createDoctor()
        {
            Console.WriteLine("Enter ID: ");
            int ID = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter first name: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter last name: ");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter salary: ");
            double salary = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter gender: ");
            char gender = char.Parse(Console.ReadLine());
            Doctor newDoctor = new Doctor(ID, firstName, lastName, salary, gender, false);
            itsDAL.addDoctor(newDoctor);
            return newDoctor;

        }

        /// <summary>
        /// get a patients list by a patient id list
        /// </summary>
        /// <param name="pat">lists of patient id and patients</param>
        /// <returns>patient objects</returns>
        public List<Patient> getPatientsByInt(List<Patient> lstPtn,List<int> pat)
        {
            List<Patient> ptnNew = new List<Patient>();
            foreach (Patient p in lstPtn)
            {
                if (pat.Contains(p.iID)) ptnNew.Add(p);
            }

            return ptnNew;
        }

        public List<int> getPatients(int doctorId)
        {
            return itsDAL.toInt(itsDAL.getListOfPatientbyDocId(doctorId));
        }

        public bool hasPatient(int doctorID, int patientID)
        {
            return itsDAL.hasPatient(doctorID, patientID);
        }

        //exports list of doctors to xml





        public void addPatientToDoctor(int doctorsID, int patientsID)
        {
            itsDAL.addPatientToDoctor(doctorsID, patientsID);
        }

        public void removePatientFromDoctor(int doctorsID, int patientsID)
        {
            itsDAL.removePatientFromDoctor(doctorsID, patientsID);
        }

        public List<Visit> getVisits(int p)
        {
            return itsDAL.toVisits(itsDAL.getListOfVisitsbyDocId(p));
        }

        public List<Treatment> getTreatments(int p)
        {
            return itsDAL.toTreatments(itsDAL.getListOfTreatmentsByDocId(p));
        }

        public List<LabTest> getTests(int p)
        {
            return itsDAL.toLabTests(itsDAL.getListOflabTestsbyDocId(p));
        }

        public List<Patient> getPatientsById(int p)
        {
            return itsDAL.toPatients(itsDAL.getListOfPatientbyDocId(p));
        }

        public void ExporttoExcel_Doctors(List<Doctor> lst)
        {
            itsDAL.ExporttoExcel_Doctors(lst);
        }


    }
}
