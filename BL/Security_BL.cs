﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;

namespace BL
{
    public class Security_BL
    {
        //fields: 

        //all of the usernames and passwords lists :
        private List<int> DoctorsUserNames;
        private List<int> PatiensUserNames;
        private List<int> DoctorsPasswords;
        private List<int> PatientsPasswords;

        private Doctor_BL itsDoctor_BL;
        private Patient_BL itsPatient_BL ;

        //constructor:
        public Security_BL(Doctor_BL aDoctor_BL, Patient_BL aPatient_BL)
        {
            this.itsDoctor_BL = aDoctor_BL;
            this.itsPatient_BL = aPatient_BL;
            makeLists();
        }

        private void makeLists()
        //the function creates the usernames and passwords lists
        {
            this.DoctorsUserNames = new List<int>();
            this.PatiensUserNames = new List<int>();
            this.DoctorsPasswords = new List<int>();
            this.PatientsPasswords = new List<int>();
        
            // the lists of the doctors usernames and passwords is initialized with their ID as both username and password.
            foreach (Doctor doctor in itsDoctor_BL.getItsDal())
            {
                // the usernames and password for each object are at the same indexes in both lists:
                this.DoctorsUserNames.Add(doctor.iID);
                this.DoctorsPasswords.Add(doctor.iID);
            }
            // the lists of the patients usernames and passwords is initialized with their ID as both username and password.
            foreach (Patient patient in itsPatient_BL.getItsDal())
            {
                // the usernames and password for each object are at the same indexes in both lists:
                this.PatiensUserNames.Add(patient.iID);
                this.PatientsPasswords.Add(patient.iID);
            }
        }

        public bool ValidateDetails (int userID, int Password)
        // the function checks if the username and password are correct.
        {
            int userIndex = PatiensUserNames.IndexOf(userID);
            if (Password == PatientsPasswords.ElementAt(userIndex))
            {
                return true;
            }
            else return false;
        }


        public void changePassword(int userID, int oldPassword, int newPassword)
        //the function change a user's password 
        {
            try 
            {
            // check if the ID is doctor or patient
            //before calling this function - validing the ID !
                if (itsPatient_BL.getPatientByID(userID) != null)
            {
                // the ID is patient
                //finds the patient index of data in the lists:
                int userIndex = PatiensUserNames.IndexOf(userID);
                // hid old password:
                int itsOldPass = PatientsPasswords.ElementAt(userIndex);
  
                //compare to the old password:
                if (oldPassword == itsOldPass )
                {
                    PatientsPasswords[userIndex]= newPassword;
                }
                else 
                {
                   throw new Exception ("the old password not found, try again");
                }
            }
                if (itsDoctor_BL.getDoctorByID(userID) != null)
            {
                // the ID is Doctor
                //finds the doctor index of data in the lists:
                int userIndex = DoctorsUserNames.IndexOf(userID);
                // hid old password:
                int itsOldPass = DoctorsPasswords.ElementAt(userIndex);
                //compare to the old password:
                if (oldPassword == itsOldPass )
                {
                    PatientsPasswords[userIndex]= newPassword;
                }
                else 
                {
                   throw new Exception ("the old password not found, try again");
                }
            }
             else // the id not doctor nor patient
                 throw new Exception ("the ID not found, try again");

            }//try
        
            catch (Exception)
            {
               Console.WriteLine("error on given data  , Try again");
            }
            
        }

      }
}

