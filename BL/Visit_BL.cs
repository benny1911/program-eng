﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public class Visit_BL
    {
        IDAL itsDAL;

        public Visit_BL(IDAL itsDAL)
        {
            this.itsDAL = itsDAL;
        }
        //public List<Visit> getItsDal()
        //{
        //    return itsDAL.Visits;
        //}


        public void RemoveVisitFromData(Visit v, Patient_BL aPatient_BL)
        {
            itsDAL.removeVisit(v.iVisitID);
            ////checks if the visit is on the visit list:
            //if (getVisitByID(v.iVisitID) != null)
            //{
            //    Patient patient = aPatient_BL.getPatientByID(v.iPatientId);
            //    foreach (int visit in patient.getVisits())
            //    {
            //        if (visit == v.iVisitID)
            //        {
            //            patient.RemoveVisit(v);
            //            break;
            //        }
            //    }


            //    foreach (Visit visit in itsDAL.Visits)
            //    {
            //        if (visit == v)
            //        {
            //            itsDAL.Visits.Remove(v);
            //            break;//once we found the visit there is no ned to continue searching..
            //        }
            //    }

            //}
            //else throw new Exception("the visit isn't exist!");
        }

        //returns the visit that matches the id the method got. if no such visit exists, returns null
        public Visit getVisitByID(int iVisitID)
        {

            return itsDAL.getVisitById(iVisitID);

        }


        //creates a visit by asking the user for all the relevant data
        //throws excepetion for wrong IDs and date
        public Visit createVisit(Doctor_BL aDoctor_BL, Treatment_BL aTreatment_BL)
        {
            bool keepRunning = true;
            int visitId = Visit.iGlobalVisitId;
            Visit.iGlobalVisitId += 1;
            Console.WriteLine("Enter Date of Visit (<month>/<day>/<year> <hour>:<minutes>");
            DateTime dateOfVisit = DateTime.Parse(Console.ReadLine(),
                          System.Globalization.CultureInfo.InvariantCulture);

            Console.WriteLine("Enter patient ID: ");
            int patientId = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Doctor notes: ");
            string doctorNotes = Console.ReadLine();
            Doctor assignedDoctor = null;
            while (keepRunning)
            {
                Console.WriteLine("Enter the Doctor's ID: ");
                int doctorId = int.Parse(Console.ReadLine());
                assignedDoctor = aDoctor_BL.getDoctorByID(doctorId);
                if (assignedDoctor == null)
                    Console.WriteLine("Wrong Id. Please try again");
                else
                    keepRunning = false;
            }
            keepRunning = true;
            int treatmentCounter = 1;
            List<int> treatmentsMade = new List<int>(); ;
            string nextTreatmentName;
            while (keepRunning)
            {
                Console.WriteLine("Enter Treatment ID " + treatmentCounter + " or 0 to stop:");
                nextTreatmentName = Console.ReadLine();
                if (nextTreatmentName == "0")
                    keepRunning = false;
                else
                {

                    treatmentsMade.Add(Convert.ToInt32(nextTreatmentName));
                    treatmentCounter++;
                }

            }

            Visit newVisit = new Visit(visitId, dateOfVisit, assignedDoctor, patientId, doctorNotes, treatmentsMade);
            itsDAL.addVisit(newVisit);
            return newVisit;
        }

        //exports list of visits to xml
        //public void ExportXML(List<Visit> vstToExport, string fileName)
        //{
        //    itsDAL.exportXMLVisits(vstToExport, fileName);
        //}



        public bool patientHasVisit(int patientID, int visitID)
        {
            return itsDAL.patientHasVisit(patientID, visitID);
        }


        public void removeVisitFromPatient(int patientID, int visitID)
        {
            itsDAL.removeVisitFromPatient(patientID, visitID);
        }



        public void addVisitToPatient(int patientID, int visitID)
        {
            itsDAL.addVisitToPatient(patientID, visitID);
        }

        public bool visitHasTreatment(int visitID, int treatmentID)
        {
            return itsDAL.visitHasTreatment(visitID, treatmentID);
        }

        public void addTreatmentToVisit(int visitID, int treatmentID)
        {
            itsDAL.addTreatmentToVisit(visitID, treatmentID);
        }

        public void removeTreatmentFromVisit(int visitID, int treatmentID)
        {
            itsDAL.removeTreatmentFromVisit(visitID, treatmentID);
        }

        public IEnumerable<int> getTreatments(int p)
        {
            return itsDAL.toInt(itsDAL.getTreatmentByVisit(p));
        }

        public void ExporttoExcel_Visits(List<Visit> currentVisitsList)
        {
            itsDAL.ExporttoExcel_Visits(currentVisitsList);
        }
    }
}
