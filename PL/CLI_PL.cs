﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using BL;





namespace PL
{
    public class CLI_PL //: IPL
    {
        ////all of the Business Layer classes that the PL works with
        //private Doctor_BL itsDoctor_BL;
        //private Patient_BL itsPatient_BL;
        //private Visit_BL itsVisit_BL;
        //private Treatment_BL itsTreatment_BL;
        //private Queries itsQueries;
        //private Securities_BL itsSecurity;
        ////public List<Doctor> Doctors { get; set; }
        ////public List<Patient> Patients { get; set; }
        ////public List<Visit> Visits { get; set; }
        ////public List<Treatment> Treatments { get; set; }



        ////constructor      
        //public CLI_PL(Doctor_BL aDoctor_BL, Patient_BL aPatient_BL, Visit_BL aVisit_BL, Treatment_BL aTreatment_BL)
        //{
        //    itsDoctor_BL = aDoctor_BL;
        //    itsPatient_BL = aPatient_BL;
        //    itsVisit_BL = aVisit_BL;
        //    itsTreatment_BL = aTreatment_BL;
        //    //itsQueries = new Queries(aDoctor_BL, aPatient_BL, aVisit_BL, aTreatment_BL);
        //    //itsSecurity = new Securities_BL(aDoctor_BL, aPatient_BL);
        //}

        ////run the PL.
        //public void Run()
        //{
        //    MainMenu();
        //}

        ////the main menu
        //private void MainMenu()
        //{
        //    bool keepRunning = true;
        //    while (keepRunning)
        //    {
        //        Console.WriteLine("What would you like to do?");
        //        Console.WriteLine("4 - access the system ");
        //        Console.WriteLine("1 - Edit existing data");
        //        Console.WriteLine("2 - Add new data");
        //        Console.WriteLine("3 - Make queries about the data");
        //        Console.WriteLine("x - Exit Program");
        //        string choice = Console.ReadLine();
        //        switch (choice)
        //        {
        //            case "1":
        //                EditMenu();
        //                break;
        //            case "2":
        //                AddMenu();
        //                break;
        //            case "3":
        //                QueryMenu();
        //                break;
        //            case "4":
        //                AccessMenu();
        //                break;
        //            case "x"://exit program
        //                keepRunning = false;
        //                break;
        //            default:
        //                Console.WriteLine("Please enter a valid choice");
        //                break;
        //        }
        //    }
        //}

        ////EditMenu- here you can edit and change existing data
        //private void EditMenu()
        //{
        //    Console.Clear();
        //    bool keepRunning = true;
        //    while (keepRunning)
        //    {
        //        Console.WriteLine("What would you like to do?");
        //        Console.WriteLine("1 - Edit Doctor data");
        //        Console.WriteLine("2 - Edit Patient data");
        //        Console.WriteLine("3 - Edit Visit data");
        //        Console.WriteLine("4 - Edit Treatment data");
        //        Console.WriteLine("0 - Previous Menu");
        //        string choice = Console.ReadLine();
        //        try
        //        {
        //            switch (choice)
        //            {
        //                case "1":
        //                    Doctor doctorFound = (Doctor)recieveByID("Doctor");
        //                    if (!(doctorFound is Doctor))
        //                        break;
        //                    else
        //                        EditDoctor(doctorFound);
        //                    break;

        //                case "2":
        //                    Patient patientFound = (Patient)recieveByID("Patient");
        //                    if (!(patientFound is Patient))
        //                        break;
        //                    else
        //                        EditPatient(patientFound);
        //                    break;

        //                case "3":
        //                    Visit visitFound = (Visit)recieveByID("Visit");
        //                    if (!(visitFound is Visit))
        //                        break;
        //                    else
        //                        EditVisit(visitFound);
        //                    break;

        //                case "4":
        //                    Treatment treatmentFound = (Treatment)recieveByID("Treatment");
        //                    if (!(treatmentFound is Treatment))
        //                        break;
        //                    else
        //                        EditTreatment(treatmentFound);
        //                    break;

        //                case "0"://previous menu
        //                    keepRunning = false;
        //                    break;

        //                default:
        //                    Console.WriteLine("Please enter a valid choice");
        //                    break;
        //            }
        //        }
        //        catch (FormatException)//if the user entered something that isn't an integer.
        //        {
        //            Console.WriteLine("invalid ID. please type in a valid ID. (integers only)");
        //        }
        //    }
        //}

        //private void EditDoctor(Doctor doctorToEdit)
        //{
        //    Console.Clear();
        //    Console.WriteLine(doctorToEdit.ToString());
        //    bool keepRunning = true;

        //    while (keepRunning)
        //    {
        //        Console.WriteLine("What would you like to do?");
        //        //Console.WriteLine("1 - Edit Doctor ID");
        //        Console.WriteLine("1 - Edit Doctor first name");//V
        //        Console.WriteLine("2 - Edit Doctor last name");//V
        //        Console.WriteLine("3 - Edit Doctor Salary");//V
        //        Console.WriteLine("4 - Edit Doctor Gender");//V
        //        Console.WriteLine("5 - Add a Patient to the Doctor's patient list");//V
        //        Console.WriteLine("6 - Remove a Patient from the Doctor's patient list");//V
        //        Console.WriteLine("0 - Previous Menu");
        //        string choice = Console.ReadLine();

        //        try
        //        {
        //            switch (choice)
        //            {
        //                //case "1":
        //                //    Console.WriteLine("Enter the new ID: ");
        //                //    int ID = int.Parse(Console.ReadLine());//throws FormatException in case of bad input
        //                //    doctorToEdit.iID = ID;
        //                //    break;

        //                case "1":
        //                    Console.WriteLine("Enter the new First name: ");
        //                    string firstName = Console.ReadLine();
        //                    doctorToEdit.sFirst_name = firstName;
        //                    break;

        //                case "2":
        //                    Console.WriteLine("Enter the new Last name: ");
        //                    string lastName = Console.ReadLine();
        //                    doctorToEdit.sLast_Name = lastName;
        //                    break;

        //                case "3":
        //                    Console.WriteLine("Enter the new Salary: ");
        //                    double salary = double.Parse(Console.ReadLine());//throws FormatException in case of bad input
        //                    doctorToEdit.dSalary = salary;
        //                    break;

        //                case "4":
        //                    Console.WriteLine("Enter the new Gender: ");
        //                    char gender = char.Parse(Console.ReadLine());
        //                    doctorToEdit.cGender = gender;
        //                    break;

        //                case "5":
        //                    Patient newPatient = (Patient)recieveByID("Patient");
        //                    if (newPatient == null)
        //                        break;
        //                    else
        //                    {
        //                        doctorToEdit.addPatient(newPatient);
        //                        Doctor dctToRemove = itsDoctor_BL.getDoctorByID(newPatient.MainDoctor);
        //                        dctToRemove.removePatient(newPatient);
        //                    }

        //                    break;

        //                case "6":
        //                    Patient newPatientToRemove = (Patient)recieveByID("Patient");
        //                    if (!(newPatientToRemove is Patient))
        //                        break;
        //                    else
        //                        doctorToEdit.removePatient(newPatientToRemove);
        //                    break;

        //                case "0"://previous menu
        //                    keepRunning = false;
        //                    break;

        //                default:
        //                    Console.WriteLine("Please enter a valid choice");
        //                    break;

        //            }
        //        }
        //        catch (Exception e)// catches format exceptions, wrong ids, wrong names
        //        {
        //            Console.WriteLine(e.Message);
        //        }
        //    }
        //}
        //private void EditPatient(Patient patientToEdit)
        //{
        //    Console.Clear();
        //    Console.WriteLine(patientToEdit.ToString());
        //    bool keepRunning = true;

        //    while (keepRunning)
        //    {
        //        try
        //        {
        //            Console.WriteLine("What would you like to do?");
        //            Console.WriteLine("1 - Edit Patient first name");//V
        //            Console.WriteLine("2 - Edit Patient last name");//V
        //            Console.WriteLine("3 - Edit Patient age");//V
        //            Console.WriteLine("4 - Edit Patient Gender");//V
        //            Console.WriteLine("5 - Edit Patient's main doctor");//V
        //            Console.WriteLine("6 - Remove patient's visit");//V
        //            Console.WriteLine("7 - Remove patient's treatment");//V
        //            Console.WriteLine("0 - Previous Menu");
        //            string choice = Console.ReadLine();
        //            switch (choice)
        //            {
        //                case "1":
        //                    Console.WriteLine("Enter the new First name: ");
        //                    string firstName = Console.ReadLine();
        //                    patientToEdit.sFirst_name = firstName;
        //                    break;

        //                case "2":
        //                    Console.WriteLine("Enter the new Last name: ");
        //                    string lastName = Console.ReadLine();
        //                    patientToEdit.sLast_Name = lastName;
        //                    break;

        //                case "3":
        //                    Console.WriteLine("Enter the new age: ");
        //                    int age = int.Parse(Console.ReadLine());//throws FormatException in case of bad input
        //                    patientToEdit.iAge = age;
        //                    break;

        //                case "4":
        //                    Console.WriteLine("Enter the new Gender: ");
        //                    char gender = char.Parse(Console.ReadLine());
        //                    patientToEdit.cGender = gender;
        //                    break;

        //                case "5":
        //                    Doctor newMainDoctor = (Doctor)recieveByID("Doctor");
        //                    if (newMainDoctor == null)
        //                        break;

        //                    else
        //                    {
        //                        Doctor toRemove = itsDoctor_BL.getDoctorByID(patientToEdit.MainDoctor);
        //                        if (toRemove != null)
        //                        {
        //                            toRemove.removePatient(patientToEdit);
        //                        }
        //                        patientToEdit.MainDoctor = newMainDoctor.iID;
        //                        newMainDoctor.addPatient(patientToEdit);


        //                    }
        //                    break;

        //                case "6":
        //                    Visit visitToRemove = (Visit)recieveByID("Visit");
        //                    if (visitToRemove == null)
        //                        break;
        //                    else
        //                        patientToEdit.RemoveVisit(visitToRemove);
        //                    break;

        //                case "7":
        //                    Treatment treatmentToRemove = (Treatment)recieveByID("Treatment");
        //                    if (treatmentToRemove == null)
        //                        break;
        //                    else
        //                        patientToEdit.RemoveTreatment(treatmentToRemove);
        //                    break;

        //                case "0"://previous menu
        //                    keepRunning = false;
        //                    break;

        //                default:
        //                    Console.WriteLine("Please enter a valid choice");
        //                    break;

        //            }
        //        }
        //        catch (Exception e)// catches format exceptions, wrong ids, wrong names
        //        {
        //            Console.WriteLine(e.Message);
        //        }

        //    }
        //}
        //private void EditVisit(Visit visitToEdit)
        //{
        //    Console.Clear();
        //    Console.WriteLine(visitToEdit.ToString());
        //    bool keepRunning = true;

        //    while (keepRunning)
        //    {
        //        try
        //        {
        //            Console.WriteLine("1- Edit date of visit: ");//V
        //            Console.WriteLine("2- Change assigned Doctor");//V
        //            Console.WriteLine("3- Add Doctor's notes");//V
        //            Console.WriteLine("4- Write new doctor notes");//V
        //            Console.WriteLine("5- Add Treatment");//V
        //            Console.WriteLine("6- Remove Treatment");//V
        //            Console.WriteLine("0- Previous Menu");
        //            string choice = Console.ReadLine();
        //            switch (choice)
        //            {
        //                case "1":
        //                    Console.WriteLine("Enter Date of Visit (<month>/<day>/<year> <hour>:<minutes>");
        //                    DateTime newDateOfVisit = DateTime.Parse(Console.ReadLine(),
        //                     System.Globalization.CultureInfo.InvariantCulture);
        //                    visitToEdit.dateOfVisit = newDateOfVisit;
        //                    break;

        //                case "2":
        //                    Doctor newAssignedDoctor = (Doctor)recieveByID("Doctor");
        //                    if (newAssignedDoctor == null)
        //                        break;
        //                    else
        //                        visitToEdit.AssignedDoctor = newAssignedDoctor.iID;
        //                    break;

        //                case "3":
        //                    Console.WriteLine("Type in additional Doctor's notes: ");
        //                    visitToEdit.sDoctorNotes += "\n" + Console.ReadLine();
        //                    break;

        //                case "4":
        //                    Console.WriteLine("Type in new Doctor's notes: ");
        //                    visitToEdit.sDoctorNotes = Console.ReadLine();
        //                    break;

        //                case "5":
        //                    Treatment newTreatment = (Treatment)recieveByID("Treatment");
        //                    if (newTreatment == null)
        //                        break;
        //                    else
        //                        visitToEdit.AddTreatment(newTreatment);
        //                    break;

        //                case "6":
        //                    Treatment treatmentToRemove = (Treatment)recieveByID("Treatment");
        //                    if (treatmentToRemove == null)
        //                        break;
        //                    else
        //                        visitToEdit.RemoveTreatment(treatmentToRemove);
        //                    break;

        //                case "0"://previous menu
        //                    keepRunning = false;
        //                    break;

        //                default:
        //                    Console.WriteLine("Please enter a valid choice");
        //                    break;

        //            }
        //        }
        //        catch (Exception e)// catches format exceptions, wrong ids, wrong names
        //        {
        //            Console.WriteLine(e.Message);
        //        }
        //    }
        //}
        //private void EditTreatment(Treatment treatmentToEdit)
        //{
        //    Console.Clear();
        //    Console.WriteLine(treatmentToEdit.ToString());
        //    bool keepRunning = true;

        //    while (keepRunning)
        //    {
        //        try
        //        {
        //            Console.WriteLine("1- Edit date of Start: ");//V
        //            Console.WriteLine("2- Edit date of Finish: ");//V
        //            Console.WriteLine("3- Write new Prognosis");//V
        //            Console.WriteLine("4- Add Prognosis data");//V
        //            Console.WriteLine("5- Add Prescription");//V
        //            Console.WriteLine("6- Remove Prescription");//V
        //            Console.WriteLine("0- Previous Menu");
        //            string choice = Console.ReadLine();
        //            switch (choice)
        //            {
        //                case "1":
        //                    Console.WriteLine("Enter Date of Start (<month>/<day>/<year> <hour>:<minutes>");
        //                    DateTime newDateOfStart = DateTime.Parse(Console.ReadLine(),
        //                     System.Globalization.CultureInfo.InvariantCulture);
        //                    treatmentToEdit.DateOfStart = newDateOfStart;
        //                    break;

        //                case "2":
        //                    Console.WriteLine("Enter Date of Finish (<month>/<day>/<year> <hour>:<minutes>");
        //                    DateTime newDateOfFinish = DateTime.Parse(Console.ReadLine(),
        //                     System.Globalization.CultureInfo.InvariantCulture);
        //                    treatmentToEdit.DateOfFinish = newDateOfFinish;
        //                    break;

        //                case "3":
        //                    Console.WriteLine("Enter new Prognosis: ");
        //                    treatmentToEdit.RemovePrognosis();
        //                    treatmentToEdit.AddPrognosis(Console.ReadLine());
        //                    break;

        //                case "4":
        //                    Console.WriteLine("Enter additional Prognosis data: ");
        //                    treatmentToEdit.AddPrognosis(Console.ReadLine());
        //                    break;

        //                case "5":
        //                    Console.WriteLine("Enter Prescription to add: ");
        //                    treatmentToEdit.addPrescriptions(Console.ReadLine());
        //                    break;

        //                case "6":
        //                    Console.WriteLine("Enter Prescription to remove: ");
        //                    treatmentToEdit.removePrescriptions(Console.ReadLine());
        //                    break;

        //                case "0"://previous menu
        //                    keepRunning = false;
        //                    break;

        //                default:
        //                    Console.WriteLine("Please enter a valid choice");
        //                    break;

        //            }
        //        }
        //        catch (Exception e)// catches format exceptions, wrong ids, wrong names
        //        {
        //            Console.WriteLine(e.Message);
        //        }
        //    }


        //}

        //private void AddMenu()
        //{
        //    Console.Clear();
        //    bool keepRunning = true;
        //    while (keepRunning == true)
        //    {
        //        try
        //        {
        //            Console.WriteLine("1- Add a Doctor");
        //            Console.WriteLine("2- Add a Patient");
        //            Console.WriteLine("3- Add a Visit");
        //            Console.WriteLine("4- Add a Treatment");
        //            Console.WriteLine("0- Previous Menu");
        //            string choice = Console.ReadLine();
        //            switch (choice)
        //            {
        //                case "1":
        //                    Doctor dctCurrent = itsDoctor_BL.createDoctor();
        //                    //itsDAL.addDocto
        //                    Console.WriteLine("Success.");
        //                    break;

        //                case "2":
        //                    itsPatient_BL.createPatient(itsDoctor_BL);
        //                    Console.WriteLine("Success.");
        //                    break;

        //                case "3":
        //                    itsVisit_BL.createVisit(itsDoctor_BL, itsTreatment_BL);
        //                    Console.WriteLine("Success.");
        //                    break;

        //                case "4":
        //                    itsTreatment_BL.createTreatment(itsDoctor_BL);
        //                    Console.WriteLine("Success.");
        //                    break;

        //                case "0"://previous menu
        //                    keepRunning = false;
        //                    break;

        //                default:
        //                    Console.WriteLine("Please enter a valid choice");
        //                    break;


        //            }
        //        }
        //        catch (Exception e)// catches format exceptions, wrong ids, wrong names
        //        {
        //            Console.WriteLine(e.Message);
        //        }
        //    }
        //}
        //// show the user an options for query
        //private void QueryMenu()
        //{
        //    Console.Clear();

        //    bool keepRunning = true;
        //    while (keepRunning == true)
        //    {
        //        try
        //        {
        //            Console.WriteLine("What would you like to search for?");
        //            Console.WriteLine("1- Doctors");
        //            Console.WriteLine("2- Patients");
        //            Console.WriteLine("3- Visits");
        //            Console.WriteLine("4- Treatments");
        //            Console.WriteLine("0- Previous Menu");
        //            string choice = Console.ReadLine();
        //            switch (choice)
        //            {
        //                case "1": // info about doctors
        //                    InfoAboutDoctors();
        //                    break;

        //                case "2": // info about Patients
        //                    InfoAboutPatients();
        //                    break;

        //                case "3":  // info about Visits
        //                    InfoAboutVisits();
        //                    break;

        //                case "4":  // info about Treatments
        //                    InfoAboutTreatments();
        //                    break;

        //                case "0":  //previous menu
        //                    keepRunning = false;
        //                    break;

        //                default:
        //                    Console.WriteLine("Please enter a valid choice");
        //                    break;


        //            }
        //        }
        //        catch (Exception e)// catches format exceptions, wrong ids, wrong names
        //        {
        //            Console.WriteLine(e.Message);
        //        }
        //    }
        //}



        //private void InfoAboutDoctors()
        //{
        //    Console.Clear();
        //    // deep copy of the data base
        //    List<Doctor> DB_Doctors = new List<Doctor>();
        //    foreach (Doctor doctor in itsDoctor_BL.getItsDal())
        //    {
        //        DB_Doctors.Add(doctor);
        //    }
        //    //initialize the patient list
        //    List<Patient> DB_Patient = new List<Patient>();
        //    foreach (Patient patient in itsPatient_BL.getItsDal())
        //    {
        //        DB_Patient.Add(patient);
        //    }


        //    bool keepRunning = true;
        //    while (keepRunning == true)
        //    {
        //        try
        //        {
        //            Console.WriteLine("What would you like to search for?");
        //            Console.WriteLine("1- Sort all doctors by ID");//V
        //            Console.WriteLine("2- Sort all doctors by gender");//V
        //            Console.WriteLine("3- Sort all doctors by salary");//V
        //            Console.WriteLine("4- Sort all doctors by first name");//V
        //            Console.WriteLine("5- Sort all doctors by last name");//V
        //            Console.WriteLine("6- Get doctor's patients list");//V
        //            Console.WriteLine("7- Get all doctors list");//V
        //            Console.WriteLine("8- Export data to XML file");//V
        //            Console.WriteLine("0- Previous Menu");
        //            string choice = Console.ReadLine();
        //            switch (choice)
        //            {
        //                case "1": // Sort all doctors by ID    
        //                    //List<Doctor> listId = new List<Doctor>();
        //                    Console.WriteLine("Enter a requested doctor's ID to begin with");
        //                    int choiceId = Convert.ToInt32(Console.ReadLine());
        //                    // @TODO - does the input valid?
        //                    DB_Doctors = itsQueries.sortByIDDoc(DB_Doctors, choiceId);
        //                    DisplayListofDoctor(DB_Doctors);
        //                    break;

        //                case "2": //Sort all doctors by gender
        //                    Console.WriteLine("Enter a requested doctor's gender , 'M' for male, 'F' for female");
        //                    Char choiceG = Convert.ToChar(Console.ReadLine());
        //                    // @TODO - does the input valid? 
        //                    DB_Doctors = itsQueries.SortByGenderDoc(DB_Doctors, choiceG);
        //                    DisplayListofDoctor(DB_Doctors);
        //                    break;

        //                case "3":  // Sort all doctors by salary
        //                    Console.WriteLine("Enter a requested doctor's salary range , low range first, high range last");
        //                    int choiceSal1 = Convert.ToInt32(Console.ReadLine());
        //                    int choiceSal2 = Convert.ToInt32(Console.ReadLine());
        //                    // @TODO - does the input valid?
        //                    DB_Doctors = itsQueries.SortBySalary(DB_Doctors, choiceSal1, choiceSal2);
        //                    DisplayListofDoctor(DB_Doctors);
        //                    break;
        //                case "4":  //  Sort all doctors by first name - returns all the doctors with the requested same name
        //                    Console.WriteLine("Enter a requested doctor's first name ");
        //                    String choiceFN = Console.ReadLine();
        //                    // @TODO - does the input valid?           
        //                    DB_Doctors = itsQueries.sortByFirstNameDoc(DB_Doctors, choiceFN);
        //                    DisplayListofDoctor(DB_Doctors);
        //                    break;
        //                case "5":  //  Sort all doctors by last name - returns all the doctors with the requested same last name
        //                    Console.WriteLine("Enter a requested doctor's last name ");
        //                    String choiceLN = Console.ReadLine();
        //                    // @TODO - does the input valid?
        //                    DB_Doctors = itsQueries.sortByLastNameDoc(DB_Doctors, choiceLN);
        //                    DisplayListofDoctor(DB_Doctors);
        //                    break;
        //                case "6":  //  Get doctor's patients list
        //                    List<Patient> listPL = new List<Patient>();
        //                    Console.WriteLine("For view the doctor's patients list, Enter the requested doctor's ID ");
        //                    int choicePL = Convert.ToInt32(Console.ReadLine());
        //                    Doctor doc = itsDoctor_BL.getDoctorByID(choicePL);
        //                    // @TODO - does the input valid?
        //                    listPL = itsDoctor_BL.getPatientsByInt(DB_Patient, doc.getPatients());
        //                    DisplayListofPatients(listPL);
        //                    break;

        //                case "7":  // Get all doctors list         
        //                    DisplayListofDoctor(DB_Doctors);
        //                    break;

        //                case "8":  // export to XML file      
        //                    Console.WriteLine("Enter the file name:");
        //                    itsDoctor_BL.ExportXML(DB_Doctors, Console.ReadLine());
        //                    break;

        //                case "0":  //previous menu
        //                    keepRunning = false;
        //                    break;

        //                default:
        //                    Console.WriteLine("Please enter a valid choice");
        //                    break;
        //            }
        //        }
        //        catch (Exception e)// catches format exceptions, wrong ids, wrong names
        //        {
        //            Console.WriteLine(e.Message);
        //        }
        //    }
        //}

        //private void InfoAboutPatients()
        //{
        //    Console.Clear();
        //    // deep copy of the data base
        //    List<Patient> DB_Patient = new List<Patient>();
        //    foreach (Patient patient in itsPatient_BL.getItsDal())
        //    {
        //        DB_Patient.Add(patient);
        //    }

        //    bool keepRunning = true;
        //    while (keepRunning == true)
        //    {
        //        try
        //        {
        //            Console.WriteLine("What would you like to search for?");
        //            Console.WriteLine("1- Sort all patient by ID");//v
        //            Console.WriteLine("2- Sort all patient by gender");//v
        //            Console.WriteLine("3- Sort all patient by first name");//v
        //            Console.WriteLine("4- Sort all patient by last name");//v
        //            Console.WriteLine("5- Get patient's visits made list");//???????
        //            Console.WriteLine("6- Get patient's treatments made list");//??????
        //            Console.WriteLine("7- Get all patients list");//V
        //            Console.WriteLine("8- Export Data to XML file");//V
        //            Console.WriteLine("0- Previous Menu");
        //            string choice = Console.ReadLine();
        //            switch (choice)
        //            {
        //                case "1": // Sort all patient by ID  PatientsortByID
        //                    Console.WriteLine("Enter a requested patient's ID to begin with");
        //                    int choiceId = Convert.ToInt32(Console.ReadLine());
        //                    // @TODO - does the input valid?   
        //                    DB_Patient = itsQueries.sortByIDP(DB_Patient, choiceId);
        //                    DisplayListofPatients(DB_Patient);
        //                    break;

        //                case "2": //Sort all patient by gender
        //                    Console.WriteLine("Enter a requested patient's gender , 'M' for male, 'F' for female");
        //                    Char choiceG = Convert.ToChar(Console.ReadLine());
        //                    // @TODO - does the input valid?
        //                    DB_Patient = itsQueries.SortByGenderP(DB_Patient, choiceG);
        //                    DisplayListofPatients(DB_Patient);
        //                    break;

        //                case "3":  // Sort all patient by first name  
        //                    Console.WriteLine("Enter a requested Patient's first name");
        //                    String choiceFN = Console.ReadLine();
        //                    // @TODO - does the input valid?
        //                    DB_Patient = itsQueries.sortByFirstNameP(DB_Patient, choiceFN);
        //                    DisplayListofPatients(DB_Patient);
        //                    break;

        //                case "4":  //  Sort all patient by last name  
        //                    Console.WriteLine("Enter a requested doctor's last name ");
        //                    String choiceLN = Console.ReadLine();
        //                    // @TODO - does the input valid?
        //                    DB_Patient = itsQueries.sortByLastNameP(DB_Patient, choiceLN);
        //                    DisplayListofPatients(DB_Patient);
        //                    break;

        //                case "5":  // Get patient's visits made list
        //                    List<Visit> listV = new List<Visit>();
        //                    Console.WriteLine("Enter a requested Patient's ID ");
        //                    // @TODO - does the input valid?
        //                    int choiceV = Convert.ToInt32(Console.ReadLine());
        //                    listV = itsQueries.VisitsMadeByPatient(choiceV);
        //                    DisplayListofVisit(listV);
        //                    break;

        //                case "6":  // Get patient's treatments made list
        //                    List<Treatment> listTL = new List<Treatment>();
        //                    Console.WriteLine("For view the patient's treatments list, Enter the requested patient's ID ");
        //                    int choiceTL = Convert.ToInt32(Console.ReadLine());
        //                    // @TODO - does the input valid?
        //                    listTL = itsQueries.TreatmentsMadeByPatient(choiceTL);
        //                    DisplayListOfTreatments(listTL);
        //                    break;

        //                case "7":  //  Get all patients list
        //                    DisplayListofPatients(DB_Patient);
        //                    break;

        //                case "8":  // export to XML file       
        //                    Console.WriteLine("Enter the file name:");
        //                    itsPatient_BL.ExportXML(DB_Patient, Console.ReadLine());
        //                    break;

        //                case "0":  //previous menu
        //                    keepRunning = false;
        //                    break;

        //                default:
        //                    Console.WriteLine("Please enter a valid choice");
        //                    break;
        //            }
        //        }
        //        catch (Exception e)// catches format exceptions, wrong ids, wrong names
        //        {
        //            Console.WriteLine(e.Message);
        //        }
        //    }
        //}

        //private void InfoAboutVisits()
        //{
        //    Console.Clear();
        //    // deep copy of the data base
        //    List<Visit> DB_Visits = new List<Visit>();
        //    foreach (Visit visit in itsVisit_BL.getItsDal())
        //    {
        //        DB_Visits.Add(visit);
        //    }
        //    bool keepRunning = true;
        //    while (keepRunning == true)
        //    {
        //        try
        //        {
        //            Console.WriteLine("What would you like to search for?");
        //            Console.WriteLine("1- Sort all visits by ID");//V
        //            Console.WriteLine("2- Sort all visits by Date");//V
        //            Console.WriteLine("3- Get all visit list");//V
        //            Console.WriteLine("4- Export Data to XML file");//V
        //            Console.WriteLine("0- Previous Menu");
        //            string choice = Console.ReadLine();
        //            switch (choice)
        //            {
        //                case "1": // Sort all visits by ID
        //                    Console.WriteLine("Enter a requested visit's ID to begin with");
        //                    int choiceId = Convert.ToInt32(Console.ReadLine());
        //                    // @TODO - does the input valid?   
        //                    DB_Visits = itsQueries.SortVisitById(DB_Visits, choiceId);
        //                    DisplayListofVisit(DB_Visits);
        //                    break;

        //                case "2": //Sort all visits by Date
        //                    Console.WriteLine("Enter Start day (<month>/<day>/<year> <hour>:<minutes>");
        //                    DateTime choiceG = DateTime.Parse(Console.ReadLine(),
        //                     System.Globalization.CultureInfo.InvariantCulture);
        //                    // @TODO - does the input valid?
        //                    DB_Visits = itsQueries.SortVisitByDate(DB_Visits, choiceG);
        //                    DisplayListofVisit(DB_Visits);
        //                    break;

        //                case "3":  //  Get all visit list
        //                    DisplayListofVisit(DB_Visits);
        //                    break;

        //                case "4":  // export to XML file       
        //                    Console.WriteLine("Enter the file name:");
        //                    itsVisit_BL.ExportXML(DB_Visits, Console.ReadLine());
        //                    break;

        //                case "0":  //previous menu
        //                    keepRunning = false;
        //                    break;

        //                default:
        //                    Console.WriteLine("Please enter a valid choice");
        //                    break;
        //            }
        //        }
        //        catch (Exception e)// catches format exceptions, wrong ids, wrong names
        //        {
        //            Console.WriteLine(e.Message);
        //        }
        //    }
        //}

        //private void InfoAboutTreatments()
        //{
        //    Console.Clear();
        //    // deep copy of the data base
        //    List<Treatment> DB_Treatments = new List<Treatment>();
        //    foreach (Treatment treatment in itsTreatment_BL.getItsDal())
        //    {
        //        DB_Treatments.Add(treatment);
        //    }
        //    bool keepRunning = true;
        //    while (keepRunning == true)
        //    {
        //        try
        //        {
        //            Console.WriteLine("What would you like to search for?");
        //            Console.WriteLine("1- Sort all treatments by ID");//v
        //            Console.WriteLine("2- Sort all treatments by date of start");//v
        //            Console.WriteLine("3- Sort all treatments by date of finish");//v
        //            Console.WriteLine("4- Sort all treatments by doctor in charge");//v
        //            Console.WriteLine("5- Get all treatment list");//v
        //            Console.WriteLine("6- Export Data to XML file");
        //            Console.WriteLine("0- Previous Menu");
        //            string choice = Console.ReadLine();
        //            switch (choice)
        //            {
        //                case "1": // Sort all treatments by ID
        //                    Console.WriteLine("Enter a requested treatment's ID to begin with");
        //                    int choiceId = Convert.ToInt32(Console.ReadLine());
        //                    // @TODO - does the input valid?   
        //                    DB_Treatments = itsQueries.SortTreatmentsById(DB_Treatments, choiceId);
        //                    DisplayListOfTreatments(DB_Treatments);
        //                    break;

        //                case "2": //Sort all treatments by date of start
        //                    Console.WriteLine("Enter a requested date to begin with  (<month>/<day>/<year> <hour>:<minutes>");
        //                    DateTime choiceG = DateTime.Parse(Console.ReadLine(),
        //                     System.Globalization.CultureInfo.InvariantCulture);
        //                    // @TODO - does the input valid?
        //                    DB_Treatments = itsQueries.SortTreatmentsByDate(DB_Treatments, choiceG);
        //                    DisplayListOfTreatments(DB_Treatments);
        //                    break;

        //                case "3": //Sort all treatments by date of finish
        //                    Console.WriteLine("Enter a requested date to end with  (<month>/<day>/<year> <hour>:<minutes>");
        //                    DateTime choiceF = DateTime.Parse(Console.ReadLine(),
        //                     System.Globalization.CultureInfo.InvariantCulture);
        //                    // @TODO - does the input valid?
        //                    DB_Treatments = itsQueries.SortTreatmentsByDateEnd(DB_Treatments, choiceF);
        //                    DisplayListOfTreatments(DB_Treatments);
        //                    break;

        //                case "4": //Sort all treatments by doctor in charge
        //                    Console.WriteLine("Enter a requested Doctor Id");
        //                    int choiceD = Convert.ToInt32(Console.ReadLine());
        //                    // @TODO - does the input valid?
        //                    DB_Treatments = itsQueries.SortTreatmentsByDoctors(DB_Treatments, choiceD);
        //                    DisplayListOfTreatments(DB_Treatments);
        //                    break;

        //                case "5":  //  Get all treatments list
        //                    DisplayListOfTreatments(DB_Treatments);
        //                    break;

        //                case "6":  // export to XML file       
        //                    Console.WriteLine("Enter the file name:");
        //                    itsTreatment_BL.ExportXML(DB_Treatments, Console.ReadLine());
        //                    break;


        //                case "0":  //previous menu
        //                    keepRunning = false;
        //                    break;

        //                default:
        //                    Console.WriteLine("Please enter a valid choice");
        //                    break;
        //            }
        //        }
        //        catch (Exception e)// catches format exceptions, wrong ids, wrong names
        //        {
        //            Console.WriteLine(e.Message);
        //        }
        //    }
        //}


        ////added an empty list notifier to all of these 5 functions (better late than never)
        //private void DisplayListOfTreatments(List<Treatment> list)
        //{
        //    if (list.Count == 0)
        //        Console.WriteLine("No matches were found");

        //    foreach (Treatment o in list)
        //    {
        //        Console.WriteLine(o.ToString(o));
        //    }
        //}
        //private void DisplayListofVisit(List<Visit> list)
        //{
        //    if (list.Count == 0)
        //        Console.WriteLine("No matches were found");

        //    foreach (Visit o in list)
        //    {
        //        Console.WriteLine(o.ToString(o));
        //    }
        //}
        //private void DisplayListofDoctor(List<Doctor> list)
        //{
        //    if (list.Count == 0)
        //        Console.WriteLine("No matches were found");

        //    foreach (Doctor o in list)
        //    {
        //        Console.WriteLine(o.ToString(o));
        //    }
        //}
        //private void DisplayListofPatients(List<Patient> list)
        //{
        //    if (list.Count == 0)
        //        Console.WriteLine("No matches were found");

        //    foreach (Patient o in list)
        //    {
        //        Console.WriteLine(o.ToString(o));
        //    }
        //}
        //private void DisplayList(List<Object> list)
        //{
        //    if (list.Count == 0)
        //        Console.WriteLine("No matches were found");

        //    foreach (object o in list)
        //    {
        //        Console.WriteLine(o.ToString());
        //    }
        //}

        ///*
        // * gets a type of object to search (for example Doctor or Treatment), and searches
        // * the DB for an instance of that object by the ID given by the user (also in this function)
        // * returns the required oject if it was found, or null if it wasn't.
        // */
        //private object recieveByID(string entityToSearchFor)
        //{
        //    Console.Clear();
        //    bool keepRunning = true;
        //    Console.WriteLine("enter the " + entityToSearchFor + " ID:");
        //    int ID = int.Parse(Console.ReadLine());//throws FormatException in case of bad input
        //    while (keepRunning)
        //    {
        //        switch (entityToSearchFor)
        //        {
        //            //if the type of object to be searched for is a doctor
        //            case "doctor":
        //            case "Doctor":
        //                Doctor doctorFound = itsDoctor_BL.getDoctorByID(ID);
        //                if (doctorFound == null)
        //                    goto case "0";
        //                else
        //                    return doctorFound;

        //            //if the type of object to be searched for is a patient
        //            case "patient":
        //            case "Patient":
        //                Patient patientFound = itsPatient_BL.getPatientByID(ID);
        //                if (patientFound == null)
        //                    goto case "0";
        //                else
        //                    return patientFound;

        //            //if the type of object to be searched for is a visit
        //            case "visit":
        //            case "Visit":
        //                Visit visitFound = itsVisit_BL.getVisitByID(ID);
        //                if (visitFound == null)
        //                    goto case "0";
        //                else
        //                    return visitFound;

        //            //if the type of object to be searched for is a treatment
        //            case "treatment":
        //            case "Treatment":
        //                Treatment treatmentFound = itsTreatment_BL.getTreatmentByID(ID);
        //                if (treatmentFound == null)
        //                    goto case "0";
        //                else
        //                    return treatmentFound;

        //            case "0"://when the user enters a bad ID.
        //                Console.WriteLine("ID was not found in the database. Try again or press 0 to return to the previous menu.");
        //                ID = int.Parse(Console.ReadLine());
        //                if (ID == 0)//if the user wants to go back to the previous menu
        //                    keepRunning = false;
        //                break;

        //            default:
        //                return null;
        //        }
        //    }
        //    return null;
        //}

        //private void AccessMenu()
        //{
        //    Console.Clear();
        //    bool keepRunning = true;
        //    while (keepRunning)
        //    {
        //        Console.WriteLine("Enter username");
        //        int ID = int.Parse(Console.ReadLine());
        //        Console.WriteLine("Enter Password");
        //        int Pass = int.Parse(Console.ReadLine());

        //        //  bool access= itsSecurity.ValidateDetails (ID,Pass);
        //        // if (access == true) 
        //        //    {
        //        //      Console.WriteLine("success");
        //        //     }
        //        //  else 
        //        //     {
        //        // Console.WriteLine("Access denied");
        //        //  }
        //        //Console.ReadLine();
        //    }
        //}
    }
}
