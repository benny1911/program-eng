﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;
using BL_Backend;
using NUnit.Framework;

namespace BL_Backend_Tests
{
    [TestFixture]
    class TestLabTest
    {
        [Test]
        public void ChangeDoctor_Test()
        {
        Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F',false);
        Doctor doctor2 = new Doctor(987654322, "benny", "cohen", 2500, 'M',true);
        LabTest labTest1=new LabTest(1, new DateTime (1,1,2000),doctor1,204369763,null);
        labTest1.changeDoctor(doctor2);
        List<LabTest> expected= new List<LabTest>();
        expected.Add(labTest1);
        Assert.AreEqual(expected,labTest1);
        }

        [Test]
        public void editDateOfLabTest_test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F', false);
            LabTest labTest1 = new LabTest(1, new DateTime(1, 1, 2000), doctor1, 204369763, null);
            labTest1.editDateOfLabTest(new DateTime(1, 2, 2001));
            List<LabTest> expected = new List<LabTest>();
            expected.Add(labTest1);
            Assert.AreEqual(expected,labTest1);  
        }

        [Test]
        public void AddDoctorNotes_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F', false);
            LabTest labTest1 = new LabTest(1, new DateTime(1, 1, 2000), doctor1, 204369763, null);
            labTest1.AddDoctorNotes("blood test failed");
            String expected = "blood test failed";
            Assert.AreEqual(expected, labTest1.sDoctorNotes);
        }

        [Test]
        public void RemoveDoctorNotes_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F', false);
            LabTest labTest1 = new LabTest(1, new DateTime(1, 1, 2000), doctor1, 204369763, null);
            labTest1.AddDoctorNotes("blood test failed");
            labTest1.RemoveDoctorNotes();
            String expected = "";
            Assert.AreEqual(expected, labTest1.sDoctorNotes);
        }
   }
     
        
}
