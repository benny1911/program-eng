﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using BL_Backend;
using BL;

namespace BL_Backed_Tests
{
    [TestFixture]
    class TestVisit
    {
        [Test]
        public void AddDoctorNotes_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F', false);
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', 111111111);
            Visit visit = new Visit(12, new DateTime(2010, 12, 1), doctor1, 204369763, "sick", null);
            visit.AddDoctorNotes("need a medicine");
            String expected ="need a medicine";
            Assert.AreEqual(expected, visit.sDoctorNotes);
        }

        [Test]
        public void RemoveDoctorNotes_Test()
    {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F', false);
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', 111111111);
            Visit visit = new Visit(12, new DateTime(2010, 12, 1), doctor1, 204369763, "sick", null);
            visit.AddDoctorNotes("need a medicine");
            visit.RemoveDoctorNotes();
            String expected = "";
            Assert.AreEqual(expected, visit.sDoctorNotes);
    }
    }
}
