﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using BL_Backend;
using BL;

namespace BL_Backed_Tests
{

    [TestFixture]
    public class TestDoctor
    {


        [SetUp]
        public void init()
        {
            //every function initializes it's own variables.
        }

        [Test]
        //this function tests the functionality of the addPatient method in BL_Backend.Doctor - it checks that when adding a patient to the doctor's patient list, the ID does indeed get added.
        public void addPatient_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F',false);
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', doctor1.iID);
            doctor1.addPatient(patient1);
            List<int> expected = new List<int>();
            expected.Add(patient1.iID);
            Assert.AreEqual(doctor1.lpatients, expected);
        }

        [Test]
        public void removePatient_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F',false);
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', doctor1.iID);
            doctor1.addPatient(patient1);
            doctor1.removePatient(patient1);
            List<int> expected = new List<int>();
            Assert.AreEqual(doctor1.lpatients, expected);
        }



    }
}
