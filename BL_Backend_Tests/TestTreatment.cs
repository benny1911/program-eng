﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_Backend;
using NUnit.Framework;

namespace BL_Backend_Tests
{
    [TestFixture]
    class TestTreatment
    {
        [Test]
        public void removePrescriptons_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F',false);
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', 111111111);
            Visit visit = new Visit(12, new DateTime(2010, 12, 1), doctor1, 204369763, "sick", null);
            Treatment treatment1 = new Treatment(111, new DateTime(2010, 12, 1), new DateTime(2010, 12, 15), doctor1, "not looking too shabby", null, "Test", patient1.iID,14);
            treatment1.addPrescriptions("antibaotica");
            treatment1.addPrescriptions("norofen");
            treatment1.removePrescriptions("antibaotica");
            List<String> expected = new List<String>();
            expected.Add(treatment1.sPrescriptions[0]);
            expected.Add(treatment1.sPrescriptions[1]);
            expected.Remove(treatment1.sPrescriptions[1]);
            Assert.AreEqual(expected, patient1.lstTreatments);

        }

        public void AddPrognosis_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F', false);
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', 111111111);
            Treatment treatment1 = new Treatment(111, new DateTime(2010, 12, 1), new DateTime(2010, 12, 15), doctor1, "not looking too shabby", null, "Test", patient1.iID, 14);
            treatment1.AddPrognosis("the patient have olcus");
            List<String> expected = new List<String>();
            expected.Add(treatment1.sPrognosis);
            Assert.AreEqual(expected,treatment1.sPrognosis);
        }

        public void removePrognosis_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F', false);
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', 111111111);
            Treatment treatment1 = new Treatment(111, new DateTime(2010, 12, 1), new DateTime(2010, 12, 15), doctor1, "not looking too shabby", null, "Test", patient1.iID, 14);
            treatment1.AddPrognosis("the patient have olcus");
            treatment1.AddPrognosis("the patient have bacteria");
            treatment1.RemovePrognosis();
            List<String> expected = new List<String>();
            Assert.AreEqual(expected, treatment1.sPrognosis);
        }

    }
}
