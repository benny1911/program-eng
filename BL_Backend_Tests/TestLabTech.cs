﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;
using BL_Backend;
using NUnit.Framework;

namespace BL_Backend_Tests
{
    class TestLabTech
    {
        [Test]
        public void IsNameValid_Test()
        {
            LabTech labTech1 = new LabTech(200200200, "Yosi", "ronen", 2000, 'm');
            String expected="";
            Assert.AreNotSame(expected,labTech1.sFirst_name);
        }

        [Test]
        public void Equal_Test()
        {
            LabTech labTech1 = new LabTech(200200200, "Yosi", "ronen", 2000, 'm');
            LabTech labTech2 = new LabTech(200200210, "Yamit", "ronen", 2000, 'f');
            bool expected = false;
            Assert.AreEqual(expected,labTech1.equals(labTech2));
        }

    }
}
