﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_Backend;
using NUnit.Framework;
namespace BL_Backend_Tests
{
    [TestFixture]
    class TestPatient
    {

        [Test, ExpectedException]
        //this method makes sure that an invalid ID will not be accepted.
        public void NegativeID_Test()
        {
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F',111111111);
            patient1.iID = 6466;
        }

        [Test]
        public void AddVisit_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F',false);
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', 111111111);
            Visit visit1 = new Visit(111, new DateTime(2010, 12, 1), doctor1, patient1.iID, "Test", null);
            patient1.AddVisit(visit1);
            List<int> expected = new List<int>();
            expected.Add(visit1.iVisitID);
            Assert.AreEqual(patient1.lstVisits, expected);
        }

        [Test]
        public void AddTreatment_Test()
        {
          
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F',false);
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', 111111111);
            Treatment treatment1 = new Treatment(111, new DateTime(2010, 12, 1), new DateTime(2010, 12, 15),doctor1, "not looking too shabby",null, "Test", patient1.iID,14);
            patient1.AddTreatment(treatment1);
            List<int> expected = new List<int>();
            expected.Add(treatment1.iTreatmentID);
            Assert.AreEqual(patient1.lstTreatments, expected);
        }

        public void removeTreatment_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F', false);
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', 111111111);
            Treatment treatment1 = new Treatment(111, new DateTime(2010, 12, 1), new DateTime(2010, 12, 15), doctor1, "not looking too shabby", null, "Test1", patient1.iID, 14);
            Treatment treatment2 = new Treatment(112, new DateTime(2010, 13, 1), new DateTime(2010, 12, 16), doctor1, " little shabby", null, "Test2", patient1.iID, 15);
            Treatment treatment3 = new Treatment(113, new DateTime(2010, 14, 1), new DateTime(2010, 12, 17), doctor1, "shabby", null, "Test3", patient1.iID, 16);
            patient1.AddTreatment(treatment1);
            patient1.AddTreatment(treatment2);
            patient1.AddTreatment(treatment3);
            patient1.RemoveTreatment(treatment2);
            List<int> expected = new List<int>();
            expected.Add(treatment1.iTreatmentID);
            expected.Add(treatment3.iTreatmentID);
            Assert.AreEqual(patient1.lstTreatments, expected);
        }

        public void removeVisit_Test()
        {
            Doctor doctor1 = new Doctor(987654321, "ronit", "rosen", 2000, 'F', false);
            Patient patient1 = new Patient(204369763, "lisa", "templer", 24, 'F', 111111111);
            Visit visit1 = new Visit(111, new DateTime(2010, 12, 1), doctor1, patient1.iID, "Test", null);
            Visit visit2 = new Visit(112, new DateTime(2010, 13, 1), doctor1, patient1.iID, "Test1", null);
            patient1.AddVisit(visit1);
            patient1.AddVisit(visit2);
            patient1.RemoveVisit(visit1);
            List<int> expected = new List<int>();
            expected.Add(visit2.iVisitID);
            Assert.AreEqual(patient1.lstVisits, expected);
        }


    }
}
