﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI_PL
{
    public partial class getIdFromUser : Window
    {
        public int ID { get; set; }
        public getIdFromUser(string typeOfID)
        {
            this.ID = -1;
            InitializeComponent();
            ID_lbl.Content = "Enter " + typeOfID + " ID:";
            this.Visibility = System.Windows.Visibility.Visible;
        }

        public int getID()
        {
            return this.ID;
        }
        private void OK_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.ID = int.Parse(ID_txt.Text);//throw exception for wrong input
                this.Close();
            }
            catch (Exception ex)
            {
                displayError(ex);
            }
        }

        //tells the user to correct his input
        public void displayError(Exception ex)
        {
            errorDialog error = new errorDialog(ex);
            error.ShowDialog();
        }
    }
}
