﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using DAL;
using BL_Backend;

namespace GUI_PL
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        PasswordBox box = new PasswordBox();
        private Doctor_BL itsDoctor_BL;
        private Patient_BL itsPatient_BL;
        private Securities_BL itsSecurity;
        //all of the Business Layer classes that the GUI works with
        private IDAL itsDAL;
        private Visit_BL itsVisit_BL;
        private Treatment_BL itsTreatment_BL;
        private LabTest_BL itsLabTest_BL;
        private Queries itsQueries;
        //the gui stores the lists it uses as attributes for multiple queries (sort by name then sort by salary, for example)    
        public List<Doctor> currentDoctorsList { get; set; }
        public List<Patient> currentPatientsList { get; set; }
        public List<Visit> currentVisitsList { get; set; }
        public List<Treatment> currentTreatmentsList { get; set; }

        public Window1(Securities_BL itsSec)
        {
            InitializeComponent();
            //creates the database
            itsDAL = new SQL_DAL_implementation();
            itsDoctor_BL = new Doctor_BL(this.itsDAL);
            itsPatient_BL = new Patient_BL(itsDAL);
            itsVisit_BL = new Visit_BL(itsDAL);
            itsTreatment_BL = new Treatment_BL(itsDAL);
            itsLabTest_BL = new LabTest_BL(itsDAL);
            itsQueries = new Queries(itsDoctor_BL, itsPatient_BL, itsVisit_BL, itsTreatment_BL, itsLabTest_BL,itsDAL);
            currentDoctorsList = new List<Doctor>();
            currentPatientsList = new List<Patient>();
            currentVisitsList = new List<Visit>();
            currentTreatmentsList = new List<Treatment>();
            resetDoctors(currentDoctorsList);//get data from Databases
            resetPatients(currentPatientsList);
            resetVisits(currentVisitsList);
            resetTreatments(currentTreatmentsList);
            this.itsSecurity = itsSec;

        }

        public void ButtonClickToChangePass(object sender, RoutedEventArgs e)
        {
            {
                // the user want to change his password
                //convert the input user ID and pass to int
                String inputId = ID.Text;
                String inputOldPass = oldPassword.Password;
                String inputNewPass = newPassword.Password;
                //keep runing until he writes the accurate password
                int inputOldPassint=0;
                int inputNewPassint=0;
                try
                {
                   inputOldPassint = Convert.ToInt32(inputOldPass);
                   inputNewPassint = Convert.ToInt32(inputNewPass);
                }
                catch (Exception ex) {}
                bool ans1;
                // check if the input is valid: and if its a doctor or a patient
                bool ans = false;
                string name = "";
                string authorization = this.itsSecurity.theUserType(inputId, inputOldPassint);

                switch (authorization)
                {
                    case "Admin": name = "Manager";
                        break;
                    case "Patient": name = itsPatient_BL.getPatientByID(Convert.ToInt32(inputId)).sFirst_name;
                        break;
                    case "Doctor": name = itsDoctor_BL.getDoctorByID(Convert.ToInt32(inputId)).sFirst_name;
                        break;
                }
                if (authorization == "Admin" || authorization == "Patient" || authorization == "Doctor")
                {
                    ans = itsSecurity.ValidateDetails(inputId.ToString(), inputOldPassint);
                    if (ans && inputNewPassint != 0)
                    {
                        ans1 = itsSecurity.changePassword(inputId, inputOldPassint, inputNewPassint);
                        if (!(ans1)) MessageBox.Show("Wrong details, Try again please.");
                        else
                        {
                            MessageBox.Show(name + " has changed his/her password successfuly!");
                            this.Close();
                        }
                    }
                    else MessageBox.Show("Wrong old password, Try again please.");
                }
                else if (authorization == "wrong input" || authorization == "none") MessageBox.Show("User not found");
                else MessageBox.Show("Wrong details, Try again please.");
            }
        }


        private void resetDoctors(List<Doctor> listToReset)
        {
            listToReset.Clear();
            List<Doctor> doctors = itsDAL.allDoctors();
            foreach (Doctor doctorToAdd in doctors)
                listToReset.Add(doctorToAdd);
        }
        private void resetPatients(List<Patient> listToReset)
        {
            listToReset.Clear();
            List<Patient> patients = itsDAL.allPatients();
            foreach (Patient patientToAdd in patients)
                listToReset.Add(patientToAdd);
        }
        private void resetVisits(List<Visit> listToReset)
        {
            listToReset.Clear();
            List<Visit> visits = itsDAL.allVisits();
            foreach (Visit visitToAdd in visits)
                listToReset.Add(visitToAdd);
        }
        private void resetTreatments(List<Treatment> listToReset)
        {
            listToReset.Clear();
            List<Treatment> treatments = itsDAL.allTreatments();
            foreach (Treatment treatmentToAdd in treatments)
                listToReset.Add(treatmentToAdd);
        }
    }
}