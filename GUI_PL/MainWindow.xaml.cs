﻿using BL;
using BL_Backend;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Web;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
//using System.Linq;

namespace GUI_PL
{
    /// Interaction logic for MainWindow.xaml
    public partial class MainWindow : Window
    {
        //saves the window as a verible to the option to change password.
        private LoginWindow login;
        //all of the Business Layer classes that the GUI works with
        private IDAL itsDAL;
        private Doctor_BL itsDoctor_BL;
        private LabTech_BL itsLabTech_BL;
        private LabTest_BL itsLabTest_BL;
        private Patient_BL itsPatient_BL;
        private Visit_BL itsVisit_BL;
        private Treatment_BL itsTreatment_BL;
        private Securities_BL itsSecurity;
        private Queries itsQueries;
        private ExcelExports itsexportExcel;

        //the gui stores the lists it uses as attributes for multiple queries (sort by name then sort by salary, for example)    
        public List<Doctor> currentDoctorsList { get; set; }
        public List<Patient> currentPatientsList { get; set; }
        public List<Visit> currentVisitsList { get; set; }
        public List<Treatment> currentTreatmentsList { get; set; }
        public List<LabTech> currentLabTechList { get; set; }
        public List<LabTest> currentLabTestList { get; set; }
        private string userType;

        public MainWindow(IDAL itsDAL, Doctor_BL itsDoctor_BL, Patient_BL itsPatient_BL, Visit_BL itsVisit_BL, Treatment_BL itsTreatment_BL, 
                            LabTech_BL itsLabTech_BL, LabTest_BL itsLabTest_BL, Securities_BL itsSecurity , Queries itsQueries)
        {

            //creates the database
            this.itsDAL = itsDAL;
            this.itsDoctor_BL = itsDoctor_BL;
            this.itsPatient_BL = itsPatient_BL;
            this.itsVisit_BL = itsVisit_BL;
            this.itsTreatment_BL = itsTreatment_BL;
            this.itsLabTech_BL = itsLabTech_BL;
            this.itsLabTest_BL = itsLabTest_BL;
            this.itsQueries = itsQueries;
            this.itsSecurity = itsSecurity;
            this.itsexportExcel = new ExcelExports();
            currentDoctorsList = new List<Doctor>();
            currentPatientsList = new List<Patient>();
            currentVisitsList = new List<Visit>();
            currentTreatmentsList = new List<Treatment>();
            currentLabTestList = new List<LabTest>();
            currentLabTechList = new List<LabTech>();
            resetDoctors(currentDoctorsList);//get data from Databases
            resetPatients(currentPatientsList);
            resetVisits(currentVisitsList);
            resetTreatments(currentTreatmentsList);
            resetLabTechs(currentLabTechList);
            resetLabTests(currentLabTestList);


            //creates a login window
            login = new LoginWindow(itsDAL);

            //opens the login dialog
            login.ShowDialog();

            //after the login we switch to cases of an admin, doctor, patient, before opening this window, when each have other premissions
            InitializeComponent();

            //tries to get the username and password from the login window, if it doesn't match then closes the window
            int i = 0;
            try
            {
                i = Convert.ToInt32(login.password.Password);
            }
            catch (Exception) { this.Close(); }
            if (!(itsSecurity.ValidateDetails(login.ID.Text, i))) this.Close();

            //cases to set the window by autorization
            userType = itsSecurity.theUserType(login.ID.Text, i);
            switch (userType)
            {
                case "Doctor":
                    initializeByDoctor();
                    break;
                case "Patient":
                    initializeByPatient();
                    break;
                case "Admin":
                    initializeByAdmin();
                    break;
                case "Tech":
                    initializeByTech();
                    break;
                default:
                    break;
            }

            //sets the window to be unresizable
            this.ResizeMode = ResizeMode.NoResize;
            Get_Def.Visibility = System.Windows.Visibility.Visible;

        }

        #region initialize

        private void initializeByTech()
        {
            currentLabTestList.Clear();
            List<LabTest> labTests=itsDAL.allLabTests();
            foreach (LabTest p in labTests)
            {
                if (p.sResults.Equals(""))
                {
                    currentLabTestList.Add(p);
                }
            }
            addLabTestToTree(currentLabTestList);

            //disables all the functions except password change
            Admin.Visibility = System.Windows.Visibility.Collapsed;
            Doctor.Visibility = System.Windows.Visibility.Collapsed;
            Patient.Visibility = System.Windows.Visibility.Collapsed;
            LabTech.Visibility = System.Windows.Visibility.Collapsed;
            Visit.Visibility = System.Windows.Visibility.Collapsed;
            Treatment.Visibility = System.Windows.Visibility.Collapsed;
            classChange.Visibility = System.Windows.Visibility.Hidden;
            cmbQueries.Visibility = System.Windows.Visibility.Hidden;
            Add_Test.Visibility = System.Windows.Visibility.Collapsed;
            graph.Visibility = System.Windows.Visibility.Hidden;


        }

        //initializes the window by admin premmisions
        private void initializeByAdmin()
        {
            barChart.Visibility = System.Windows.Visibility.Collapsed;
        }
        //initializes the window by patients premmisions
        private void initializeByPatient()
        {
            //find the patient to add his properties to the tree
            List<Patient> patients = itsDAL.allPatients();
            foreach (Patient p in patients)
            {
                int k = Convert.ToInt32(login.ID.Text);
                if (k == p.iID)
                {
                    currentPatientsList.Clear();
                    currentPatientsList.Add(p);
                    break;
                }
            }
            currentVisitsList = itsDAL.allVisits();
            List<Visit> vst = new List<Visit>();
            foreach(Visit v in currentVisitsList)
            {
                if (v.iPatientId == currentPatientsList.ElementAt(0).iID) { vst.Add(v); }
                else
                {

                }
            }
            currentVisitsList = vst;
            currentTreatmentsList = itsDAL.allTreatments();
            List<Treatment> trm = new List<Treatment>();
            foreach (Treatment v in currentTreatmentsList)
            {
                if (v.iPatientID == currentPatientsList.ElementAt(0).iID) { trm.Add(v); }
                else
                {

                }
            }
            currentTreatmentsList = trm;
            currentLabTestList = itsDAL.allLabTests();
            List<LabTest> lab = new List<LabTest>();
            foreach (LabTest v in currentLabTestList)
            {
                if (v.iPatientId == currentPatientsList.ElementAt(0).iID) { lab.Add(v); }
                else
                {

                }
            }
            currentLabTestList = lab;
            //adds the patient to the tree
            addPatientsToTree(currentPatientsList);

            //initialize the combobox so the patient could save his own details

            classChange.SelectedItem = cmbPatient;
            //disables all the functions except password change
            Admin.Visibility = System.Windows.Visibility.Collapsed;
            Doctor.Visibility = System.Windows.Visibility.Collapsed;
            LabTech.Visibility = System.Windows.Visibility.Collapsed;
            LabTest.Visibility = System.Windows.Visibility.Collapsed;
            Patient.Visibility = System.Windows.Visibility.Collapsed;
            Add_treatment.Visibility = System.Windows.Visibility.Collapsed;
            Edit_treatment.Visibility = System.Windows.Visibility.Collapsed;
            Remove_treatment.Visibility = System.Windows.Visibility.Collapsed;
            Visit.Visibility = System.Windows.Visibility.Collapsed;
            Treatment.Visibility = System.Windows.Visibility.Visible;
            classChange.Visibility = System.Windows.Visibility.Hidden;
            cmbQueries.Visibility = System.Windows.Visibility.Hidden;
            graph.Visibility = System.Windows.Visibility.Hidden;
            Get_Def.Visibility = System.Windows.Visibility.Visible;

        }

        //initialize the window by doctors permissions
        private void initializeByDoctor()
        {

            currentPatientsList.Clear();
            currentDoctorsList.Clear();
            currentVisitsList.Clear();
            currentTreatmentsList.Clear();
            currentLabTestList.Clear();
            Doctor curr = itsDoctor_BL.getDoctorByID(Convert.ToInt32(login.ID.Text));//finds the doctor
            if (curr.needsToBeNotified)
            {
                MessageBox.Show("There are new test results in.");
                curr.needsToBeNotified = false;
                itsDoctor_BL.updateDoctor(curr);
            }
            currentDoctorsList.Add(curr);
            addDoctorsToTree(currentDoctorsList);

            //hides unneccesary features
            Admin.Visibility = System.Windows.Visibility.Collapsed;
            Doctor.Visibility = System.Windows.Visibility.Collapsed;
            cmbDoctor.Visibility = System.Windows.Visibility.Collapsed;
            cmbLabTechs.Visibility = System.Windows.Visibility.Collapsed;
            LabTech.Visibility = System.Windows.Visibility.Collapsed;
            agePieChart.Visibility = System.Windows.Visibility.Collapsed;
            genderPieChart.Visibility = System.Windows.Visibility.Collapsed;

            //creates the patients list
            currentPatientsList = itsDoctor_BL.getPatientsById(curr.iID);
            //creates the visits list
            List<Visit> visits = itsDoctor_BL.getVisits(curr.iID); 
            foreach (Visit v in visits)
            {
                if (v.AssignedDoctor == curr.iID) currentVisitsList.Add(v);
            }

            //creates the treatment lists
            List<Treatment> treatments = itsDoctor_BL.getTreatments(curr.iID);
            foreach (Treatment t in treatments)
            {
                if (t.createdByDoctor == curr.iID) currentTreatmentsList.Add(t);
            }

            List<LabTest> tests = itsDoctor_BL.getTests(curr.iID);
            foreach (LabTest t in tests)
            {
                if (t.AssignedDoctor == curr.iID) currentLabTestList.Add(t);
            }
        }
        #endregion

        #region reset lists
        //these functions reset the current lists according to the Database.
        private void resetDoctors(List<Doctor> listToReset)
        {
            listToReset.Clear();
            List<Doctor> doctors = itsDAL.allDoctors();
            foreach (Doctor doctorToAdd in doctors)
                listToReset.Add(doctorToAdd);
        }
        private void resetPatients(List<Patient> listToReset)
        {
            listToReset.Clear();
            List<Patient> patients = itsDAL.allPatients();
            foreach (Patient patientToAdd in patients)
                listToReset.Add(patientToAdd);
        }
        private void resetVisits(List<Visit> listToReset)
        {
            listToReset.Clear();
            List<Visit> visits = itsDAL.allVisits();

            foreach (Visit visitToAdd in visits)
                listToReset.Add(visitToAdd);
        }
        private void resetTreatments(List<Treatment> listToReset)
        {
            listToReset.Clear();
            List<Treatment> treatments = itsDAL.allTreatments();

            foreach (Treatment treatmentToAdd in treatments)
                listToReset.Add(treatmentToAdd);
        }

        private void resetLabTechs(List<LabTech> listToReset)
        {
            listToReset.Clear();
            List<LabTech> techs = itsDAL.allLabTechs();

            foreach (LabTech techToAdd in techs)
                listToReset.Add(techToAdd);
        }

        private void resetLabTests(List<LabTest> listToReset)
        {
            listToReset.Clear();
            List<LabTest> tests = itsDAL.allLabTests();

            foreach (LabTest testToAdd in tests)
                listToReset.Add(testToAdd);
        }
        #endregion

        #region menu panel
        private void Add_admin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //new get id window
                getIdFromUser prompt = new getIdFromUser("Admin");//gets an id for the Admin to add
                prompt.ShowDialog();
                //if it's an external user that is not already in the system
                if (itsSecurity.userExists(prompt.ID).Equals("wrong input")) itsSecurity.addUser(prompt.ID, "Admin");
                //else the user is already exists
                else itsSecurity.changeAutorization(prompt.ID, "Admin");
            }
            catch (Exception ex)
            {
                displayError(ex);
            }
        }

        private void Remove_admin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //new get id window
                getIdFromUser prompt = new getIdFromUser("Admin");//gets an id for the Admin to add
                prompt.ShowDialog();
                //checks if it's a doctor
                if (itsDoctor_BL.getDoctorByID(prompt.ID) != null)
                {
                    itsSecurity.changeAutorization(prompt.ID, "Doctor");
                    return;
                }
                else
                    //chceks if it's a patient
                    if (itsPatient_BL.getPatientByID(prompt.ID) != null)
                    {
                        itsSecurity.changeAutorization(prompt.ID, "Patient");
                        return;
                    }
                    //the user not exists
                    else itsSecurity.removeUser(prompt.ID, "Admin");

            }
            catch (Exception ex)
            {
                displayError(ex);
            }
        }

        private void Add_doctor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Doctor");//gets an id for the new doctor
                prompt.ShowDialog();
                if (itsDoctor_BL.getDoctorByID(prompt.ID) != null)//if the doctor already exists
                    throw new Exception("Doctor ID already exists in the Database.");
                if (prompt.ID == -1)//if no id was entered, do nothing
                    return;
                //else - add the doctor to the database and open the edit screen so he can edit his account
                Doctor newDoctor = new Doctor(prompt.ID, "", "", 0, 'm',false);
                EditDoctor editNewDoctor = new EditDoctor(newDoctor, itsPatient_BL, itsDoctor_BL);
                editNewDoctor.ShowDialog();
                if (!(newDoctor.sFirst_name == ""))//if the doctor is new and was not edited properly, dont add him
                {
                    itsDAL.addDoctor(newDoctor);
                    Securities newSecurity = new Securities(newDoctor.iID, newDoctor.iID, "Doctor");
                    itsDAL.addSecurity(newSecurity);//add the user to the security
                }
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }

        private void Edit_doctor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Doctor");//gets an id for the doctor to edit
                prompt.ShowDialog();
                if (itsDoctor_BL.getDoctorByID(prompt.ID) == null)//if the doctor doesn't exist
                    throw new Exception("Wrong ID.");
                //else - edit the doctor- open the edit screen so he can edit his account
                Doctor doctorToEdit = itsDoctor_BL.getDoctorByID(prompt.ID);
                EditDoctor editDoctor = new EditDoctor(doctorToEdit, itsPatient_BL, itsDoctor_BL);
                editDoctor.ShowDialog();
                itsDAL.updateDoctor(doctorToEdit);
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }


        private void Remove_doctor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Doctor");//gets an id for the doctor to remove
                prompt.ShowDialog();
                if (itsDoctor_BL.getDoctorByID(prompt.ID) == null)//if the doctor doesn't exist
                    throw new Exception("Wrong ID.");
                //else - remove the doctor
                Doctor doctorToRemove = itsDoctor_BL.getDoctorByID(prompt.ID);
                itsDoctor_BL.RemoveDoctorFromData(doctorToRemove, itsVisit_BL);
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }



        private void Add_patient_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Patient");//gets an id for the new patient
                prompt.ShowDialog();
                if (itsPatient_BL.getPatientByID(prompt.ID) != null)//if the patient already exists
                    throw new Exception("Patient ID already exists in the Database.");
                if (prompt.ID == -1)//if no id was entered, do nothing
                    return;

                //else - add the patient to the database and open the edit screen so he can edit his account
                Patient newPatient = new Patient(prompt.ID, "", "", 0, 'm', 0);
                EditPatient editNewPatient = new EditPatient(newPatient, itsDoctor_BL, itsVisit_BL);
                editNewPatient.ShowDialog();
                if (!(newPatient.sFirst_name == ""))//if the patient has just been added, and made up his mind mid-sign up, dont add him
                {
                    itsDAL.addPatient(newPatient);
                    Securities newSecurity = new Securities(newPatient.iID, newPatient.iID, "Patient");
                    itsDAL.addSecurity(newSecurity);
                }

            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }

        private void Edit_patient_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Patient");//gets an id for the patient to edit
                prompt.ShowDialog();
                if (itsPatient_BL.getPatientByID(prompt.ID) == null)//if the patient doesn't exist
                    throw new Exception("Wrong ID.");
                //else - edit the patient- open the edit screen so he can edit his account
                Patient patientToEdit = itsPatient_BL.getPatientByID(prompt.ID);
                EditPatient editPatient = new EditPatient(patientToEdit, itsDoctor_BL, itsVisit_BL);
                editPatient.ShowDialog();
                itsDAL.updatePatient(patientToEdit);


            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }


        private void Remove_patient_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Patient");//gets an id for the patient to remove
                prompt.ShowDialog();
                if (itsPatient_BL.getPatientByID(prompt.ID) == null)//if the patient doesn't exist
                    throw new Exception("Wrong ID.");
                //else - remove the patient
                Patient patientToRemove = itsPatient_BL.getPatientByID(prompt.ID);
                itsPatient_BL.RemovePatientFromData(patientToRemove, itsVisit_BL);
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }



        private void Add_visit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser promptPatient = new getIdFromUser("Patient's");//gets an id for the patient to add the visit to
                promptPatient.ShowDialog();
                if (itsPatient_BL.getPatientByID(promptPatient.ID) == null)//if the patient doesn't exist
                    throw new Exception("Wrong ID.");
                getIdFromUser promptDoctor = new getIdFromUser("Assigned Doctor's");//gets an id for the assigned doctor
                promptDoctor.ShowDialog();
                if (itsDoctor_BL.getDoctorByID(promptDoctor.ID) == null)//if the doctor doesn't exist
                    throw new Exception("Wrong ID.");
                //else: create the visit
                int visitId = BL_Backend.Visit.iGlobalVisitId;//get the current visit global id
                BL_Backend.Visit.iGlobalVisitId += 1;
                Visit newVisit = new Visit(visitId, new DateTime(),
                   itsDoctor_BL.getDoctorByID(promptDoctor.ID), promptPatient.ID, "", new List<int>());
                EditVisit editNewVisit = new EditVisit(newVisit, itsDoctor_BL, itsTreatment_BL, itsVisit_BL);
                editNewVisit.ShowDialog();
                if (newVisit.dateOfVisit.ToString() != (new DateTime()).ToString())//if the date hasn't been changed, meaning the sign up was not completed, then dont add the visit.
                    itsDAL.addVisit(newVisit);
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }

        private void Edit_Visit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Visit");//gets an id for the visit to edit
                prompt.ShowDialog();
                if (itsVisit_BL.getVisitByID(prompt.ID) == null)//if the visit doesn't exist
                    throw new Exception("Wrong Visit ID.");
                //else - edit the visit- open the edit screen so it can be edited
                Visit visitToEdit = itsVisit_BL.getVisitByID(prompt.ID);
                EditVisit editVisit = new EditVisit(visitToEdit, itsDoctor_BL, itsTreatment_BL, itsVisit_BL);
                editVisit.ShowDialog();
                itsDAL.updateVisit(visitToEdit);
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }

        private void Remove_Visit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Visit");//gets an id for the visit to remove
                prompt.ShowDialog();
                if (itsVisit_BL.getVisitByID(prompt.ID) == null)//if the visit doesn't exist
                    throw new Exception("Wrong Visit ID.");
                //else - remove the visit
                Visit visitToRemove = itsVisit_BL.getVisitByID(prompt.ID);
                itsVisit_BL.RemoveVisitFromData(visitToRemove, itsPatient_BL);
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }

        private void Add_treatment_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser promptPatient = new getIdFromUser("Patient's");//gets an id for the patient to add the treatment to
                promptPatient.ShowDialog();
                if (itsPatient_BL.getPatientByID(promptPatient.ID) == null)//if the patient doesn't exist
                    throw new Exception("Wrong ID.");

                getIdFromUser promptDoctor = new getIdFromUser("Creating Doctor's");//gets an id for the creating doctor
                promptDoctor.ShowDialog();
                if (itsDoctor_BL.getDoctorByID(promptDoctor.ID) == null)//if the doctor doesn't exist
                    throw new Exception("Wrong ID.");

                getIdFromUser promptVisit = new getIdFromUser("Visit's");//gets an id for the visit
                promptVisit.ShowDialog();
                if (itsVisit_BL.getVisitByID(promptVisit.ID) == null)//if the visit doesn't exist
                    throw new Exception("Wrong ID.");

                //else: create the treatment
                int treatmentId = BL_Backend.Treatment.iGlobalTreatmentId;//get the current treatment global id
                BL_Backend.Treatment.iGlobalTreatmentId += 1;
                Treatment newTreatment = new Treatment(treatmentId, new DateTime(), new DateTime(),
                   itsDoctor_BL.getDoctorByID(promptDoctor.ID), "", new List<string>(), "", promptPatient.ID, promptVisit.ID);
                EditTreatment editNewTreatment = new EditTreatment(newTreatment, itsTreatment_BL);
                editNewTreatment.ShowDialog();
                if ((newTreatment.DateOfFinish.ToString() != (new DateTime()).ToString())//if the date hasn't been changed, meaning the sign up was not completed, then dont add the visit.
                    || (newTreatment.DateOfStart.ToString() != (new DateTime()).ToString()))
                    itsDAL.addTreatment(newTreatment);
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }

        private void Edit_treatment_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Treatment");//gets an id for the treatment to edit
                prompt.ShowDialog();
                if (itsTreatment_BL.getTreatmentByID(prompt.ID) == null)//if the treatment doesn't exist
                    throw new Exception("Wrong Treatment ID.");
                //else - edit the treatment- open the edit screen so it can be edited
                Treatment treatmentToEdit = itsTreatment_BL.getTreatmentByID(prompt.ID);
                EditTreatment editTreatment = new EditTreatment(treatmentToEdit, itsTreatment_BL);
                editTreatment.ShowDialog();
                itsDAL.updateTreatment(treatmentToEdit);
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }


        private void Remove_treatment_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Treatment");//gets an id for the treatment to remove
                prompt.ShowDialog();
                if (itsTreatment_BL.getTreatmentByID(prompt.ID) == null)//if the treatment doesn't exist
                    throw new Exception("Wrong Treatment ID.");
                //else - remove the treatment
                Treatment treatmentToRemove = itsTreatment_BL.getTreatmentByID(prompt.ID);
                itsTreatment_BL.RemovetreatmentFromData(treatmentToRemove);
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }



        private void Add_Tech_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Technician");//gets an id for the new tech
                prompt.ShowDialog();
                if (itsLabTech_BL.getLabTechByID(prompt.ID) != null)//if the tech already exists
                    throw new Exception("Technician ID already exists in the Database.");
                if (prompt.ID == -1)//if no id was entered, do nothing
                    return;

                //else - add the technician to the database and open the edit screen so he can edit his account
                LabTech newTech = new LabTech(prompt.ID, "", "", 0, 'm');
                EditLabTech editNewTech = new EditLabTech(newTech);
                editNewTech.ShowDialog();
                if (!(newTech.sFirst_name == ""))//if the tech has just been added, and made up his mind mid-sign up, dont add him
                {
                    Securities newSecurity = new Securities(newTech.iID, newTech.iID, "Tech");
                    itsDAL.addSecurity(newSecurity);
                    itsDAL.addTech(newTech);
                }

            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }

        private void Edit_Tech_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Technician");//gets an id for the new tech
                prompt.ShowDialog();
                if (itsLabTech_BL.getLabTechByID(prompt.ID) == null)//if the tech doesnt exist
                    throw new Exception("Technician ID not found in Database.");
                if (prompt.ID == -1)//if no id was entered, do nothing
                    return;

                //else - add the technician to the database and open the edit screen so he can edit his account
                LabTech newTech = itsLabTech_BL.getLabTechByID(prompt.ID);
                EditLabTech editNewTech = new EditLabTech(newTech);
                editNewTech.ShowDialog();
                itsDAL.updateLabTech(newTech);
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }

        private void Remove_Tech_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Technician");//gets an id for the technician to remove
                prompt.ShowDialog();
                if (itsLabTech_BL.getLabTechByID(prompt.ID) == null)//if the technician doesn't exist
                    throw new Exception("Wrong ID.");
                //else - remove the tech
                LabTech techToRemove = itsLabTech_BL.getLabTechByID(prompt.ID);
                itsLabTech_BL.RemoveLabTechFromData(techToRemove);
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                displayError(ex);
            }
        }




        private void Add_Test_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser promptPatient = new getIdFromUser("Patient's");//gets an id for the patient to add the test to
                promptPatient.ShowDialog();
                if (itsPatient_BL.getPatientByID(promptPatient.ID) == null)//if the patient doesn't exist
                    throw new Exception("Wrong ID.");
                getIdFromUser promptDoctor = new getIdFromUser("Assigned Doctor's");//gets an id for the assigned doctor
                promptDoctor.ShowDialog();
                if (itsDoctor_BL.getDoctorByID(promptDoctor.ID) == null)//if the doctor doesn't exist
                    throw new Exception("Wrong ID.");
                //else: create the test
                int testId = BL_Backend.LabTest.iGlobalLabTestId;//get the current test global id
                BL_Backend.LabTest.iGlobalLabTestId += 1;
                LabTest newTest = new LabTest(testId, new DateTime(),
                   itsDoctor_BL.getDoctorByID(promptDoctor.ID), promptPatient.ID, "");
                EditLabTest editNewTest = new EditLabTest(newTest, itsDoctor_BL, itsLabTech_BL);
                editNewTest.ShowDialog();
                if (newTest.dateOfLabTest.ToString() != (new DateTime()).ToString())//if the date hasn't been changed, meaning the sign up was not completed, then dont add the test.
                {
                    itsDAL.addLabTest(newTest);
                   
                }
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }

        private void Edit_Test_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Test");//gets an id for the test to edit
                prompt.ShowDialog();
                if (itsLabTest_BL.getLabTestByID(prompt.ID) == null)//if the test doesn't exist
                    throw new Exception("Wrong Test ID.");
                //else - edit the test- open the edit screen so it can be edited
                LabTest testToEdit = itsLabTest_BL.getLabTestByID(prompt.ID);
                EditLabTest editLabTest = new EditLabTest(testToEdit, itsDoctor_BL, itsLabTech_BL);
                editLabTest.ShowDialog();
                itsDAL.updateLabTest(testToEdit);
            }
            catch (Exception ex)
            {
                displayError(ex);
            }
        }

        private void changePass_Click(object sender, RoutedEventArgs e)
        {
            Window1 changePassWin = new Window1(itsSecurity);
            changePassWin.ShowDialog();

        }


        private void ExporttoExcel_Click(object sender, EventArgs e)
        {
            String fileToSave = classChange.Text;

            switch (fileToSave)
            {
                case "Doctor":
                    itsDoctor_BL.ExporttoExcel_Doctors(currentDoctorsList);
                    break;
                case "Patient":
                    itsPatient_BL.ExporttoExcel_Patients(currentPatientsList);
                    break;
                case "Visit":
                    itsVisit_BL.ExporttoExcel_Visits(currentVisitsList);
                    break;
                case "Treatments":
                    itsTreatment_BL.ExporttoExcel_Treatments(currentTreatmentsList);
                    break;
                case "Lab techs":
                    itsLabTech_BL.ExporttoExcel_LabTech(currentLabTechList);
                    break;
                case "Lab tests":
                    itsLabTest_BL.ExporttoExcel_LabTest(currentLabTestList);
                    break;
                default:
                    break;
            }
      

            MessageBox.Show("File created !");
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            String fileToSave = classChange.Text;
            string dateFormat = " " + DateTime.Now.ToString("d MMM yyyy");

            switch (fileToSave)
            {
                case "Doctor":
                    itsDAL.exportXMLDoctors(itsDAL.allDoctors(), fileToSave + dateFormat);
                    break;
                case "Patient":
                    itsDAL.exportXMLPatients(itsDAL.allPatients(), fileToSave + dateFormat);
                    break;
                case "Visit":
                    itsDAL.exportXMLVisits(itsDAL.allVisits(), fileToSave + dateFormat);
                    break;
                case "Treatment":
                    itsDAL.exportXMLTreatments(itsDAL.allTreatments(), fileToSave + dateFormat);
                    break;
                default:
                    break;
            }
        }
        private void ExporttoPDF_Click(object sender, RoutedEventArgs e)
        {
            String fileToSave = classChange.Text;
            if (userType.Equals("Patient"))
            {
                ExportToPdf.ExportToPDFall(currentPatientsList, currentLabTestList, currentTreatmentsList, currentVisitsList);
            }
            else
            {
                switch (fileToSave)
                {
                    case "Doctor":
                        ExportToPdf.ExportToPDFDoctors(currentDoctorsList);
                        break;
                    case "Patient":
                        ExportToPdf.ExportToPDFPatients(currentPatientsList);
                        break;
                    case "Visit":
                        ExportToPdf.ExportToPDFVisits(currentVisitsList);
                        break;
                    case "Treatments":
                        ExportToPdf.ExportToPDFTreatments(currentTreatmentsList);
                        break;
                    case "Lab techs":
                        ExportToPdf.ExportToPDFLabTech(currentLabTechList);
                        break;
                    case "Lab tests":
                        ExportToPdf.ExportToPDFLabTest(currentLabTestList);
                        break;
                    default:

                        break;
                }

            }
            

            MessageBox.Show("File created !");

        }

        private void agePieChart_Click(object sender, RoutedEventArgs e)
        {
            PieChart chart = new PieChart(currentPatientsList, "Age");
            chart.ShowDialog();
        }

        private void genderPieChart_Click(object sender, RoutedEventArgs e)
        {
            PieChart chart = new PieChart(currentPatientsList, "Gender");
            chart.ShowDialog();

        }

        private void barChart_Click(object sender, RoutedEventArgs e)
        {
            getIdFromUser prompt = new getIdFromUser("Doctor");//gets an id for the doctor to plot
            prompt.ShowDialog();
            if (itsDoctor_BL.getDoctorByID(prompt.ID) == null)//if the doctush doesn't exist
                throw new Exception("Wrong Doctor ID.");
                                                                                                                                                                                                                                                                                                                                                            List<Visit> visits = itsDAL.toVisits(itsDAL.getListOfVisitsbyDocId(prompt.ID));

            BarChart barChart = new BarChart(visits);
            barChart.ShowDialog();
        }

        #endregion

        #region main combo box

        //once the selection has been changed in the class combobox, reset all lists according to the Database.
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //initializes the lists
            String s = itsSecurity.theUserType(login.ID.Text, Convert.ToInt32(login.password.Password));
            switch (s)
            {
                case "Admin":
                    resetDoctors(currentDoctorsList);
                    resetPatients(currentPatientsList);
                    resetVisits(currentVisitsList);
                    resetTreatments(currentTreatmentsList);
                    resetLabTechs(currentLabTechList);
                    resetLabTests(currentLabTestList);
                    Input_Queries.Text = "";
                    TreeView.Items.Clear();
                    break;
                case "Doctor":
                    Input_Queries.Text = "";
                    TreeView.Items.Clear();
                    initializeByDoctor();
                    break;
                //there is no need to check the patient because he doesn't have this combobox

            }

        }

        //Doctors selected
        private void ComboBoxItem_Selected(object sender, RoutedEventArgs e)
        {
            //takes all the query menu string that can be searched for
            //resetDoctors(currentDoctorsList);
            cmbQueries.Items.Clear();
            cmbQueries.Items.Add("All doctors");
            cmbQueries.Items.Add("First name");
            cmbQueries.Items.Add("Last name");
            cmbQueries.Items.Add("ID");
            cmbQueries.Items.Add("Gender");
            cmbQueries.Items.Add("Salary");
            cmbQueries.Items.Add("Patients list");

            //hides the search bars
            cmbQueries.Visibility = System.Windows.Visibility.Visible;
            cmbGender.Visibility = System.Windows.Visibility.Hidden;
            btnSearch.Visibility = System.Windows.Visibility.Hidden;
            Input_Queries.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMin.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMax.Visibility = System.Windows.Visibility.Hidden;
            Salary_Label.Visibility = System.Windows.Visibility.Hidden;
            graph.Visibility = System.Windows.Visibility.Hidden;
            lblSearch.Content = "";
        }

        private void cmbPatient_Selected(object sender, RoutedEventArgs e)
        {
            //takes all the query menu string that can be searched for
            cmbQueries.Items.Clear();
            cmbQueries.Items.Add("All patients");
            cmbQueries.Items.Add("First name");
            cmbQueries.Items.Add("Last name");
            cmbQueries.Items.Add("ID");
            cmbQueries.Items.Add("Gender");//added
            cmbQueries.Items.Add("visits made");
            cmbQueries.Items.Add("treatments made");

            //hides the search bars
            cmbQueries.Visibility = System.Windows.Visibility.Visible;
            cmbGender.Visibility = System.Windows.Visibility.Hidden;
            btnSearch.Visibility = System.Windows.Visibility.Hidden;
            Input_Queries.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMin.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMax.Visibility = System.Windows.Visibility.Hidden;
            Salary_Label.Visibility = System.Windows.Visibility.Hidden;
            graph.Visibility = System.Windows.Visibility.Visible;

            lblSearch.Content = "";
        }

        private void cmbVisit_Selected(object sender, RoutedEventArgs e)
        {
            //takes all the query menu string that can be searched for
            cmbQueries.Items.Clear();
            cmbQueries.Items.Add("All visits");
            cmbQueries.Items.Add("Date");
            cmbQueries.Items.Add("ID");

            //hides the search bars
            cmbQueries.Visibility = System.Windows.Visibility.Visible;
            cmbGender.Visibility = System.Windows.Visibility.Hidden;
            btnSearch.Visibility = System.Windows.Visibility.Hidden;
            Input_Queries.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMin.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMax.Visibility = System.Windows.Visibility.Hidden;
            Salary_Label.Visibility = System.Windows.Visibility.Hidden;
            graph.Visibility = System.Windows.Visibility.Hidden;

            lblSearch.Content = "";
        }

        private void cmbTreatments_Selected(object sender, RoutedEventArgs e)
        {
            //takes all the query menu string that can be searched for
            cmbQueries.Items.Clear();
            cmbQueries.Items.Add("All treatments");
            cmbQueries.Items.Add("Date of start");
            cmbQueries.Items.Add("Date of finish");
            cmbQueries.Items.Add("doctor in charge");
            cmbQueries.Items.Add("ID");

            //hides the search bars
            cmbQueries.Visibility = System.Windows.Visibility.Visible;
            cmbGender.Visibility = System.Windows.Visibility.Hidden;
            btnSearch.Visibility = System.Windows.Visibility.Hidden;
            Input_Queries.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMin.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMax.Visibility = System.Windows.Visibility.Hidden;
            Salary_Label.Visibility = System.Windows.Visibility.Hidden;
            graph.Visibility = System.Windows.Visibility.Hidden;

            lblSearch.Content = "";
        }

        private void cmbLabTests_Selected(object sender, RoutedEventArgs e)
        {
            //takes all the query menu string that can be searched for
            cmbQueries.Items.Clear();
            cmbQueries.Items.Add("All Lab tests");
            cmbQueries.Items.Add("Date of start");
            cmbQueries.Items.Add("Date of finish");
            cmbQueries.Items.Add("doctor in charge");
            cmbQueries.Items.Add("Patients ID");


            //hides the search bars
            cmbQueries.Visibility = System.Windows.Visibility.Visible;
            cmbGender.Visibility = System.Windows.Visibility.Hidden;
            btnSearch.Visibility = System.Windows.Visibility.Visible;
            Input_Queries.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMin.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMax.Visibility = System.Windows.Visibility.Hidden;
            Salary_Label.Visibility = System.Windows.Visibility.Hidden;
            graph.Visibility = System.Windows.Visibility.Hidden;



        }


        #endregion

        #region secondary combobox
        private void cmbQueries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //empties the tree and clears the dialog
            if (btnSearch.Visibility == System.Windows.Visibility.Hidden)
            {//if we had just shown a doctor's patient list
                TreeView.Items.Clear();//then we need to remove the patients displayed in the tree
                resetDoctors(currentDoctorsList);
            }
            Input_Queries.Text = "";

            //makes the search box and button visible
            if (!(((String)cmbQueries.SelectedItem) == "All doctors") && !(((String)cmbQueries.SelectedItem) == "All patients") &&
                !(((String)cmbQueries.SelectedItem) == "All visits") && !(((String)cmbQueries.SelectedItem) == "All treatments")
                && !(((String)cmbQueries.SelectedItem) == "All Lab tests") && !(((String)cmbQueries.SelectedItem) == "All Lab techs"))// if a query needs to be made
                Input_Queries.Visibility = System.Windows.Visibility.Visible;

            else//if "all" was selected
                Input_Queries.Visibility = System.Windows.Visibility.Hidden;

            btnSearch.Visibility = System.Windows.Visibility.Visible;//the button needs to be visible anyway
            cmbGender.Visibility = System.Windows.Visibility.Hidden;//the extra options will be hidden at first
            Salary_queryMin.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMax.Visibility = System.Windows.Visibility.Hidden;
            Salary_Label.Visibility = System.Windows.Visibility.Hidden;
            //adds the text to the label
            String choice = (String)cmbQueries.SelectedItem;
            switch (choice)
            {
                //doctors cases
                case "All doctors":
                    lblSearch.Content = "";
                    break;
                case "First name":
                    lblSearch.Content = "Please enter First Name:";
                    break;
                case "Last name":
                    lblSearch.Content = "Please enter Last Name:";
                    break;
                case "ID":
                    lblSearch.Content = "Please enter the ID:";
                    break;
                case "Gender":
                    //creates a combobox with 2 options
                    lblSearch.Content = "Please select gender:";
                    cmbGender.Visibility = System.Windows.Visibility.Visible;
                    Input_Queries.Visibility = System.Windows.Visibility.Hidden;
                    break;
                case "Salary":
                    //creates 2 new text boxes  
                    lblSearch.Content = "Please enter the salary range:";
                    Salary_queryMin.Visibility = System.Windows.Visibility.Visible;
                    Salary_queryMax.Visibility = System.Windows.Visibility.Visible;
                    Salary_Label.Visibility = System.Windows.Visibility.Visible;
                    Input_Queries.Visibility = System.Windows.Visibility.Hidden;
                    break;

                case "Patients list":
                    lblSearch.Content = "Please enter the doctor's ID:";
                    break;
                //additional patients cases
                case "visits made":
                    lblSearch.Content = "Please enter the patient's ID:";
                    break;
                case "treatments made":
                    lblSearch.Content = "Please enter the patient's ID:";
                    break;
                //visits cases
                case "Date":
                    lblSearch.Content = "Please enter the Date of start mm/dd/yy:";
                    break;
                //treatments cases
                case "Date of start":
                    lblSearch.Content = "Please enter the Date of start mm/dd/yy:";
                    break;
                case "Date of finish":
                    lblSearch.Content = "Please enter the Date of finish mm/dd/yy:";
                    break;
                case "doctor in charge":
                    goto case "ID";


                //additional lab test
                case "All Lab tests":
                    lblSearch.Content = "";
                    break;
                case "Patients ID":
                    lblSearch.Content = "Please enter patient's id";
                    break;

                case "All Lab techs":
                    lblSearch.Content = "";
                    break;

                default:
                    lblSearch.Content = "";
                    break;
            }

        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            //empties the tree
            TreeView.Items.Clear();
            String choice = classChange.Text;
            switch (choice)
            {
                case "Doctor":
                    {
                        bool patientListSelected = false;
                        String queryChoice = cmbQueries.Text;
                        try
                        {
                            switch (queryChoice)
                            {
                                case "All doctors":
                                    //resetDoctors(currentDoctorsList);
                                    lblSearch.Content = "";
                                    break;
                                case "First name":
                                    currentDoctorsList = itsQueries.sortByFirstNameDoc(currentDoctorsList, Input_Queries.Text);
                                    break;
                                case "Last name":
                                    currentDoctorsList = itsQueries.sortByLastNameDoc(currentDoctorsList, Input_Queries.Text);
                                    break;
                                case "ID":
                                    int Id = int.Parse(Input_Queries.Text);
                                    currentDoctorsList = itsQueries.sortByIDDoc(currentDoctorsList, Id);
                                    break;

                                case "Gender":
                                    //takes the first char in doctor
                                    Char StringConvertedToChar = ((String)cmbGender.Text)[0];
                                    currentDoctorsList = itsQueries.SortByGenderDoc(currentDoctorsList, StringConvertedToChar);
                                    break;
                                case "Salary":
                                    currentDoctorsList = itsQueries.SortBySalary(currentDoctorsList, int.Parse(Salary_queryMin.Text), int.Parse(Salary_queryMax.Text));
                                    break;
                                case "Patients list":
                                    Input_Queries.Visibility = System.Windows.Visibility.Hidden;
                                    btnSearch.Visibility = System.Windows.Visibility.Hidden;
                                    lblSearch.Content = "";
                                    int doctorId = int.Parse(Input_Queries.Text);
                                    Doctor doc = itsDoctor_BL.getDoctorByID(doctorId);
                                    //if (!currentDoctorsList.Contains(doc))
                                    //{
                                    //    currentDoctorsList.Clear();
                                    //    break;
                                    //}
                                    currentPatientsList = itsDoctor_BL.getPatientsById(doc.iID);
                                    TreeView.Items.Clear();
                                    if (currentPatientsList.Count == 0)
                                    {
                                        throw new Exception("No matching Patients were found in the doctor's patient list.");
                                    }
                                    addPatientsToTree(currentPatientsList);
                                    currentDoctorsList.Clear();
                                    patientListSelected = true;
                                    break;


                                default:
                                    break;
                            }
                            if (currentDoctorsList.Count == 0 && !patientListSelected)
                            {
                                resetDoctors(currentDoctorsList);
                                throw new Exception("No matching Doctors were found");
                            }
                            addDoctorsToTree(currentDoctorsList);
                            patientListSelected = false;
                        }
                        catch (Exception ex)
                        {
                            displayError(ex);//calls the method that should pop a dialog box and shout at the user
                        }
                    }
                    break;

                case "Patient":
                    {
                        int patientId;//these help identify the patient to make actions on in case of the last 2 choices.
                        Patient patient;
                        bool visitListSelected = false;
                        bool treatmentListSelected = false;
                        String queryChoice = cmbQueries.Text;
                        try
                        {
                            switch (queryChoice)
                            {
                                case "All patients":
                                    //resetPatients(currentPatientsList);
                                    lblSearch.Content = "";
                                    break;
                                case "First name":
                                    currentPatientsList = itsQueries.sortByFirstNameP(currentPatientsList, Input_Queries.Text);
                                    break;
                                case "Last name":
                                    currentPatientsList = itsQueries.sortByLastNameP(currentPatientsList, Input_Queries.Text);
                                    break;
                                case "ID":
                                    int Id = int.Parse(Input_Queries.Text);
                                    currentPatientsList = itsQueries.sortByIDP(currentPatientsList, Id);
                                    break;
                                case "Gender":
                                    //takes the first char in the combobox
                                    Char StringConvertedToChar = ((String)cmbGender.Text)[0];
                                    currentPatientsList = itsQueries.SortByGenderP(currentPatientsList, StringConvertedToChar);
                                    break;
                                case "visits made":
                                    Input_Queries.Visibility = System.Windows.Visibility.Hidden;
                                    btnSearch.Visibility = System.Windows.Visibility.Hidden;
                                    lblSearch.Content = "";
                                    patientId = int.Parse(Input_Queries.Text);//throw exception for bad input
                                    patient = itsPatient_BL.getPatientByID(patientId);
                                    //if (!currentPatientsList.Contains(patient))
                                    //{
                                    //    currentPatientsList.Clear();
                                    //    break;
                                    //}
                                    currentVisitsList = itsPatient_BL.getVisitssByList(currentVisitsList, patient.getVisits());
                                    TreeView.Items.Clear();
                                    if (currentVisitsList.Count == 0)
                                    {
                                        throw new Exception("No matching Visits were found in the Patients's visit list.");
                                    }
                                    addVisitsToTree(currentVisitsList);
                                    currentPatientsList.Clear();
                                    visitListSelected = true;
                                    break;

                                case "treatments made":
                                    Input_Queries.Visibility = System.Windows.Visibility.Hidden;
                                    btnSearch.Visibility = System.Windows.Visibility.Hidden;
                                    lblSearch.Content = "";
                                    patientId = int.Parse(Input_Queries.Text);//throw exception for bad input
                                    patient = itsPatient_BL.getPatientByID(patientId);
                                    if (!currentPatientsList.Contains(patient))
                                    {
                                        currentPatientsList.Clear();
                                        break;
                                    }
                                    currentTreatmentsList = itsPatient_BL.getTreatmentsByList(currentTreatmentsList, patient.getTreatments());
                                    TreeView.Items.Clear();
                                    if (currentTreatmentsList.Count == 0)
                                    {
                                        throw new Exception("No matching Treatments were found in the Patients's treatment list.");
                                    }
                                    addTreatmentsToTree(currentTreatmentsList);
                                    currentPatientsList.Clear();
                                    treatmentListSelected = true;
                                    break;
                                default:
                                    break;
                            }
                            if (currentPatientsList.Count == 0 && !visitListSelected && !treatmentListSelected)
                            {
                                resetPatients(currentPatientsList);
                                throw new Exception("No matching patients were found");
                            }

                            addPatientsToTree(currentPatientsList);
                            visitListSelected = false;
                            treatmentListSelected = false;

                        }
                        catch (Exception ex)
                        {
                            displayError(ex);
                        }

                    }

                    break;


                case "Visit":
                    {
                        String queryChoice = cmbQueries.Text;
                        try
                        {
                            switch (queryChoice)
                            {
                                case "All visits":
                                    //resetVisits(currentVisitsList);
                                    lblSearch.Content = "";
                                    break;

                                case "Date":
                                    DateTime dateToCheck = DateTime.Parse(Input_Queries.Text,
                                    System.Globalization.CultureInfo.InvariantCulture);
                                    currentVisitsList = itsQueries.SortVisitByDate(currentVisitsList, dateToCheck);
                                    break;
                                case "ID":
                                    int Id = int.Parse(Input_Queries.Text);
                                    currentVisitsList = itsQueries.SortVisitById(currentVisitsList, Id);
                                    break;
                                default:
                                    break;
                            }
                            if (currentVisitsList.Count == 0)
                            {
                                resetVisits(currentVisitsList);
                                throw new Exception("No matching Visits were found");
                            }

                            addVisitsToTree(currentVisitsList);
                        }
                        catch (Exception ex)
                        {
                            displayError(ex);
                        }

                    }
                    break;

                case "Treatments":
                    {
                        String queryChoice = cmbQueries.Text;
                        try
                        {
                            switch (queryChoice)
                            {
                                case "All treatments":
                                    //resetTreatments(currentTreatmentsList);
                                    lblSearch.Content = "";
                                    break;

                                case "Date of start":
                                    DateTime dateOfStart = DateTime.Parse(Input_Queries.Text,
                                    System.Globalization.CultureInfo.InvariantCulture);
                                    currentTreatmentsList = itsQueries.SortTreatmentsByDate(currentTreatmentsList, dateOfStart);
                                    break;
                                case "Date of finish":
                                    DateTime dateOfFinish = DateTime.Parse(Input_Queries.Text,
                                    System.Globalization.CultureInfo.InvariantCulture);
                                    currentTreatmentsList = itsQueries.SortTreatmentsByDate(currentTreatmentsList, dateOfFinish);
                                    break;
                                case "doctor in charge":
                                    break;
                                case "ID":
                                    int Id = int.Parse(Input_Queries.Text);
                                    currentTreatmentsList = itsQueries.SortTreatmentsById(currentTreatmentsList, Id);
                                    break;
                                default:
                                    break;
                            }

                            if (currentTreatmentsList.Count == 0)
                            {
                                resetTreatments(currentTreatmentsList);
                                throw new Exception("No matching Treatments were found");
                            }

                            addTreatmentsToTree(currentTreatmentsList);
                        }
                        catch (Exception ex)
                        {
                            displayError(ex);
                        }

                    }
                    break;
                case "Lab tests":
                    {
                        String queryChoice = cmbQueries.Text;
                        try
                        {
                            switch (queryChoice)
                            {
                                case "All Lab tests":
                                    //resetTreatments(currentTreatmentsList);
                                    lblSearch.Content = "";
                                    break;

                                case "Date of start":
                                    DateTime dateOfStart = DateTime.Parse(Input_Queries.Text,
                                    System.Globalization.CultureInfo.InvariantCulture);
                                    currentLabTestList = itsQueries.SortLabTestsByDate(currentLabTestList, dateOfStart);
                                    break;
                                case "Date of finish":
                                    DateTime dateOfFinish = DateTime.Parse(Input_Queries.Text,
                                    System.Globalization.CultureInfo.InvariantCulture);
                                    currentLabTestList = itsQueries.SortLabTestsByDate(currentLabTestList, dateOfFinish);
                                    break;
                                case "doctor in charge":
                                case "Patients ID":
                                    int Id = int.Parse(Input_Queries.Text);
                                    currentLabTestList = itsLabTest_BL.getLabTestByPatID(currentLabTestList,Id);
                                    break;
                                default:
                                    break;
                            }

                            if (currentLabTestList.Count == 0)
                            {
                                resetLabTests(currentLabTestList);
                                throw new Exception("No matching Lab Tests were found");
                            }

                            addLabTestToTree(currentLabTestList);
                        }
                        catch (Exception ex)
                        {
                            displayError(ex);
                        }
                    }
                    break;
                case "Lab techs":
                    {
                        String queryChoice = cmbQueries.Text;
                        try
                        {
                            switch (queryChoice)
                            {
                                case "All Lab techs":
                                    lblSearch.Content = "";
                                    break;
                                case "First name":
                                    currentLabTechList = itsQueries.sortByFirstNameTech(currentLabTechList, Input_Queries.Text);
                                    break;
                                case "Last name":
                                    currentLabTechList = itsQueries.sortByLastNameTech(currentLabTechList, Input_Queries.Text);
                                    break;
                                case "ID":
                                    int Id = int.Parse(Input_Queries.Text);
                                    currentLabTechList = itsQueries.sortByIDTech(currentLabTechList, Id);
                                    break;

                                case "Gender":
                                    //takes the first char in doctor
                                    Char StringConvertedToChar = ((String)cmbGender.Text)[0];
                                    currentLabTechList = itsQueries.sortByGenderTech(currentLabTechList, StringConvertedToChar);
                                    break;
                                case "Salary":
                                    currentLabTechList = itsQueries.SortBySalaryTech(currentLabTechList, int.Parse(Salary_queryMin.Text), int.Parse(Salary_queryMax.Text));
                                    break;
                                default:
                                    break;
                            }
                            if (currentLabTechList.Count == 0)
                            {
                                resetLabTechs(currentLabTechList);
                                throw new Exception("No matching Lab Techs were found");
                            }

                            addLabTechToTree(currentLabTechList);
                        }
                        catch (Exception ex)
                        {
                            displayError(ex);
                        }
                    }
                    break;
                    

                default:
                    break;
            }
        }



        //opens the errorDialog window
        public void displayError(Exception ex)
        {
            errorDialog error = new errorDialog(ex);
            error.ShowDialog();
        }
        private void cmbLabTechs_Selected(object sender, RoutedEventArgs e)
        {
            //takes all the query menu string that can be searched for
            cmbQueries.Items.Clear();
            cmbQueries.Items.Add("All Lab techs");
            cmbQueries.Items.Add("First name");
            cmbQueries.Items.Add("Last name");
            cmbQueries.Items.Add("ID");
            cmbQueries.Items.Add("Gender");
            cmbQueries.Items.Add("Salary");

            //hides the search bars
            cmbQueries.Visibility = System.Windows.Visibility.Visible;
            cmbGender.Visibility = System.Windows.Visibility.Hidden;
            btnSearch.Visibility = System.Windows.Visibility.Hidden;
            Input_Queries.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMin.Visibility = System.Windows.Visibility.Hidden;
            Salary_queryMax.Visibility = System.Windows.Visibility.Hidden;
            Salary_Label.Visibility = System.Windows.Visibility.Hidden;
            lblSearch.Content = "";


        }


        #endregion

        #region add to tree functions


        //awdawdjhfklrgbodrhglduhgdrlighdlirughdilrghdrulghdrdulghdukrhgurfilegfliaesgfaoiwt4foatwefoet7fbiso7ft4tfbistfbtb487tfbirsfgbte4k7tfbielgt4beilgteigtbdg
        private void addDoctorsToTree(List<Doctor> dctToAdd)
        {
            foreach (Doctor d in dctToAdd)
            {
                //adds the id as main
                TreeViewItem trvID = new TreeViewItem();
                trvID.Header = "ID: " + d.iID;
                TreeView.Items.Add(trvID);
                //adds all the other field into the id

                //first name
                TreeViewItem trvFirstName = new TreeViewItem();
                trvFirstName.Header = "First Name: " + d.sFirst_name;
                trvID.Items.Add(trvFirstName);

                //last name
                TreeViewItem trvLastName = new TreeViewItem();
                trvLastName.Header = "Last Name: " + d.sLast_Name;
                trvID.Items.Add(trvLastName);

                //salary
                TreeViewItem trvSalary = new TreeViewItem();
                trvSalary.Header = "Salary: " + d.dSalary;
                trvID.Items.Add(trvSalary);

                //Gender
                TreeViewItem trvGender = new TreeViewItem();
                trvGender.Header = "Gender: " + d.cGender;
                trvID.Items.Add(trvGender);

                //patients list
                TreeViewItem trvPatients = new TreeViewItem();
                trvPatients.Header = "Patients ";
                trvID.Items.Add(trvPatients);
                //adds the patients into the patient section
                foreach (int i in itsDoctor_BL.getPatients(d.iID))
                {
                    TreeViewItem trvPatient = new TreeViewItem();
                    trvPatient.Header = i;
                    trvPatients.Items.Add(trvPatient);
                }
            }
        }
        private void addPatientsToTree(List<Patient> ptnToAdd)
        {
            foreach (Patient p in ptnToAdd)
            {
                //adds the id as main
                TreeViewItem trvID = new TreeViewItem();
                trvID.Header = "ID: " + p.iID;
                TreeView.Items.Add(trvID);
                //adds all the other field into the id

                //first name
                TreeViewItem trvFirstName = new TreeViewItem();
                trvFirstName.Header = "First Name: " + p.sFirst_name;
                trvID.Items.Add(trvFirstName);

                //last name
                TreeViewItem trvLastName = new TreeViewItem();
                trvLastName.Header = "Last Name: " + p.sLast_Name;
                trvID.Items.Add(trvLastName);

                //Age
                TreeViewItem trvAge = new TreeViewItem();
                trvAge.Header = "Age: " + p.iAge;
                trvID.Items.Add(trvAge);

                //Gender
                TreeViewItem trvGender = new TreeViewItem();
                trvGender.Header = "Gender: " + p.cGender;
                trvID.Items.Add(trvGender);

                //main doctor
                TreeViewItem trvDoc = new TreeViewItem();
                trvDoc.Header = "Main Doctor: " + p.MainDoctor;
                trvID.Items.Add(trvDoc);

                //Visits list
                TreeViewItem trvVisits = new TreeViewItem();
                trvVisits.Header = "Visits ";
                trvID.Items.Add(trvVisits);

                //adds the patients into the patient section
                foreach (int i in itsPatient_BL.getVisits(p.iID))
                {
                    Visit tmp = itsVisit_BL.getVisitByID(i);
                    addVisitsToComponent(trvVisits, tmp);
                }
                //Treatment list
                TreeViewItem trvTreatments = new TreeViewItem();
                trvTreatments.Header = "Treatments:";
                trvID.Items.Add(trvTreatments);
                //adds the treatments into the patient section
                foreach (int i in itsPatient_BL.getTreatments(p.iID))
                {
                    Treatment tmp = itsTreatment_BL.getTreatmentByID(i);
                    addTreatmentsToComponent(trvTreatments, tmp);
                }
                TreeViewItem trvTests = new TreeViewItem();
                trvTests.Header = "Tests:";
                trvID.Items.Add(trvTests);
                //adds the treatments into the patient section
                foreach (LabTest i in itsPatient_BL.getLabTest(p.iID))
                {
                    if (i.iPatientId == p.iID) addLabTestsToComponent(trvTests, i);
                }
            }
        }

        private void addLabTestsToComponent(TreeViewItem trvTests, LabTest t)
        {
            //adds the id as main
            TreeViewItem trvID = new TreeViewItem();
            trvID.Header = "ID: " + t.iLabTestID;
            trvTests.Items.Add(trvID);

            //patient id
            TreeViewItem patientID = new TreeViewItem();
            patientID.Header = "Patients ID: " + t.iPatientId;
            trvID.Items.Add(patientID);

            //date of start
            TreeViewItem dateOfstart = new TreeViewItem();
            dateOfstart.Header = "Date of start: " + t.dateOfLabTest;
            trvID.Items.Add(dateOfstart);

            //assinged doctor
            TreeViewItem assingedDoc = new TreeViewItem();
            assingedDoc.Header = "Assinged doctor: " + t.AssignedDoctor;
            trvID.Items.Add(assingedDoc);

            //prognosis
            TreeViewItem results = new TreeViewItem();
            results.Header = "Results: " + t.sResults;
            trvID.Items.Add(results);

            //prognosis
            TreeViewItem prognosis = new TreeViewItem();
            prognosis.Header = "Prognosis: " + t.sDoctorNotes;
            trvID.Items.Add(prognosis);
        }

        private void addVisitsToComponent(TreeViewItem trvVisits, Visit v)
        {
            //ramaaut shel gvarim havaaalazmannnnn
            //adds the id as main
            TreeViewItem trvID = new TreeViewItem();
            trvID.Header = "ID: " + v.iVisitID;
            trvVisits.Items.Add(trvID);

            //adds all the other field into the id
            //patient id
            TreeViewItem patientID = new TreeViewItem();
            patientID.Header = "Patients ID: " + v.iPatientId;
            trvID.Items.Add(patientID);

            //date of visit
            TreeViewItem dateOfVisit = new TreeViewItem();
            dateOfVisit.Header = "Date of visit: " + v.dateOfVisit;
            trvID.Items.Add(dateOfVisit);

            //assinged doctor
            TreeViewItem assingedDoc = new TreeViewItem();
            assingedDoc.Header = "Assinged doctor: " + v.AssignedDoctor;
            trvID.Items.Add(assingedDoc);

            //doctor notes
            TreeViewItem doctorNotes = new TreeViewItem();
            doctorNotes.Header = "Notes: " + v.sDoctorNotes;
            trvID.Items.Add(doctorNotes);

            //all the treatments
            TreeViewItem treatments = new TreeViewItem();
            treatments.Header = "Treatments made";
            trvID.Items.Add(treatments);

            //adds the treatments into the visit section
            foreach (int i in itsVisit_BL.getTreatments(v.iVisitID))
            {
                TreeViewItem trvVisit = new TreeViewItem();
                trvVisit.Header = i;
                treatments.Items.Add(trvVisit);
            }
        }

        private void addVisitsToTree(List<Visit> vstToAdd)
        {
            foreach (Visit v in vstToAdd)
            {
                //adds the id as main
                TreeViewItem trvID = new TreeViewItem();
                trvID.Header = "ID: " + v.iVisitID;
                TreeView.Items.Add(trvID);

                //adds all the other field into the id
                //patient id
                TreeViewItem patientID = new TreeViewItem();
                patientID.Header = "Patients ID: " + v.iPatientId;
                trvID.Items.Add(patientID);

                //date of visit
                TreeViewItem dateOfVisit = new TreeViewItem();
                dateOfVisit.Header = "Date of visit: " + v.dateOfVisit;
                trvID.Items.Add(dateOfVisit);

                //assinged doctor
                TreeViewItem assingedDoc = new TreeViewItem();
                assingedDoc.Header = "Assinged doctor: " + v.AssignedDoctor;
                trvID.Items.Add(assingedDoc);

                //doctor notes
                TreeViewItem doctorNotes = new TreeViewItem();
                doctorNotes.Header = "Notes: " + v.sDoctorNotes;
                trvID.Items.Add(doctorNotes);

                //all the treatments
                TreeViewItem treatments = new TreeViewItem();
                treatments.Header = "Treatments made";
                trvID.Items.Add(treatments);

                //adds the treatments into the visit section
                foreach (int i in itsVisit_BL.getTreatments(v.iVisitID))
                {
                    TreeViewItem trvVisit = new TreeViewItem();
                    trvVisit.Header = i;
                    treatments.Items.Add(trvVisit);
                }
            }
        }

        private void addTreatmentsToTree(List<Treatment> trmToAdd)
        {
            foreach (Treatment t in trmToAdd)
            {
                //adds the id as main
                TreeViewItem trvID = new TreeViewItem();
                trvID.Header = "ID: " + t.iTreatmentID;
                TreeView.Items.Add(trvID);

                //adds all the other field into the id
                //Treatments name
                TreeViewItem name = new TreeViewItem();
                name.Header = "Treatments name: " + t.sTreatmentName;
                trvID.Items.Add(name);

                //patient id
                TreeViewItem patientID = new TreeViewItem();
                patientID.Header = "Patients ID: " + t.iPatientID;
                trvID.Items.Add(patientID);

                //date of start
                TreeViewItem dateOfstart = new TreeViewItem();
                dateOfstart.Header = "Date of start: " + t.DateOfStart;
                trvID.Items.Add(dateOfstart);


                //date of finish
                TreeViewItem dateOffinish = new TreeViewItem();
                dateOffinish.Header = "Date of start: " + t.DateOfFinish;
                trvID.Items.Add(dateOffinish);

                //assinged doctor
                TreeViewItem assingedDoc = new TreeViewItem();
                assingedDoc.Header = "Assinged doctor: " + t.createdByDoctor;
                trvID.Items.Add(assingedDoc);

                //prognosis
                TreeViewItem prognosis = new TreeViewItem();
                prognosis.Header = "Prognosis: " + t.sPrognosis;
                trvID.Items.Add(prognosis);

                //prognosis of the technician


                //all the prescriptions
                TreeViewItem prescriptions = new TreeViewItem();
                prescriptions.Header = "Prescriptionss given";
                trvID.Items.Add(prescriptions);

                //adds the prescription into the prescription section
                foreach (string s in t.sPrescriptions)
                {
                    TreeViewItem pres = new TreeViewItem();
                    pres.Header = s;
                    prescriptions.Items.Add(pres);
                }
            }
        }
        private void addTreatmentsToComponent(TreeViewItem trvToAdd, Treatment t)
        {
            //ramaaut shel gvarim havaaalazmannnnn
            //adds the id as main
            TreeViewItem trvID = new TreeViewItem();
            trvID.Header = "ID: " + t.iTreatmentID;
            trvToAdd.Items.Add(trvID);

            //adds all the other field into the id
            //Treatments name
            TreeViewItem name = new TreeViewItem();
            name.Header = "Treatments name: " + t.sTreatmentName;
            trvID.Items.Add(name);

            //patient id
            TreeViewItem patientID = new TreeViewItem();
            patientID.Header = "Patients ID: " + t.iPatientID;
            trvID.Items.Add(patientID);

            //date of start
            TreeViewItem dateOfstart = new TreeViewItem();
            dateOfstart.Header = "Date of start: " + t.DateOfStart;
            trvID.Items.Add(dateOfstart);


            //date of finish
            TreeViewItem dateOffinish = new TreeViewItem();
            dateOffinish.Header = "Date of start: " + t.DateOfFinish;
            trvID.Items.Add(dateOffinish);

            //assinged doctor
            TreeViewItem assingedDoc = new TreeViewItem();
            assingedDoc.Header = "Assinged doctor: " + t.createdByDoctor;
            trvID.Items.Add(assingedDoc);

            //prognosis
            TreeViewItem prognosis = new TreeViewItem();
            prognosis.Header = "Prognosis: " + t.sPrognosis;
            trvID.Items.Add(prognosis);

            //prognosis of the technician


            //all the prescriptions
            TreeViewItem prescriptions = new TreeViewItem();
            prescriptions.Header = "Prescriptionss given";
            trvID.Items.Add(prescriptions);

            //adds the prescription into the prescription section
            foreach (string s in t.sPrescriptions)
            {
                TreeViewItem pres = new TreeViewItem();
                pres.Header = s;
                prescriptions.Items.Add(pres);
            }
        }
        private void addLabTestToTree(List<LabTest> currentLabTestList)
        {
            foreach (LabTest t in currentLabTestList)
            {
                //adds the id as main
                TreeViewItem trvID = new TreeViewItem();
                trvID.Header = "ID: " + t.iLabTestID;
                TreeView.Items.Add(trvID);

                //patient id
                TreeViewItem patientID = new TreeViewItem();
                patientID.Header = "Patients ID: " + t.iPatientId;
                trvID.Items.Add(patientID);

                //date of start
                TreeViewItem dateOfstart = new TreeViewItem();
                dateOfstart.Header = "Date of start: " + t.dateOfLabTest;
                trvID.Items.Add(dateOfstart);

                //assinged doctor
                TreeViewItem assingedDoc = new TreeViewItem();
                assingedDoc.Header = "Assinged doctor: " + t.AssignedDoctor;
                trvID.Items.Add(assingedDoc);

                //prognosis
                TreeViewItem results = new TreeViewItem();
                results.Header = "Results: " + t.sResults;
                trvID.Items.Add(results);

                //prognosis
                TreeViewItem prognosis = new TreeViewItem();
                prognosis.Header = "Prognosis: " + t.sDoctorNotes;
                trvID.Items.Add(prognosis);
            }
        }

        private void addLabTechToTree(List<LabTech> currentLabTechList)
        {
            foreach (LabTech t in currentLabTechList)
            {
                //adds the id as main
                TreeViewItem trvID = new TreeViewItem();
                trvID.Header = "ID: " + t.iID;
                TreeView.Items.Add(trvID);
                //adds all the other field into the id

                //first name
                TreeViewItem trvFirstName = new TreeViewItem();
                trvFirstName.Header = "First Name: " + t.sFirst_name;
                trvID.Items.Add(trvFirstName);

                //last name
                TreeViewItem trvLastName = new TreeViewItem();
                trvLastName.Header = "Last Name: " + t.sLast_Name;
                trvID.Items.Add(trvLastName);

                //salary
                TreeViewItem trvSalary = new TreeViewItem();
                trvSalary.Header = "Salary: " + t.dSalary;
                trvID.Items.Add(trvSalary);

                //Gender
                TreeViewItem trvGender = new TreeViewItem();
                trvGender.Header = "Gender: " + t.cGender;
                trvID.Items.Add(trvGender);
            }
        }
        #endregion



        private void Window_Closed(object sender, EventArgs e)
        {
            itsDAL.exportXMLDoctors(itsDAL.allDoctors(), "Doctor");
            itsDAL.exportXMLPatients(itsDAL.allPatients(), "Patient");
            itsDAL.exportXMLVisits(itsDAL.allVisits(), "Visit");
            itsDAL.exportXMLTreatments(itsDAL.allTreatments(), "Treatment");
            itsDAL.exportXMLSecurity(itsDAL.allSecurities(), "Security");
            itsDAL.exportXMLTechs(itsDAL.allLabTechs(), "Techs");
            itsDAL.exportXMLTests(itsDAL.allLabTests(), "Tests");
            //MessageBox.Show("colse");
        }

        private void Get_Def_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Treatment");//gets an id for the treatment to remove
                prompt.ShowDialog();
                if (itsTreatment_BL.getTreatmentByID(prompt.ID) == null)//if the treatment doesn't exist
                    throw new Exception("Wrong Treatment ID.");
                Treatment treatmentToDef = itsTreatment_BL.getTreatmentByID(prompt.ID);
                PrognosisDefinition def = new PrognosisDefinition(treatmentToDef.sPrognosis);
                def.ShowDialog();
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }

      

       








    }
}
