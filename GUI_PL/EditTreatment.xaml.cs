﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL_Backend;
using BL;

//this window is used to change account details of a treatment and update the new data in the database.

namespace GUI_PL
{
    /// Interaction logic for EditTreatment.xaml
    public partial class EditTreatment : Window
    {

        private Treatment itsTreatment;
        private Treatment_BL itsTreatment_BL;

        public EditTreatment(Treatment treatmentToEdit, Treatment_BL aTreatment_BL)
        {

            itsTreatment = treatmentToEdit;
            itsTreatment_BL = aTreatment_BL;
            InitializeComponent();
            Name_txt.Text = treatmentToEdit.sTreatmentName;
            ID_display.Content = treatmentToEdit.iTreatmentID;
            StartDate_txt.Text = treatmentToEdit.DateOfStart.ToString();
            EndDate_txt.Text = treatmentToEdit.DateOfFinish.ToString();
            Prognosis_txt.Text = treatmentToEdit.sPrognosis;
            visitID_display.Content = treatmentToEdit.iVisitID;
            string data = " ";
            foreach (string s in treatmentToEdit.sPrescriptions)
            {
                data += s.ToString() + ", ";
            }
            data = data.Substring(0, data.Length - 1);
            if (data == "")
                data = "none";
            Prescriptions_txt.Text = data;
            this.Visibility = System.Windows.Visibility.Visible;
        }

        // DateTime date = DateTime.Parse(Date_txt.Text, System.Globalization.CultureInfo.InvariantCulture);

        private void SaveChanges_btn_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                //int visitID = int.Parse(this.visitID_display.Content);
                //if (itsVisit_BL.getVisitByID() == null)
                //    throw new Exception("Main Doctor was not found in the Database");
                //these throw wrong input exception
                DateTime Startdate = DateTime.Parse(StartDate_txt.Text, System.Globalization.CultureInfo.InvariantCulture);
                DateTime Enddate = DateTime.Parse(EndDate_txt.Text, System.Globalization.CultureInfo.InvariantCulture);
                itsTreatment.sPrognosis = Prognosis_txt.Text;
                itsTreatment.DateOfStart = Startdate;
                itsTreatment.DateOfFinish = Enddate;
                itsTreatment.sTreatmentName = Name_txt.Text;
                if(itsTreatment.sPrescriptions.Count!=0)
                    itsTreatment.sPrescriptions.RemoveAt(0);
                itsTreatment.sPrescriptions.Add(Prescriptions_txt.Text);
                MessageBox.Show("Success");
                this.Close();
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }

        //private void addPrescription_btn_Click(object sender, RoutedEventArgs e)
        //{
        //    //no exceptions to catch here.
        //    getPrescriptionFromUser prompt = new getPrescriptionFromUser();
        //    prompt.ShowDialog();
        //    if (prompt.Prescription == "Empty")//if nothing was entered
        //        return;
        //    //else..
        //    string data = itsTreatment.sPrescriptions.ElementAt(0) + prompt.Prescription;
        //    itsTreatment.sPrescriptions.RemoveAt(0);
        //    itsTreatment.sPrescriptions.Add(data);
        //    MessageBox.Show("Success!");

        //    //string data = " ";//update the prescriptions label
        //    //foreach (string s in itsTreatment.sPrescriptions)
        //    //{
        //    //    data += s.ToString() + "\n";
        //    //}
        //    //data = data.Substring(0, data.Length - 1);
        //    //if (data == "")
        //    //    data = "none";
        //    Prescriptions_display.Content = data;
        //}

        //private void removePrescription_btn_Click(object sender, RoutedEventArgs e)
        //{
        //    getPrescriptionFromUser prompt = new getPrescriptionFromUser();
        //    prompt.ShowDialog();
        //    if (prompt.Prescription == "Empty")//if nothing was entered
        //        return;
        //    try
        //    {
        //        if (!itsTreatment.sPrescriptions.Contains(prompt.Prescription_txt.Text))
        //            throw new Exception("No such Prescription was found in the current treatment.");
        //        //else..
        //        itsTreatment.sPrescriptions.Remove(prompt.Prescription);
        //        MessageBox.Show("Success!");

        //        string data = " ";//update the prescriptions label
        //        foreach (string s in itsTreatment.sPrescriptions)
        //        {
        //            data += s.ToString() + "\n";
        //        }
        //        data = data.Substring(0, data.Length - 1);
        //        if (data == "")
        //            data = "none";
        //        Prescriptions_display.Content = data;
        //    }
        //    catch(Exception ex)
        //    {
        //        displayError(ex);
        //    }
        //}

        public void displayError(Exception ex)
        {
            errorDialog error = new errorDialog(ex);
            error.ShowDialog();
        }






    }
}
