﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL_Backend;
using BL;

//this window is used to change account details of a patient and update the new data in the database.

namespace GUI_PL
{
    /// Interaction logic for EditPatient.xaml
    public partial class EditPatient : Window
    {

        private Doctor_BL itsDoctor_BL;
        private Visit_BL itsVisit_BL;
        private Patient itsPatient;
        public EditPatient(Patient patientToEdit, Doctor_BL aDoctor_BL, Visit_BL aVisit_BL )
        {
            itsVisit_BL = aVisit_BL;
            itsDoctor_BL = aDoctor_BL;
            itsPatient = patientToEdit;
            InitializeComponent();
            ID_lbl.Content = patientToEdit.iID;
            FirstName_txt.Text = patientToEdit.sFirst_name;
            LastName_txt.Text = patientToEdit.sLast_Name;
            Age_txt.Text = Convert.ToString(patientToEdit.iAge);
            if (patientToEdit.cGender == 'm' || patientToEdit.cGender == 'M')
                Gender_cmb.SelectedItem = Male;

            else
                Gender_cmb.SelectedItem = Female;
            MainDoctor_txt.Text = Convert.ToString(patientToEdit.MainDoctor);
            this.Visibility = System.Windows.Visibility.Visible;
        }



        private void RemoveVisit_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Visit");
                prompt.ShowDialog();
                if (prompt.ID == -1)
                    return;
                //else..

                if (!itsVisit_BL.patientHasVisit(itsPatient.iID, prompt.ID))//if the visit isn't in the patient's list of visit
                    throw new Exception("No matching visits were found in the Database.");
                //else..
                itsVisit_BL.removeVisitFromPatient(itsPatient.iID, prompt.ID);

                MessageBox.Show("Success!");

            }
            catch (Exception ex)
            {
                displayError(ex);
            }
        }

        private void AddVisit_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Visit");
                prompt.ShowDialog();
                if (prompt.ID == -1)
                    return;
                //else..
                if (itsVisit_BL.patientHasVisit(itsPatient.iID, prompt.ID))//if the patient isn't in the doctor's list of patients
                    throw new Exception("Visit is already listed for this patient");

                if (itsVisit_BL.getVisitByID(prompt.ID) == null)
                    throw new Exception("Visit does not exist in the Database.");

                //else..
                itsVisit_BL.addVisitToPatient(itsPatient.iID,prompt.ID);
                MessageBox.Show("Success!");

            }
            catch (Exception ex)
            {
                displayError(ex);
            }
        }

        private void SaveChanges_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string firstName = FirstName_txt.Text;
                string lastName = LastName_txt.Text;
                int newAge = int.Parse(Age_txt.Text);//throws wrong input exception
                int assignedDoctor = int.Parse(MainDoctor_txt.Text);//throws wrong input exception
                //check for wrong inputs
                if (firstName == "")
                    throw new Exception("Please enter a First Name");
                if (lastName == "")
                    throw new Exception("Please enter a Last Name");
                if (newAge < 0)
                    throw new Exception("Invalid Age.");
                if (Gender_cmb.SelectedIndex == -1)
                    throw new Exception("Please enter a Gender");
                if (itsDoctor_BL.getDoctorByID(assignedDoctor) == null)
                    throw new Exception("Main Doctor was not found in the Database");
                //if no exceptions were thrown, update the Doctor in the DB
                itsPatient.sFirst_name = firstName;
                itsPatient.sLast_Name = lastName;
                itsPatient.iAge = newAge;
                if (Gender_cmb.SelectedItem == Female)
                    itsPatient.cGender = 'f';
                else
                    itsPatient.cGender = 'm';
                itsPatient.MainDoctor = assignedDoctor;
                MessageBox.Show("Success");
                this.Close();
            }
            catch (Exception ex)
            {
                displayError(ex);//tell the user to correct his input
            }
        }

        public void displayError(Exception ex)
        {
            errorDialog error = new errorDialog(ex);
            error.ShowDialog();
        }

     
    }
}
