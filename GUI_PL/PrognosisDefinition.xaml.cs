﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI_PL
{
    /// <summary>
    /// Interaction logic for PrognosisDefinition.xaml
    /// </summary>
    public partial class PrognosisDefinition : Window
    {
        public PrognosisDefinition(String s)
        {
            InitializeComponent();
            currText.Text = s;
        }

        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            CurrBlock.Text = retrive(currText.Text);
            //System.Diagnostics.Process.Start("http://www.clinicalnotesaid.org/emrreadability/notesaidresults.uwm?emr_text=" + CurrBlock.Text + "&submit=Simplify");
        }

        public String retrive(string str)
        {
            string htmlCode = "";
            str = str.Trim();
            if (str != "")
            {
                try
                {
                    WebClient client = new WebClient();

                    htmlCode = client.DownloadString(new Uri("http://en.wikipedia.org/wiki/" + str));
                    // return htmlCode.Substring(0,2000);
                    if (htmlCode.Contains("disambigbox"))
                    {

                        htmlCode = subString(htmlCode, "<ul>", "</ul>");
                        htmlCode = subString(htmlCode, "<li>", "</a>");
                        htmlCode = remove(htmlCode, "<", ">");
                        return retrive(htmlCode);

                    }


                    else
                    {
                        htmlCode = htmlCode.Substring(htmlCode.IndexOf("<p>") - 100, 10000);
                        // return htmlCode;
                        htmlCode = subString(htmlCode, "<p>", "</p>");
                        htmlCode = remove(htmlCode, "<", ">");
                        htmlCode = remove(htmlCode, "[", "]");
                        htmlCode = remove(htmlCode, "(", ")");
                        return htmlCode;
                    }
                }
                catch (WebException e)
                {
                    //another tryout
                    if (!str.Contains("Special"))
                    {

                        return retrive("Special:Search/" + str);
                    }
                    else
                    {
                        return htmlCode;
                    }

                }

                catch (Exception e)
                {

                    //another tryout
                    //       if (!str.Contains("Special")) { 
                    //    return retrive("Special:Search/" + str);
                    //       }else{
                    return htmlCode;
                    //       }
                }
            }
            else
            {
                return str;
            }
        }


        public String subString(String str, String fromSTR, String toSTR)
        {
            int from = str.IndexOf(fromSTR);
            int to = str.IndexOf(toSTR);
            if (from >= 0 && to - from >= 0)
            {
                str = str.Substring(from, to - from);
                return str;
            }
            else
            {
                return "to" + to + " from:" + from + str;
            }

        }
        public String remove(String str, String fromSTR, String toSTR)
        {

            int from = str.IndexOf(fromSTR);
            int to = str.IndexOf(toSTR);
            while (from >= 0 && to - from + 1 >= 0)
            {
                str = str.Remove(from, to - from + 1); from = str.IndexOf(fromSTR);
                from = str.IndexOf(fromSTR);
                to = str.IndexOf(toSTR);
            }
            return str;
        }

    }

}

