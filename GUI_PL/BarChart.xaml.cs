﻿using System.Windows.Controls.DataVisualization.Charting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL_Backend;

namespace GUI_PL
{
    /// <summary>
    /// Interaction logic for PieChart.xaml
    /// </summary>Check
    public partial class BarChart : Window
    {
        public BarChart(List<Visit> hisVisits)
        {
            InitializeComponent();
            int first=0, second=0, third=0, fourth=0;
            foreach(Visit v in hisVisits)
            {
                if (v.dateOfVisit.Year == DateTime.Now.Year)
                {
                    if (v.dateOfVisit.Month >= 1 && v.dateOfVisit.Month <= 3)
                        first++;
                    if (v.dateOfVisit.Month >= 4 && v.dateOfVisit.Month <= 6)
                        second++;
                    if (v.dateOfVisit.Month >= 7 && v.dateOfVisit.Month <= 9)
                        third++;
                    if (v.dateOfVisit.Month >= 10 && v.dateOfVisit.Month <= 12)
                        fourth++;
                }

            }
                    chart.DataContext = new KeyValuePair<string, int>[] {
                                    new KeyValuePair<string, int>("January-March", first),
                                    new KeyValuePair<string, int>("April-June", second),
                                    new KeyValuePair<string, int>("July-September", third),
                                    new KeyValuePair<string, int>("October-December", fourth)};
                    Title = "Quarterly Workload" ;
                  

             

            }
        }

    }


