﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL_Backend;
using BL;

//this window is used to change account details of a test and update the new data in the database.

namespace GUI_PL
{
    /// Interaction logic for EditTest.xaml
    public partial class EditLabTest : Window
    {
        private LabTest itsTest;
        private Doctor_BL itsDoctor_BL;
        private LabTech_BL itsLabTech_BL;
        public EditLabTest(LabTest testToEdit, Doctor_BL aDoctor_BL, LabTech_BL aLabTech_BL)
        {
            this.itsDoctor_BL=aDoctor_BL;
            this.itsLabTech_BL=aLabTech_BL;
            this.itsTest = testToEdit;
            InitializeComponent();
            ID_display.Content = testToEdit.iLabTestID;
            Date_txt.Text = itsTest.dateOfLabTest.ToString();
            Notes_txt.Text = itsTest.sDoctorNotes;
            Results_txt.Text = itsTest.sResults;
            AssignedDoctor_txt.Text = Convert.ToString(itsTest.AssignedDoctor);
            PatientID_lbl.Content = "Patient ID: " + itsTest.iPatientId;
            AssignedTech_txt.Text = Convert.ToString(itsTest.iLabTechId);
            this.Visibility = System.Windows.Visibility.Visible;
        }


        private void SaveChanges_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime date = DateTime.Parse(Date_txt.Text, System.Globalization.CultureInfo.InvariantCulture);
                int assignedDoctor = int.Parse(AssignedDoctor_txt.Text);//throws wrong input exception
                int assignedTechnician = int.Parse(AssignedTech_txt.Text);//throws wrong input exception

                //check for wrong inputs
                if (itsDoctor_BL.getDoctorByID(assignedDoctor) == null)
                    throw new Exception("Assigned Doctor was not found in the Database");
                if (itsLabTech_BL.getLabTechByID(assignedTechnician) == null&&assignedTechnician!=0)
                    throw new Exception("Assigned Technician was not found in the Database");
                if (date.ToString() == (new DateTime()).ToString())//if the date wasn't changed
                    throw new Exception("Please enter Date");
                //if no exceptions were thrown, update the Test in the DB
                itsTest.AssignedDoctor = assignedDoctor;
                itsTest.iLabTechId = assignedTechnician;
                itsTest.dateOfLabTest = date;
                itsTest.sDoctorNotes = Notes_txt.Text;
                itsTest.sResults = Results_txt.Text;
                Doctor doctor = itsDoctor_BL.getDoctorByID(assignedDoctor);
                if (!Results_txt.Text.Equals(""))
                {
                    doctor.needsToBeNotified = true;
                    itsDoctor_BL.updateDoctor(doctor);
                }

                MessageBox.Show("Success");
                this.Close();
            }
            catch (Exception ex)
            {
                displayError(ex);//tell the user to correct his input
            }
        }

        public void displayError(Exception ex)
        {
            errorDialog error = new errorDialog(ex);
            error.ShowDialog();
        }








    }
}
