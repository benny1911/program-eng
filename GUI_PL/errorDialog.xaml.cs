﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI_PL
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class errorDialog : Window
    {
        public errorDialog(Exception ex)
        {
            InitializeComponent();
            errorLabel.Content=ex.Message;
        }

        private void CloseWindow(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


    }
}
