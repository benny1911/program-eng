﻿#pragma checksum "..\..\EditPatient.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "6F6F7644ECF448449AAD779A82E9F8CB"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GUI_PL {
    
    
    /// <summary>
    /// EditPatient
    /// </summary>
    public partial class EditPatient : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 20 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label FirstName_lbl;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LastName_lbl;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Gender_lbl;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Age_lbl;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label MainDoctor_lbl;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddVisit_btn;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button RemoveVisit_btn;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ID_lbl;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FirstName_txt;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LastName_txt;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Age_txt;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Gender_cmb;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem Male;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem Female;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox MainDoctor_txt;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\EditPatient.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveChanges_btn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GUI_PL;component/editpatient.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\EditPatient.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.FirstName_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.LastName_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.Gender_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.Age_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.MainDoctor_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.AddVisit_btn = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\EditPatient.xaml"
            this.AddVisit_btn.Click += new System.Windows.RoutedEventHandler(this.AddVisit_btn_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.RemoveVisit_btn = ((System.Windows.Controls.Button)(target));
            
            #line 26 "..\..\EditPatient.xaml"
            this.RemoveVisit_btn.Click += new System.Windows.RoutedEventHandler(this.RemoveVisit_btn_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.ID_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.FirstName_txt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.LastName_txt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.Age_txt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.Gender_cmb = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 13:
            this.Male = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 14:
            this.Female = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 15:
            this.MainDoctor_txt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.SaveChanges_btn = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\EditPatient.xaml"
            this.SaveChanges_btn.Click += new System.Windows.RoutedEventHandler(this.SaveChanges_btn_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

