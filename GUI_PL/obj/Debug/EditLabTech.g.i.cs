﻿#pragma checksum "..\..\EditLabTech.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "2F6BE95B2A2DAACF72986432F333EF36"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GUI_PL {
    
    
    /// <summary>
    /// EditLabTech
    /// </summary>
    public partial class EditLabTech : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\EditLabTech.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label FirstName_lbl;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\EditLabTech.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LastName_lbl;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\EditLabTech.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Gender_lbl;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\EditLabTech.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Salary_lbl;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\EditLabTech.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ID_lbl;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\EditLabTech.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FirstName_txt;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\EditLabTech.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LastName_txt;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\EditLabTech.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Salary_txt;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\EditLabTech.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Gender_cmb;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\EditLabTech.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem Male;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\EditLabTech.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem Female;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\EditLabTech.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveChanges_btn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GUI_PL;component/editlabtech.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\EditLabTech.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.FirstName_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.LastName_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.Gender_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.Salary_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.ID_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.FirstName_txt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.LastName_txt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.Salary_txt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.Gender_cmb = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 10:
            this.Male = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 11:
            this.Female = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 12:
            this.SaveChanges_btn = ((System.Windows.Controls.Button)(target));
            
            #line 36 "..\..\EditLabTech.xaml"
            this.SaveChanges_btn.Click += new System.Windows.RoutedEventHandler(this.SaveChanges_btn_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

