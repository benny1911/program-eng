﻿#pragma checksum "..\..\EditTreatment.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4E3DB58436A55BCE69349A7D3DBA5095"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GUI_PL {
    
    
    /// <summary>
    /// EditTreatment
    /// </summary>
    public partial class EditTreatment : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 18 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ID_lbl;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label StartDate_lbl;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label EndDate_lbl;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Prognosis_lbl;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Prescriptions_lbl;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ID_display;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox StartDate_txt;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox EndDate_txt;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Prognosis_txt;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Prescriptions_txt;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label AssignedDoctor_display;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveChanges_btn;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Name_lbl;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Name_txt;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label visitID_lbl;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\EditTreatment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label visitID_display;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GUI_PL;component/edittreatment.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\EditTreatment.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ID_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.StartDate_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.EndDate_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.Prognosis_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.Prescriptions_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.ID_display = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.StartDate_txt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.EndDate_txt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.Prognosis_txt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.Prescriptions_txt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.AssignedDoctor_display = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.SaveChanges_btn = ((System.Windows.Controls.Button)(target));
            
            #line 31 "..\..\EditTreatment.xaml"
            this.SaveChanges_btn.Click += new System.Windows.RoutedEventHandler(this.SaveChanges_btn_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.Name_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.Name_txt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.visitID_lbl = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.visitID_display = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

