﻿using System.Windows.Controls.DataVisualization.Charting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL_Backend;

namespace GUI_PL
{
    /// <summary>
    /// Interaction logic for PieChart.xaml
    /// </summary>Check
    public partial class PieChart : Window
    {
        public PieChart(List<Patient> patients, string whatToCheck)
        {
            InitializeComponent();
            switch (whatToCheck)
            {
                case "Age":
                    int children=0, teenagers=0, youngAdults=0, adults=0, elderly=0;
                    foreach (Patient p in patients)
                    {
                        if(p.iAge<12)
                            children++;
                        else if(p.iAge<20)
                            teenagers++;
                        else if (p.iAge<31)
                            youngAdults++;
                        else if (p.iAge<51)
                            adults++;
                        else
                            elderly++;
                    }

                    chart.DataContext = new KeyValuePair<string, int>[] {
                                    new KeyValuePair<string, int>("Children 0-11", children),
                                    new KeyValuePair<string, int>("Teenagers 12-19", teenagers),
                                    new KeyValuePair<string, int>("Young Adults 20-30", youngAdults),
                                    new KeyValuePair<string, int>("Adults 31-50", adults),
                                    new KeyValuePair<string, int>("Elderly 51+", elderly) };
                    Title = "Division By " + whatToCheck;
                    break;
                    
                case "Gender":
                    int male=0, female=0;
                    foreach (Patient p in patients)
                    {
                        if (p.cGender == 'm' || p.cGender == 'M')
                            male++;
                        else
                            female++;
                       
                    }

                    chart.DataContext = new KeyValuePair<string, int>[] {
                                    new KeyValuePair<string, int>("Male", male),
                                    new KeyValuePair<string, int>("Female", female)
                                     };
                    Title = "Division By " + whatToCheck;

                    break;

                default: break;
    
            }
        }

    }

}
