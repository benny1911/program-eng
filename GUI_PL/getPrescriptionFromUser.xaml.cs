﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI_PL
{
    public partial class getPrescriptionFromUser : Window
    {
        public string Prescription { get; set; }
        public getPrescriptionFromUser()
        {
            this.Prescription = "Empty";
            InitializeComponent();
            this.Visibility = System.Windows.Visibility.Visible;
        }

        public string getPrescription()
        {
            return this.Prescription;
        }
        private void OK_btn_Click(object sender, RoutedEventArgs e)
        {
                this.Prescription = Prescription_txt.Text;
                this.Close();
        }

        //tells the user to correct his input
        public void displayError(Exception ex)
        {
            errorDialog error = new errorDialog(ex);
            error.ShowDialog();
        }
    }
}
