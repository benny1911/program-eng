﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL_Backend;
using BL;

//this window is used to change account details of a visit and update the new data in the database.

namespace GUI_PL
{
    /// Interaction logic for EditVisit.xaml
    public partial class EditVisit : Window
    {
        private Treatment_BL itsTreatment_BL;
        private Doctor_BL itsDoctor_BL;
        private Visit itsVisit;
        private Visit_BL itsVisit_BL;
        public EditVisit(Visit visitToEdit, Doctor_BL aDoctor_BL, Treatment_BL aTreatment_BL, Visit_BL aVisit_BL)
        {
            itsDoctor_BL = aDoctor_BL;
            itsTreatment_BL = aTreatment_BL;
            itsVisit = visitToEdit;
            itsVisit_BL = aVisit_BL;
            InitializeComponent();
            ID_display.Content = visitToEdit.iVisitID;
            Date_txt.Text = itsVisit.dateOfVisit.ToString();
            Notes_txt.Text = itsVisit.sDoctorNotes;
            AssignedDoctor_txt.Text = Convert.ToString(itsVisit.AssignedDoctor);
            PatientID_lbl.Content = "Patient ID: " + itsVisit.iPatientId;
            this.Visibility = System.Windows.Visibility.Visible;
        }


        private void addTreatment_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Treatment");
                prompt.ShowDialog();
                if (prompt.ID == -1)
                    return;
                //else..

                if (itsVisit_BL.visitHasTreatment(itsVisit.iVisitID, prompt.ID))//if the treatments isn't in the visit's list of treatments
                    throw new Exception("Treatment is already listed for this visit");

                if (itsTreatment_BL.getTreatmentByID(prompt.ID) == null)
                    throw new Exception("Treatment does not exist in the Database.");

                //else..
                itsVisit_BL.addTreatmentToVisit(itsVisit.iVisitID,prompt.ID);
                MessageBox.Show("Success!");

            }
            catch (Exception ex)
            {
                displayError(ex);
            }
        }

        private void removeTreatment_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Treatment");
                prompt.ShowDialog();
                if (prompt.ID == -1)
                    return;
                //else..
                if (!itsVisit_BL.visitHasTreatment(itsVisit.iVisitID, prompt.ID))//if the treatment isn't in the visit's list of treatments
                    throw new Exception("No matching treatments were found in the Database.");
                //else..
                itsVisit_BL.removeTreatmentFromVisit(itsVisit.iVisitID, prompt.ID);
                MessageBox.Show("Success!");

            }
            catch (Exception ex)
            {
                displayError(ex);
            }
        }
        private void SaveChanges_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime date = DateTime.Parse(Date_txt.Text, System.Globalization.CultureInfo.InvariantCulture);
                int assignedDoctor = int.Parse(AssignedDoctor_txt.Text);//throws wrong input exception
                //check for wrong inputs
                if (itsDoctor_BL.getDoctorByID(assignedDoctor) == null)
                    throw new Exception("Assigned Doctor was not found in the Database");
                if(date.ToString()==(new DateTime()).ToString())//if the date wasn't changed
                    throw new Exception("Please enter Date");
                //if no exceptions were thrown, update the Doctor in the DB
                itsVisit.AssignedDoctor = assignedDoctor;
                itsVisit.dateOfVisit = date;
                itsVisit.sDoctorNotes = Notes_txt.Text;
                MessageBox.Show("Success");
                this.Close();
            }
            catch (Exception ex)
            {
                displayError(ex);//tell the user to correct his input
            }
        }

        public void displayError(Exception ex)
        {
            errorDialog error = new errorDialog(ex);
            error.ShowDialog();
        }








    }
}
