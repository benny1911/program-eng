﻿using BL;
using DAL;
using BL_Backend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;




namespace GUI_PL
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {

        //fields:
        PasswordBox box = new PasswordBox();
        private Doctor_BL itsDoctor_BL;
        private LabTech_BL itsLabTech_BL;
        private Patient_BL itsPatient_BL;
        private Securities_BL itsSecurity;
        //all of the Business Layer classes that the GUI works with
        private IDAL itsDAL;
        //the gui stores the lists it uses as attributes for multiple queries (sort by name then sort by salary, for example)    
        public List<Doctor> currentDoctorsList { get; set; }
        public List<Patient> currentPatientsList { get; set; }


        public LoginWindow(IDAL itsDal)
        {
            InitializeComponent();
            //creates the database
            this.itsDAL = itsDal;
            itsDoctor_BL = new Doctor_BL(itsDAL);
            itsLabTech_BL = new LabTech_BL(itsDAL);
            itsPatient_BL = new Patient_BL(itsDAL);
            currentDoctorsList = new List<Doctor>();
            currentPatientsList = new List<Patient>();
            resetDoctors(currentDoctorsList);//get data from Databases
            resetPatients(currentPatientsList);
            this.itsSecurity = new Securities_BL(itsDAL);
        }
        private void resetDoctors(List<Doctor> listToReset)
        {
            listToReset.Clear();
            List<Doctor> doctors=itsDAL.allDoctors();
            foreach (Doctor doctorToAdd in doctors)
                listToReset.Add(doctorToAdd);
        }
        private void resetPatients(List<Patient> listToReset)
        {
            listToReset.Clear();
            List<Patient> patients = itsDAL.allPatients();
            foreach (Patient patientToAdd in patients)
                listToReset.Add(patientToAdd);
        }


        private void ButtonClickToLogin(object sender, RoutedEventArgs e)
        {
            //the inputs:
            String inputId = ID.Text;
            String inputPass = password.Password;

            bool ans = false;
            string name = "";
            string authorization = "wrong input";
            try
            {
                authorization = this.itsSecurity.theUserType(inputId, Convert.ToInt32(inputPass));
            }
            catch (Exception) { }
            switch (authorization)
            {
                case "Admin": name = "Manager";
                    break;
                case "Patient": name = itsPatient_BL.getPatientByID(Convert.ToInt32(inputId)).sFirst_name;
                    break;
                case "Doctor": name = itsDoctor_BL.getDoctorByID(Convert.ToInt32(inputId)).sFirst_name;
                    break;
                case "Tech": name = itsLabTech_BL.getLabTechByID(Convert.ToInt32(inputId)).sFirst_name;
                    break;
            }
            if (authorization == "Admin" || authorization == "Patient" || authorization == "Doctor" || authorization == "Tech")
            {
                ans = itsSecurity.ValidateDetails(inputId.ToString(), Convert.ToInt32(inputPass));
                if (!(ans)) MessageBox.Show("Wrong password, Try again please.");
                else
                {
                    MessageBox.Show("Welcome " + name + "!");
                    this.Close();
                }
            }
            else if (authorization == "wrong input" || authorization == "none") MessageBox.Show("Wrong input.");
            else MessageBox.Show("Wrong details, Try again please.");

        }


    }
}
