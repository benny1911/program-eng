﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL_Backend;
using BL;

//this window is used to change account details of a doctor and update the new data in the database.

namespace GUI_PL
{
    /// Interaction logic for EditDoctor.xaml
    public partial class EditDoctor : Window
    {
        private Patient_BL itsPatient_BL;
        private Doctor itsDoctor;
        private Doctor_BL itsDoctor_BL;

        public EditDoctor(Doctor doctorToEdit, Patient_BL aPatient_BL, Doctor_BL aDoctor_BL)
        {
            itsPatient_BL = aPatient_BL;
            itsDoctor = doctorToEdit;
            itsDoctor_BL = aDoctor_BL;
            InitializeComponent();
            ID_lbl.Content = doctorToEdit.iID;
            FirstName_txt.Text = doctorToEdit.sFirst_name;
            LastName_txt.Text = doctorToEdit.sLast_Name;
            Salary_txt.Text = Convert.ToString(doctorToEdit.dSalary);
            if (doctorToEdit.cGender == 'm' || doctorToEdit.cGender == 'M')
                Gender_cmb.SelectedItem = Male;

            else
                Gender_cmb.SelectedItem = Female;
            this.Visibility = System.Windows.Visibility.Visible;
        }


        private void AddPatient_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Patient");
                prompt.ShowDialog();
                if (prompt.ID == -1)
                    return;
                //else..

                if (itsDoctor_BL.hasPatient(itsDoctor.iID,prompt.ID))//if the patient isn't in the doctor's list of patients
                    throw new Exception("Patient is already a patient of the doctor.");

                if (itsPatient_BL.getPatientByID(prompt.ID) == null)
                    throw new Exception("Patient does not exist in the Database.");

                //else..
                itsDoctor_BL.addPatientToDoctor(itsDoctor.iID, prompt.ID);
                MessageBox.Show("Success!");

            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }

        private void RemovePatient_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                getIdFromUser prompt = new getIdFromUser("Patient");
                prompt.ShowDialog();
                if (prompt.ID == -1)
                    return;
                //else..
                if (!itsDoctor_BL.hasPatient(itsDoctor.iID, prompt.ID))//if the patient isn't in the doctor's list of patients
                    throw new Exception("No matching patients were found in the Database.");
                //else..
                itsDoctor_BL.removePatientFromDoctor(itsDoctor.iID, prompt.ID);
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                displayError(ex);
            }

        }



        private void SaveChanges_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string firstName = FirstName_txt.Text;
                string lastName = LastName_txt.Text;
                double salary = double.Parse(Salary_txt.Text);//throws wrong input exception
                //check for wrong inputs
                if (firstName == "")
                    throw new Exception("Please enter a First Name");
                if (lastName == "")
                    throw new Exception("Please enter a Last Name");
                if (salary < 0)
                    throw new Exception("Invalid salary.");
                if (Gender_cmb.SelectedIndex == -1)
                    throw new Exception("Please enter a Gender");

                //if no exceptions were thrown, update the Doctor in the DB
                itsDoctor.sFirst_name = firstName;
                itsDoctor.sLast_Name = lastName;
                itsDoctor.dSalary = salary;
                if (Gender_cmb.SelectedItem == Female)
                    itsDoctor.cGender = 'f';
                else
                    itsDoctor.cGender = 'm';
                MessageBox.Show("Success");
                this.Close();
            }
            catch (Exception ex)
            {
                displayError(ex);//tell the user to correct his input
            }
        }

        //tells the user to correct his input
        public void displayError(Exception ex)
        {
            errorDialog error = new errorDialog(ex);
            error.ShowDialog();
        }




    }
}
