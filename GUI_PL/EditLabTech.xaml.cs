﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL_Backend;
using BL;

//this window is used to change account details of a Tech and update the new data in the database.

namespace GUI_PL
{
    /// Interaction logic for EditTech.xaml
    public partial class EditLabTech : Window
    {
        private LabTech itsTech;

        public EditLabTech(LabTech TechToEdit)
        {
            itsTech = TechToEdit;
            InitializeComponent();
            ID_lbl.Content = TechToEdit.iID;
            FirstName_txt.Text = TechToEdit.sFirst_name;
            LastName_txt.Text = TechToEdit.sLast_Name;
            Salary_txt.Text = Convert.ToString(TechToEdit.dSalary);
            if (TechToEdit.cGender == 'm' || TechToEdit.cGender == 'M')
                Gender_cmb.SelectedItem = Male;

            else
                Gender_cmb.SelectedItem = Female;
            this.Visibility = System.Windows.Visibility.Visible;
        }



        private void SaveChanges_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string firstName = FirstName_txt.Text;
                string lastName = LastName_txt.Text;
                double salary = double.Parse(Salary_txt.Text);//throws wrong input exception
                //check for wrong inputs
                if (firstName == "")
                    throw new Exception("Please enter a First Name");
                if (lastName == "")
                    throw new Exception("Please enter a Last Name");
                if (salary < 0)
                    throw new Exception("Invalid salary.");
                if (Gender_cmb.SelectedIndex == -1)
                    throw new Exception("Please enter a Gender");

                //if no exceptions were thrown, update the Tech in the DB
                itsTech.sFirst_name = firstName;
                itsTech.sLast_Name = lastName;
                itsTech.dSalary = salary;
                if (Gender_cmb.SelectedItem == Female)
                    itsTech.cGender = 'f';
                else
                    itsTech.cGender = 'm';
                MessageBox.Show("Success");
                this.Close();
            }
            catch (Exception ex)
            {
                displayError(ex);//tell the user to correct his input
            }
        }

        //tells the user to correct his input
        public void displayError(Exception ex)
        {
            errorDialog error = new errorDialog(ex);
            error.ShowDialog();
        }




    }
}
